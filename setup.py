from distutils.core import setup

setup(
    name = 'SeyboldLab',
    packages = [
        'seyboldlab',
        #'seyboldlab.stimuli',
        #'seyboldlab.experiments',
        #'seyboldlab.tools',
        #'seyboldlab.analysis',

        #'seyboldlab.experiments.behavior',
        #'seyboldlab.experiments.extracellular',

        #'seyboldlab.tools.spikesorter',
        #'seyboldlab.tools.modelneuron',
        #'seyboldlab.tools.webserver',
    ],
    version = '0.0.1',
    description = "Code for Bryan Seybold's lab",
    author = 'Bryan Seybold',
    author_email = 'baseybold@gmail.com',
    url = 'http://www.ucsf.edu',
    download_url = 'http://',
    keywords = ['science','data processing','sound'],
    classifiers = [
        "Programming Language :: Python :: 3",
    ],
    long_description = """\
TEXT BLAH BLAH
    """)
