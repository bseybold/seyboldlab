import seyboldlab.tools.data.seybold_hdf5 as hdf
import tables
import unittest
import os
import shutil
import numpy as np

class BasicFileIO(unittest.TestCase):
    def tearDown(self):
        try:
            os.remove('test.hdf5')
        except OSError:
            pass

    def test_open(self):
        myfile = hdf.RecordingFile('test.hdf5','w')

    def test_open_with_title(self):
        myfile = hdf.RecordingFile('test.hdf5','w','test file')

    def test_close(self):
        myfile = hdf.RecordingFile('test.hdf5','w','test file')
        myfile.close()

    def test_attr_setting(self):
        myfile = hdf.RecordingFile('test.hdf5','w','test file')
        myfile.set_attrs_from_dict({'test_1':'yes'})
        self.assertEqual(myfile.attrs['test_1'],'yes')
        myfile.close()
        myfile = hdf.RecordingFile('test.hdf5','r','test file')
        self.assertEqual(myfile.attrs['test_1'],'yes')

    def test_attr_retreival(self):
        myfile = hdf.RecordingFile('test.hdf5','w','test file')
        myfile.attrs['test_1'] = 'yes'
        myfile.close()
        myfile = hdf.RecordingFile('test.hdf5','a','test file')
        self.assertEqual(myfile.attrs['test_1'],'yes')
        myfile.attrs['test_2'] = 'no'
        myfile.close()
        myfile = hdf.RecordingFile('test.hdf5','r','test file')
        self.assertEqual(myfile.attrs['test_1'],'yes')
        self.assertEqual(myfile.attrs['test_2'],'no')

    def test_overwrite(self):
        myfile = hdf.RecordingFile('test.hdf5','w')
        myfile.close()
        with self.assertRaises(ValueError):
            myfile = hdf.RecordingFile('test.hdf5','w')
        myfile = hdf.RecordingFile('test.hdf5','w',allow_overwrite=True)
        myfile.close()
        myfile = hdf.RecordingFile('test.hdf5','a')
        myfile.close()

class RecordingChannelsTest(unittest.TestCase):
    def setUp(self):
        self.file = hdf.RecordingFile('test.hdf5','w','test file')

    def tearDown(self):
        self.file.close()
        os.remove('test.hdf5')

    def test_initialize_bare_bones(self):
        rc = self.file.open_recording_channels('raw','csc',1,include_time=False)

    def test_initialize_with_timing(self):
        rc = self.file.open_recording_channels('raw','csc',1)

    def test_initialize_with_integers(self):
        rc = self.file.open_recording_channels('raw','csc',1,dtype=np.int16)

    def test_initialize_with_multiple_channels(self):
        rc = self.file.open_recording_channels('raw','csc',2)

    def test_write(self):
        rc = self.file.open_recording_channels('raw','csc',2)
        rc.append_data(np.array([[0, 1],[2, 3],[4,5]]),np.array([0,1,2]))

    def test_read(self):
        rc = self.file.open_recording_channels('raw','csc',2)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        data, time = rc.read_data()
        self.assertTrue((in1 == data).all())
        self.assertTrue((in2 == time).all())

    def test_read_with_integers(self):
        rc = self.file.open_recording_channels('raw','csc',2,dtype=np.int16)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        data, time = rc.read_data()
        self.assertTrue((in1 == data).all())
        self.assertTrue((in2 == time).all())

    def test_read_where(self):
        rc = self.file.open_recording_channels('raw','csc',2)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        data, time = rc.read_data(0,1)
        self.assertTrue((data == in1[:1,:]).all())
        self.assertTrue((time == in2[:1]).all())
        data, time = rc.read_data(0,2)
        self.assertTrue((data == in1[:2,:]).all())
        self.assertTrue((time == in2[:2]).all())
        data, time = rc.read_data(1,3)
        self.assertTrue((data == in1[1:3,:]).all())
        self.assertTrue((time == in2[1:3]).all())

    def test_restore(self):
        rc = self.file.open_recording_channels('raw','csc',2,dtype=np.int16)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        self.file.close()
        self.file = hdf.RecordingFile('test.hdf5','r','test file')
        rc = self.file.open_recording_channels('raw','csc',2,dtype=np.int16)
        data, time = rc.read_data()
        self.assertTrue((in1 == data).all())
        self.assertTrue((in2 == time).all())

class RecordingEventsTest(unittest.TestCase):
    def setUp(self):
        self.file = hdf.RecordingFile('test.hdf5','w','test file')

    def tearDown(self):
        self.file.close()
        os.remove('test.hdf5')

    def test_initialize_bare_bones(self):
        rc = self.file.open_recording_events('raw','csc',1,include_time=False)

    def test_initialize_with_timing(self):
        rc = self.file.open_recording_events('raw','csc',1)

    def test_initialize_with_integers(self):
        rc = self.file.open_recording_events('raw','csc',1,dtype=np.int16)

    def test_initialize_with_multiple_channels(self):
        rc = self.file.open_recording_events('raw','csc',2)

    def test_write(self):
        rc = self.file.open_recording_events('raw','csc',2)
        rc.append_data(np.array([[0, 1],[2, 3],[4,5]]),np.array([0,1,2]))

    def test_read(self):
        rc = self.file.open_recording_events('raw','csc',2)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        data, time = rc.read_data()
        self.assertTrue((in1 == data).all())
        self.assertTrue((in2 == time).all())

    def test_read_with_integers(self):
        rc = self.file.open_recording_events('raw','csc',2,dtype=np.int16)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        data, time = rc.read_data()
        self.assertTrue((in1 == data).all())
        self.assertTrue((in2 == time).all())

    def test_read_where(self):
        rc = self.file.open_recording_events('raw','csc',2)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        data, time = rc.read_data(0,1)
        self.assertTrue((data == in1[:1,:]).all())
        self.assertTrue((time == in2[:1]).all())
        data, time = rc.read_data(0,2)
        self.assertTrue((data == in1[:2,:]).all())
        self.assertTrue((time == in2[:2]).all())
        data, time = rc.read_data(1,3)
        self.assertTrue((data == in1[1:3,:]).all())
        self.assertTrue((time == in2[1:3]).all())

    def test_restore(self):
        rc = self.file.open_recording_events('raw','csc',2,dtype=np.int16)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        self.file.close()
        self.file = hdf.RecordingFile('test.hdf5','r','test file')
        rc = self.file.open_recording_events('raw','csc',2,dtype=np.int16)
        data, time = rc.read_data()
        self.assertTrue((in1 == data).all())
        self.assertTrue((in2 == time).all())

class RecordingWaveformTest(unittest.TestCase):
    def setUp(self):
        self.file = hdf.RecordingFile('test.hdf5','w','test file')

    def tearDown(self):
        self.file.close()
        os.remove('test.hdf5')

    def test_initialize_bare_bones(self):
        rc = self.file.open_recording_waveforms('raw','csc',1,include_time=False)

    def test_initialize_with_timing(self):
        rc = self.file.open_recording_waveforms('raw','csc',1)

    def test_initialize_with_integers(self):
        rc = self.file.open_recording_waveforms('raw','csc',1,dtype=np.int16)

    def test_initialize_with_multiple_channels(self):
        rc = self.file.open_recording_waveforms('raw','csc',2)

    def test_write(self):
        rc = self.file.open_recording_waveforms('raw','csc',2)
        rc.append_data(np.array([[0, 1],[2, 3],[4,5]]),np.array([0,1,2]))

    def test_read(self):
        rc = self.file.open_recording_waveforms('raw','csc',2)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        data, time = rc.read_data()
        self.assertTrue((in1 == data).all())
        self.assertTrue((in2 == time).all())

    def test_read_with_integers(self):
        rc = self.file.open_recording_waveforms('raw','csc',2,dtype=np.int16)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        data, time = rc.read_data()
        self.assertTrue((in1 == data).all())
        self.assertTrue((in2 == time).all())

    def test_read_where(self):
        rc = self.file.open_recording_waveforms('raw','csc',2)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        data, time = rc.read_data(0,1)
        self.assertTrue((data == in1[:1,:]).all())
        self.assertTrue((time == in2[:1]).all())
        data, time = rc.read_data(0,2)
        self.assertTrue((data == in1[:2,:]).all())
        self.assertTrue((time == in2[:2]).all())
        data, time = rc.read_data(1,3)
        self.assertTrue((data == in1[1:3,:]).all())
        self.assertTrue((time == in2[1:3]).all())

    def test_restore(self):
        rc = self.file.open_recording_waveforms('raw','csc',2,dtype=np.int16)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        self.file.close()
        self.file = hdf.RecordingFile('test.hdf5','r','test file')
        rc = self.file.open_recording_waveforms('raw','csc',2,dtype=np.int16)
        data, time = rc.read_data()
        self.assertTrue((in1 == data).all())
        self.assertTrue((in2 == time).all())

class ErrorsTest(unittest.TestCase):
    def setUp(self):
        self.file = hdf.RecordingFile('test.hdf5','w','test file')

    def tearDown(self):
        self.file.close()
        os.remove('test.hdf5')

    def test_double_assign(self):
        with self.assertRaises(TypeError):
            rc = self.file.open_recording_waveforms('raw','csc',1)
            rc = self.file.open_recording_channels('raw','csc',1)

    def test_bad_datatype(self):
        with self.assertRaises(TypeError):
            rc = self.file.open_recording_channels('raw','csc',1,dtype='string')

    def test_bad_time_datatype(self):
        with self.assertRaises(TypeError):
            rc = self.file.open_recording_channels('raw','csc',1,time_dtype='string')

    def test_negative_channels(self):
        with self.assertRaises(ValueError):
            rc = self.file.open_recording_channels('raw','csc',-1)

    def test_append_to_different_channel_count(self):
        with self.assertRaises(ValueError):
            rc = self.file.open_recording_waveforms('raw','csc',1)
            rc.append_data(np.array([0,1]),np.array([0,1]))
            rc = self.file.open_recording_waveforms('raw','csc',2)
            rc.append_data(np.array([[0,1],[2,3]]),np.array([0,1]))

    def test_read_from_greater_channel_count(self):
        with self.assertRaises(ValueError):
            rc = self.file.open_recording_waveforms('raw','csc',1)
            rc.append_data(np.array([0,1]),np.array([0,1]))
            rc = self.file.open_recording_waveforms('raw','csc',2)
            rc.read_data()

    def test_read_from_lower_channel_count(self):
        with self.assertRaises(ValueError):
            rc = self.file.open_recording_waveforms('raw','csc',2)
            rc.append_data(np.array([[0,1],[2,3]]),np.array([0,1]))
            rc = self.file.open_recording_waveforms('raw','csc',1)
            rc.read_data()

class MultiplesTest(unittest.TestCase):
    def setUp(self):
        self.file = hdf.RecordingFile('test.hdf5','w','test file')

    def tearDown(self):
        self.file.close()
        os.remove('test.hdf5')

    def test_one_of_each(self):
        rc = self.file.open_recording_waveforms('raw','snip',2)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        rc = self.file.open_recording_channels('raw','wave',2)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)
        rc = self.file.open_recording_events('raw','event',2)
        in1 = np.array([[0, 1],[2, 3],[4,5]])
        in2 = np.array([0,1,2])
        rc.append_data(in1,in2)

if __name__ == '__main__':
    single_test = None
    ##comment or uncomment to allow testing of a single test
    #single_test = MultiplesTest
    if single_test is not None:
        suite = unittest.TestLoader().loadTestsFromTestCase(single_test)
        unittest.TextTestRunner(verbosity=1).run(suite)
    else:
        unittest.main()
