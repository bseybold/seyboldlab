import unittest
from seyboldlab.analysis.continuousanalysis import *
from numpy import *

class matrix_conversion_test(unittest.TestCase):
    def test_basic(self):
        #test output
        mat = convert_matrix_to_frames(array([[1,0],[0,1],[0,0]]).T,2)
        self.assertTrue(all(mat == array([[1,0,0,1],[0,1,0,0]]).T))
        #test shape change
        mat = convert_matrix_to_frames(zeros((10,100)),5)
        self.assertTrue(all(mat.shape == (50,96)))

    def test_dimensions_raises(self):
        with self.assertRaises(ValueError):
            convert_matrix_to_frames(zeros((2,2,2)),1)
        with self.assertRaises(ValueError):
            convert_matrix_to_frames(zeros((2,)),1)

    def test_rf_to_vector(self):
        #test output
        rf = convert_matrix_rf_to_vector(array([[0,1],[1,0]]).T)
        self.assertTrue(all(rf == array([0,1,1,0])))
        #test shape
        rf = convert_matrix_rf_to_vector(array([[0,1],[1,0],[0,0],[1,1]]).T)
        self.assertTrue(all(rf == array([0,1,1,0,0,0,1,1]).T))

    def test_rf_dimensions_raises(self):
        with self.assertRaises(ValueError):
            convert_matrix_rf_to_vector(zeros((2,2,2)))

    def test_rf_to_vector_vectorization(self):
        # with different dimensions
        arf_1 = array([[0,1],[1,0],[0,0]]).T
        arf_2 = array([[0,1,1],[0,0,0]]).T
        rf_1 = convert_matrix_rf_to_vector(arf_1)
        rf_2 = convert_matrix_rf_to_vector(arf_2)
        self.assertTrue(all(rf_1 == rf_2))
        # with different orientations
        rf_3 = convert_matrix_rf_to_vector(array([0,1,1]))
        rf_4 = convert_matrix_rf_to_vector(array([[0],[1],[1]]))
        self.assertTrue(all(rf_3 == rf_4))

    def test_string_fail(self):
        with self.assertRaises(AttributeError):
            #pass a string
            convert_matrix_to_frames('testing',2)

class convolution_tests(unittest.TestCase):
    def test_basic(self):
        rf = array([1,1])
        stim = array([[0,0],[0,1],[0,-1],[-1,1],[1,1]]).T
        conv = convolve_rf_and_stimulus(rf,stim)
        self.assertTrue(all(conv == array([0,1,-1,0,2])))

    def test_auto_reshape(self):
        rf = array([0,0,1,0])
        stim = array([[0,0],[0,1],[0,-1],[-1,1],[1,1]]).T
        conv = convolve_rf_and_stimulus(rf,stim)
        self.assertTrue(all(conv == array([0,0,-1,1])))

        rf = array([[0,0],[1,0]]).T
        conv2 = convolve_rf_and_stimulus(rf,stim)
        self.assertTrue(all(conv == conv2))

    def test_shape_errors(self):
        rf = array([0,1])
        bad_rf = array([[[0,1]],[[0,2]],[[0,3]]])
        stim = array([[0,2],[3,4]])
        bad_stim = array([0,2,3,4])

        with self.assertRaises(ValueError):
            convolve_rf_and_stimulus(rf,bad_stim)
        with self.assertRaises(ValueError):
            convolve_rf_and_stimulus(bad_rf,stim)

    def test_string_errors(self):
        rf = array([0,1])
        stim = array([[0,2],[3,4]])

        with self.assertRaises(AttributeError):
            convolve_rf_and_stimulus(rf,'bad')
        with self.assertRaises(AttributeError):
            convolve_rf_and_stimulus('bad',rf)

class STA_tests(unittest.TestCase):
    def test_try_with_identity_stimulus(self):
        conditions = 3
        stimulus_matrix = identity(conditions)
        for i in xrange(conditions):
             spikes = zeros((conditions,))
             spikes[i] = 1
             sta = calculate_sta(stimulus_matrix,spikes)
             self.assertTrue(i == argmax(sta))

    def test_vector_normalization(self):
        v = array([3,4])
        self.assertTrue( v.dot(v) == 25)
        v_n = v_norm(v)
        self.assertTrue( v_n.dot(v_n) == 1)
        o = array([4,-3])
        self.assertTrue( v.dot(o) == 0)
        self.assertAlmostEqual( v_n.dot(o),0 )

    def test_s_shape(self):
        v = random.random((10,))
        arr = zeros((4,10))
        m = s_shape(v,arr)
        self.assertEqual(arr.shape,m.shape)
        self.assertTrue(all(v[6] == m[:,6]))

    def test_sta_from_model_neurons(self):
        d1 = 5
        d2 = 10000
        conditions = 10
        for i in xrange(conditions):
            rf = zeros((d1,d1))
            stim = random.random((d1,d2))
            x1 = random.randint(d1)
            y1 = random.randint(d1)
            x2 = random.randint(d1)
            y2 = random.randint(d1)
            if x1 == x2 and y1 == y2:
                continue
            rf[x1,y1] = 1
            rf[x2,y2] = -1
            rf_1 = convert_matrix_rf_to_vector(rf)
            stim_1 = convert_matrix_to_frames(stim,d1)
            conv = convolve_rf_and_stimulus(rf_1,stim_1)
            nl_x = [conv.min(),conv.mean(),conv.max()]
            nl_y = [0,0,2]
            nlo = pass_through_nonlinearity(conv,nl_x,nl_y)
            sta = calculate_sta(stim_1,nlo)
            #x1+y1*d1 computes the linear index of the points we specified
            self.assertEqual(x1+y1*d1, sta.argmax())
            self.assertEqual(x2+y2*d1, sta.argmin())

    def test_sta_with_symmetric_nonlinearity(self):
        d1 = 5
        d2 = 10000
        conditions = 10
        for i in xrange(conditions):
            rf = zeros((d1,d1))
            stim = random.random((d1,d2))
            x1 = random.randint(d1)
            y1 = random.randint(d1)
            rf[x1,y1] = 1
            rf_1 = convert_matrix_rf_to_vector(rf)
            stim_1 = convert_matrix_to_frames(stim,d1)
            conv = convolve_rf_and_stimulus(rf_1,stim_1)
            nl_x = [conv.min(),conv.mean(),conv.max()]
            nl_y = [2,0,2]
            nlo = pass_through_nonlinearity(conv,nl_x,nl_y)
            sta = calculate_sta(stim_1,nlo)
            self.assertLess(abs(sta[x1+y1*d1]),10*abs(sta).mean())




if __name__ == '__main__':
    unittest.main()
    ##examples to run a single test

    #matrix_conversion_test('basic_test').run()

    #suite = unittest.TestSuite()
    #suite.addTest(matrix_conversion_test('basic_test'))
    #runner = unittest.TextTestRunner()
    #runner.run(suite)
