import unittest
from seyboldlab.stimuli.stimulus_descriptors import *
from numpy import *
import os

class ValidationTests(unittest.TestCase):
    def test_none(self):
        validate_none('anything')
        validate_none(1)
        validate_none([1,3,4])

        with self.assertRaises(TypeError):
            validate_none()
        with self.assertRaises(TypeError):
            validate_none(1,2)

    def test_number(self):
        with self.assertRaises(ValueError):
            validate_number('anything')

        validate_number(1)
        validate_number([1,3,4])

        with self.assertRaises(TypeError):
            validate_none()
        with self.assertRaises(TypeError):
            validate_none(1,2)

    def test_string(self):
        validate_string('anything')
        validate_string(('one','two'))
        with self.assertRaises(ValueError):
            validate_string(1)
        with self.assertRaises(ValueError):
            validate_string([1,3,4])

        with self.assertRaises(TypeError):
            validate_string()
        with self.assertRaises(TypeError):
            validate_string(1,2)

    def test_enum(self):
        validate_enum('anything',['anything'])
        validate_enum(('one','two'),('one','two','three'))
        validate_enum((1,2,3,1),(1,2,3,4))
        validate_enum(('one',1),('one',1))
        with self.assertRaises(ValueError):
            validate_enum(('one','two'),('one','three'))
        with self.assertRaises(ValueError):
            validate_enum(1,('one'))
        with self.assertRaises(ValueError):
            validate_enum([1,3,4],('one'))

        with self.assertRaises(TypeError):
            validate_enum()
        with self.assertRaises(TypeError):
            validate_enum(1,1,2)

class ToneTests(unittest.TestCase):
    #also tests SweepBase, SoundBase, and delcaratives

    def test_creation(self):
        t = Tone()
        t = Tone({'frequency':[2000,4000],'sweep_duration':[1,2]})
        test_dict = {}
        for key, value in t.default_parameters.items():
            Tone({key:value})
            test_dict[key] = value
        Tone(test_dict)

        with self.assertRaises(KeyError):
            Tone({'garbage_parameter':'garbage'})

    def test_buffer(self):
        t = Tone()
        trial_dict = t.get_trial_parameters()
        t.get_buffer(trial_dict)

        trial_dict['sweep_duration'] = 1
        buffer = t.get_buffer(trial_dict)
        trial_dict['sweep_duration'] = 2
        buffer_2 = t.get_buffer(trial_dict)
        trial_dict['sampling_rate'] /= 2
        buffer_3 = t.get_buffer(trial_dict)
        self.assertEqual(buffer.size*2,buffer_2.size)
        self.assertTrue(all(buffer == buffer_3))

        self.assertTrue(all(buffer == 0))

    def test_timing(self):
        t = Tone()
        trial_dict = t.get_trial_parameters()
        t.get_timing(trial_dict)

        trial_dict['sweep_duration'] = 1
        timing = t.get_buffer(trial_dict)
        trial_dict['sweep_duration'] = 2
        timing_2 = t.get_buffer(trial_dict)
        trial_dict['sampling_rate'] /= 2
        timing_3 = t.get_buffer(trial_dict)
        self.assertEqual(timing.size*2,timing_2.size)
        self.assertEqual(timing_3.size*2,timing_2.size)
        self.assertEqual(timing_2.max(),timing_3.max())
        self.assertEqual(diff(timing).mean(),diff(timing_2).mean())
        self.assertAlmostEqual(diff(timing_3).mean(),diff(timing).mean()*2)

    def test_frequency(self):
        t = Tone()
        trial_dict = t.get_trial_parameters()
        trial_dict['frequency'] = 2000
        trial_dict['sweep_duration'] = 1
        trial_dict['envelope_duration']=trial_dict['sweep_duration']
        trial_dict['gate_type'] = None
        w = t.get_waveform(trial_dict)
        time = arange(0,trial_dict['sweep_duration'],
                      1/float(trial_dict['sampling_rate']))
        sin_wave = sin(2*pi*time*2000)
        self.assertTrue(all(sin_wave - w < 0.0000001))
        p1 = power_at_frequency(w,trial_dict['sampling_rate'],2000)
        p2 = power_at_frequency(w,trial_dict['sampling_rate'],4000)
        self.assertAlmostEqual(p2,0)
        self.assertTrue(p1 > 10000*p2)
        trial_dict['frequency'] = 4000
        w = t.get_waveform(trial_dict)
        sin_wave = sin(2*pi*time*4000)
        self.assertTrue(all(sin_wave - w < 0.0000001))
        p1 = power_at_frequency(w,trial_dict['sampling_rate'],2000)
        p2 = power_at_frequency(w,trial_dict['sampling_rate'],4000)
        self.assertAlmostEqual(p1,0)
        self.assertTrue(p2 > 10000*p1)

class EnvelopeTest(unittest.TestCase):
    def test_envelope(self):
        t = Tone()
        trial_dict = t.get_trial_parameters()
        envelope = t.get_envelope(trial_dict)
        self.assertEqual(envelope.max(),1)

    def test_gain(self):
        t = Tone()
        trial_dict = t.get_trial_parameters()
        envelope = t.get_envelope(trial_dict)
        trial_dict_soft = trial_dict.copy()
        trial_dict_loud = trial_dict.copy()
        trial_dict_soft['gain'] = -20
        trial_dict_loud['gain'] = 20
        envelope_soft = t.get_envelope(trial_dict_soft)
        envelope_loud = t.get_envelope(trial_dict_loud)
        self.assertAlmostEqual(envelope_soft.max()*10,envelope.max(),3)
        self.assertAlmostEqual(envelope.max()*10,envelope_loud.max(),3)

    def test_envelope_duration(self):
        t = Tone()
        trial_dict = t.get_trial_parameters()
        envelope = t.get_envelope(trial_dict)
        trial_dict_long = trial_dict.copy()
        trial_dict_long['envelope_duration'] = 1
        trial_dict_long['gate_type'] = None
        trial_dict_short = trial_dict.copy()
        trial_dict_short['envelope_duration'] = 0.5
        trial_dict_short['gate_type'] = None
        envelope_long = t.get_envelope(trial_dict_long)
        envelope_short = t.get_envelope(trial_dict_short)
        ind_short = arange(0,trial_dict['sampling_rate']*0.5,dtype='i')
        ind_long  = arange(trial_dict['sampling_rate']*0.5,len(envelope),dtype='i')
        self.assertTrue(all(envelope_short[ind_short] == envelope_long[ind_short]))
        self.assertTrue(all(envelope_short[ind_long] == 0))
        self.assertTrue(all(envelope_long[ind_long] == 1))

    def test_envelope_offset(self):
        t = Tone()
        trial_dict = t.get_trial_parameters()
        envelope = t.get_envelope(trial_dict)
        trial_dict_long = trial_dict.copy()
        trial_dict_long['envelope_duration'] = 1
        trial_dict_long['gate_type'] = None
        trial_dict_short = trial_dict.copy()
        trial_dict_short['envelope_duration'] = 0.5
        trial_dict_short['gate_type'] = None
        envelope_long = t.get_envelope(trial_dict_long)
        envelope_short = t.get_envelope(trial_dict_short)
        ind_short = arange(0,trial_dict['sampling_rate']*0.5,dtype='i')
        ind_long  = arange(trial_dict['sampling_rate']*0.5,len(envelope),dtype='i')
        trial_dict_short_late = trial_dict.copy()
        trial_dict_short_late['envelope_offset'] = 0.5
        trial_dict_short_late['gate_type'] = None
        envelope_short_late = t.get_envelope(trial_dict_short_late)
        self.assertTrue(all(envelope_short_late[ind_short] == 0))
        self.assertTrue(all(envelope_short_late[ind_long] == 1))

    def test_gate_time_and_linear(self):
        t = Tone()
        trial_dict = t.get_trial_parameters()
        envelope = t.get_envelope(trial_dict)
        trial_dict_gate_ramp = trial_dict.copy()
        trial_dict_gate_ramp['gate_type'] = 'linear'
        trial_dict_gate_ramp['gate_time'] = trial_dict['envelope_duration']/2
        trial_dict_gate_ramp_short = trial_dict_gate_ramp.copy()
        trial_dict_gate_ramp_short['gate_time'] = trial_dict_gate_ramp['gate_time']/2
        envelope_ramp = t.get_envelope(trial_dict_gate_ramp)
        envelope_ramp_short = t.get_envelope(trial_dict_gate_ramp_short)
        ramp_len = ceil(trial_dict['sampling_rate']*trial_dict_gate_ramp['gate_time'])
        ramp_len_short = ceil(ramp_len/2.0)
        ramp = arange(0,1,1/float(ramp_len))
        ramp_short = arange(0,1,1/float(ramp_len_short))
        self.assertTrue(all(envelope_ramp[:ramp_len]==ramp))
        self.assertTrue(all(envelope_ramp_short[:ramp_len_short]==ramp_short))
        self.assertTrue(all(envelope_ramp[ramp_len:2*ramp_len]==ramp[::-1]))
        self.assertAlmostEqual(envelope_ramp.max(),1,4)
        self.assertEqual(envelope_ramp.min(),0)

    def test_gate_type_cosine(self):
        t = Tone()
        trial_dict = t.get_trial_parameters()
        envelope = t.get_envelope(trial_dict)
        trial_dict_gate_ramp = trial_dict.copy()
        trial_dict_gate_ramp['gate_type'] = 'linear'
        trial_dict_gate_ramp['gate_time'] = trial_dict['envelope_duration']/2
        trial_dict_gate_ramp_short = trial_dict_gate_ramp.copy()
        trial_dict_gate_ramp_short['gate_time'] = trial_dict_gate_ramp['gate_time']/2
        envelope_ramp = t.get_envelope(trial_dict_gate_ramp)
        envelope_ramp_short = t.get_envelope(trial_dict_gate_ramp_short)
        ramp_len = ceil(trial_dict['sampling_rate']*trial_dict_gate_ramp['gate_time'])
        ramp_len_short = ceil(ramp_len/2.0)
        ramp = arange(0,1,1/float(ramp_len))
        ramp_short = arange(0,1,1/float(ramp_len_short))
        trial_dict_gate_cos = trial_dict_gate_ramp.copy()
        trial_dict_gate_cos['gate_type'] = 'cosine'
        envelope_cosine = t.get_envelope(trial_dict_gate_cos)
        cos_gate = 0.5-cos(ramp/2*2*pi)/2
        self.assertTrue(all(envelope_cosine[:ramp_len]==cos_gate))
        self.assertTrue(all(envelope_cosine[ramp_len:2*ramp_len]==cos_gate[::-1]))
        self.assertAlmostEqual(envelope_cosine.max(),1,4)
        self.assertEqual(envelope_cosine.min(),0)

class NoiseTest(unittest.TestCase):

    def test_noise(self):
        n = Noise()
        trial_dict = n.get_trial_parameters()
        trial_dict['normalized_RMS'] = 0.5
        w = n.get_waveform(trial_dict)
        self.assertAlmostEqual(0.5, sqrt(mean(square(w))))
        trial_dict['normalized_RMS'] = 1
        w = n.get_waveform(trial_dict)
        self.assertAlmostEqual(1, sqrt(mean(square(w))))
        ##TODO: develope a test for 'white-ness' of the noise
        #trial_dict['sweep_duration'] = 10
        #trial_dict['envelope_duration'] = 10
        #w = n.get_waveform(trial_dict)
        #p1 = power_at_frequency(w,trial_dict['sampling_rate'],2000)
        #p2 = power_at_frequency(w,trial_dict['sampling_rate'],4000)
        #self.assertLess(abs(p1-p2) , p1/10)

    def test_bandpass_noise(self):
        bpn = BandpassNoise()
        self.assertEqual((4000,64000),bpn.compute_from_center(16000,4))
        trial_dict = bpn.get_trial_parameters()
        trial_dict['normalized_RMS'] = 0.5
        w = bpn.get_waveform(trial_dict)
        self.assertAlmostEqual(0.5, sqrt(mean(square(w))))
        trial_dict['normalized_RMS'] = 1
        w = bpn.get_waveform(trial_dict)
        self.assertAlmostEqual(1, sqrt(mean(square(w))))
        p1 = power_at_frequency(w,trial_dict['sampling_rate'],2000)
        p2 = power_at_frequency(w,trial_dict['sampling_rate'],4000)
        self.assertAlmostEqual(p1,0)
        self.assertTrue(p2 > 10000*p1)

    def test_banked_noise(self):
        fbn = FrequencyBankNoise()
        trial_dict = fbn.get_trial_parameters()

        trial_dict['frequency_start'] = 4000
        trial_dict['frequency_stop'] = 16000
        trial_dict['num_signals'] = 1
        trial_dict['frequency_scale'] = 'log'
        self.assertEqual(4000,fbn.generate_frequency_bank(trial_dict))
        w1 = fbn.get_waveform(trial_dict)

        trial_dict['frequency_start'] = 10000
        trial_dict['frequency_stop'] = 16000
        trial_dict['num_signals'] = 1
        trial_dict['frequency_scale'] = 'log'
        self.assertEqual(10000,fbn.generate_frequency_bank(trial_dict))
        w2 = fbn.get_waveform(trial_dict)

        trial_dict['frequency_start'] = 4000
        trial_dict['frequency_stop'] = 16000
        trial_dict['num_signals'] = 2
        trial_dict['frequency_scale'] = 'linear'
        self.assertTrue( (np.array((4000,10000))==fbn.generate_frequency_bank(trial_dict)).all())
        w3 = fbn.get_waveform(trial_dict)

        #normalize rms and compare, but not true because of different phases
        w12 = w1+w2
        rms12 = np.sqrt(np.mean(np.square(w12)))
        rms3 = np.sqrt(np.mean(np.square(w3)))
        import pdb; pdb.set_trace()  # XXX BREAKPOINT
        self.assertTrue(  (w3 == w12*(rms3/rms12)).all()) #should be true if phases set to 0, but not for some reason
        self.assertEqual( w3.size, trial_dict['sampling_rate']*trial_dict['sweep_duration']);

        trial_dict['frequency_start'] = 4000
        trial_dict['frequency_stop'] = 16000
        trial_dict['num_signals'] = 2
        trial_dict['frequency_scale'] = 'log'
        self.assertTrue( (np.array((4000,8000))==fbn.generate_frequency_bank(trial_dict)).all())

        trial_dict['normalized_RMS'] = 0.5
        w = fbn.get_waveform(trial_dict)
        self.assertAlmostEqual(0.5, sqrt(mean(square(w))))
        trial_dict['normalized_RMS'] = 1
        w = fbn.get_waveform(trial_dict)
        self.assertAlmostEqual(1, sqrt(mean(square(w))))

class MultiComponentTest(unittest.TestCase):
    def test_basic(self):
        t = Tone(static_parameters={'frequency':2000})
        t_2 = Tone(static_parameters={'frequency':4000})
        mc_1 = MultiComponentSweep(static_parameters={'components':(t,)})
        mc_2 = MultiComponentSweep(static_parameters={'components':(t_2,)})
        mc_3 = MultiComponentSweep(static_parameters={'components':(t,t_2)})

        trial_t = t.get_trial_parameters()
        w_t = t.get_waveform(trial_t)
        trial_mc_1 = mc_1.get_trial_parameters()
        w_mc_1 = mc_1.get_waveform(trial_mc_1)
        self.assertTrue(all(w_t==w_mc_1))

        trial_t_2 = t_2.get_trial_parameters()
        w_t_2 = t_2.get_waveform(trial_t_2)
        trial_mc_2 = mc_2.get_trial_parameters()
        w_mc_2 = mc_2.get_waveform(trial_mc_2)
        self.assertTrue(all(w_t_2==w_mc_2))

        trial_mc_3 = mc_3.get_trial_parameters()
        w_mc_3 = mc_3.get_waveform(trial_mc_3)
        self.assertTrue(all((w_t+w_t_2)==w_mc_3))
        mc_3.get_combinations()

        with self.assertRaises(ValueError):
            t_4 = Tone(dynamic_parameters = {'sweep_duration':(1,2)})
            mc_4 = MultiComponentSweep(static_parameters={'components':(t_4,)})
            mc_4.get_combinations()

class DynamicMovingRippleTest(unittest.TestCase):
    def setUp(self):
        self.parameters = {
            'cycles_per_octave_max':4,
            'cycles_per_octave_rate':0.5,
            'octaves_per_second_max':40,
            'octaves_per_second_rate':0.3,
            'modulation_depth':40,
            'sampling_rate':4,
            'parameter_interpolation':'linear',
            'duration':10,
        }

    def test_init(self):
        dmr = DynamicMovingRipple(self.parameters)
        self.assertEqual(len(dmr.cycles_per_octave),
                np.ceil(dmr.parameters['duration']
                    *dmr.parameters['cycles_per_octave_rate'])+1)
        self.assertEqual(len(dmr.octaves_per_second),
                np.ceil(dmr.parameters['duration']
                    *dmr.parameters['octaves_per_second_rate'])+1)

    def test_parameter_interpolations_vary_rate(self):
        dmr = DynamicMovingRipple(self.parameters)
        target1 = np.array([
            0,0.125,0.25,0.375,
            0.5,0.625,0.75,0.875,
            1.0,1.125,1.25,1.375,
            1.5,1.625,1.75,1.875,
            ])
        target4 = np.array([
            0,0.11,0.22,
            0.33,0.44,0.55,
            0.66,0.77,0.88,
            1,1.11,1.22,
            ])
        self.parameters.update({
            'sampling_rate':4,
            'parameter_start_time':0,
            'parameter_end_time':4,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.5,
            })
        o1 = dmr.interpolate_points(self.parameters)
        self.parameters.update({
            'sampling_rate':3,
            'parameter_start_time':0,
            'parameter_start_time':0,
            'parameter_end_time':4,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.33,
            })
        o4 = dmr.interpolate_points(self.parameters)
        self.assertTrue((o1 == target1).all())
        self.assertTrue((o4 == target4).all())

    def test_chunked_interpolation(self):
        dmr = DynamicMovingRipple(self.parameters)
        self.parameters.update({
            'sampling_rate':4,
            'parameter_start_time':0,
            'parameter_end_time':4,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.5,
            })
        o1 = dmr.interpolate_points(self.parameters)
        self.parameters.update({
            'parameter_start_time':0,
            'parameter_end_time':2,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.5,
            })
        o2 = dmr.interpolate_points(self.parameters)
        self.parameters.update({
            'parameter_start_time':2,
            'parameter_end_time':4,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.5,
            })
        o3 = dmr.interpolate_points(self.parameters)
        self.assertTrue((o1 == np.concatenate((o2,o3))).all())

    def test_chunked_integration(self):
        dmr = DynamicMovingRipple(self.parameters)
        self.parameters.update({
            'sampling_rate':4,
            'parameter_start_time':0,
            'parameter_end_time':4,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.5,
            })
        o1 = dmr.integrate_temporal_modulation(self.parameters)
        del self.parameters['parameter_points']
        dmr = DynamicMovingRipple(self.parameters)
        self.parameters.update({
            'parameter_start_time':0,
            'parameter_end_time':2,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.5,
            })
        o2 = dmr.integrate_temporal_modulation(self.parameters)
        self.parameters.update({
            'parameter_start_time':2,
            'parameter_end_time':4,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.5,
            })
        o3 = dmr.integrate_temporal_modulation(self.parameters)
        self.assertTrue((o1 - np.concatenate((o2,o3)) < 0.001).all())

    def test_chunked_integration_with_fast_rates(self):
        dmr = DynamicMovingRipple(self.parameters)
        self.parameters.update({
            'sampling_rate':4,
            'parameter_start_time':0,
            'parameter_end_time':6,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':3,
            })
        o1 = dmr.integrate_temporal_modulation(self.parameters)
        del self.parameters['parameter_points']
        dmr = DynamicMovingRipple(self.parameters)
        self.parameters.update({
            'parameter_start_time':0,
            'parameter_end_time':3,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':3,
            })
        o2 = dmr.integrate_temporal_modulation(self.parameters)
        self.parameters.update({
            'parameter_start_time':3,
            'parameter_end_time':6,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':3,
            })
        o3 = dmr.integrate_temporal_modulation(self.parameters)
        self.assertTrue((o1 - np.concatenate((o2,o3)) < 0.001).all())

    def test_chunked_integration_with_odd_rates(self):
        dmr = DynamicMovingRipple(self.parameters)
        self.parameters.update({
            'sampling_rate':4,
            'parameter_start_time':0,
            'parameter_end_time':6,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.3,
            })
        o1 = dmr.integrate_temporal_modulation(self.parameters)
        del self.parameters['parameter_points']
        dmr = DynamicMovingRipple(self.parameters)
        self.parameters.update({
            'parameter_start_time':0,
            'parameter_end_time':3,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.3,
            })
        o2 = dmr.integrate_temporal_modulation(self.parameters)
        self.parameters.update({
            'parameter_start_time':3,
            'parameter_end_time':6,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.3,
            })
        o3 = dmr.integrate_temporal_modulation(self.parameters)
        self.assertTrue((o1 - np.concatenate((o2,o3)) < 0.001).all())

    def test_chunked_waveform_and_spectrogram(self):
        dmr = DynamicMovingRipple(self.parameters)
        self.parameters.update({
            'sampling_rate':4,
            'parameter_start_time':0,
            'parameter_end_time':6,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.85,
            })
        o1,s1 = dmr.get_waveform_and_spectrogram(self.parameters)
        del self.parameters['parameter_points']
        dmr = DynamicMovingRipple(self.parameters)
        self.parameters.update({
            'parameter_start_time':0,
            'parameter_end_time':3,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.3,
            })
        o2,s2 = dmr.get_waveform_and_spectrogram(self.parameters)
        self.parameters.update({
            'parameter_start_time':3,
            'parameter_end_time':6,
            'parameter_points':[0,1,2,3,4,5],
            'parameter_rate':0.3,
            })
        o3,s3 = dmr.get_waveform_and_spectrogram(self.parameters)
        self.assertTrue((o1 - np.concatenate((o2,o3)) < 0.001).all())
        self.assertTrue((s1 - np.concatenate((s2,s3),1) < 0.001).all())

class ChunkedWriteToFileTest(unittest.TestCase):
    def setUp(self):
        self.out_wave = 'bs_testing_unittest_output_waveform';
        self.out_spec = 'bs_testing_unittest_output_spectrogram';
        self.parameters = {
            'cycles_per_octave_max':4,
            'cycles_per_octave_rate':0.5,
            'octaves_per_second_max':40,
            'octaves_per_second_rate':0.3,
            'modulation_depth':40,
            'sampling_rate':40,
            'parameter_interpolation':'linear',
            'duration':21,
            'output_file':self.out_wave,
            'spectrogram_file':self.out_spec,
            'parameter_chunk_time_step':1,
        }

    def tearDown(self):
        try:
            os.remove(self.out_wave)
        except:
            pass
        try:
            os.remove(self.out_spec)
        except:
            pass

    def test_basic_write_read_cycle(self):
        dmr = DynamicMovingRipple(self.parameters)
        dmr.write_waveform_and_spectrogram_in_chunks(self.parameters)
        self.parameters['parameter_start_time'] = 0
        self.parameters['parameter_end_time'] = self.parameters['duration']
        o1,s1 = dmr.get_waveform_and_spectrogram(self.parameters)
        fo1 = fromfile(self.out_wave)
        fs1 = fromfile(self.out_spec)
        fs1 = fs1.reshape( (dmr.frequency_bank.size, fs1.size/dmr.frequency_bank.size),order='F')
        import pdb; pdb.set_trace()  # XXX BREAKPOINT
        self.assertTrue(((o1 - fo1)<0.00001).all())
        self.assertTrue(((s1 - fs1)<0.00001).all())

if __name__ == "__main__":
    single_test = None
    ##comment or uncomment to allow testing of a single test
    single_test = NoiseTest
    #single_test = ChunkedWriteToFileTest
    #single_test = DynamicMovingRippleTest
    if single_test is not None:
        suite = unittest.TestLoader().loadTestsFromTestCase(single_test)
        unittest.TextTestRunner(verbosity=1).run(suite)
    else:
        unittest.main()

    #unittest.main()
