import sys

isPy3 = (sys.version_info.major == 3)

#python3 shivs
if isPy3:
    from io import StringIO
    exec23 = getattr(builtins,'exec')
else:
    import StringIO
    #stolen from the python module six
    def exec23(_code_, _globals_ = None, _locals_= None):
        if _globals_ is None:
            frame = sys._getframe(1)
            _globals_ = frame.f_globals
            if _locals_ is None:
                _locals_ = frame.f_locals
            del frame
        elif _locals_ is None:
            _locals_ = _globals_
        exec('''exec _code_ in _globals_, _locals_''')

#workaround to support python2 and python3 metaclasses
#Base = Meta('Base',(object,),{})
#then subclasses of the Base, will have the Meta class

def ensure_group(argument):
    '''coerce any input into an iterable group (particularly strings)'''
    if (not hasattr(argument,'__iter__') or isinstance(argument,str)
        or isinstance(argument,basestring)):
        return (argument,)
    return argument


