import bottle

app = bottle.Bottle()

@app.route('/')
def base():
    return 'test was successful'

@app.route('/json')
def json_test():
    return {'object':1,'other':123}

@app.post('/loopback')
def loopback():
    return request.params
