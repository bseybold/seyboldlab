import bottle
import importlib

root_app = bottle.Bottle()

#specifgy apps in the following manner
# {'mounting_name':{name='module_name',app='module_app_name'}}
# eg. {'awesome_url':{name='awesome_module',app='app'}}
app_dict = {
    'test':{
        'name':'server_test',
        'app':'app',
    },
    'stimuli':{
        'name':'stimulus_server',
        'app':'app',
    },
}

server_host = "192.168.42.76"
server_port = 8000
debug       = True
static_dir  = '/home/bryan/python_dev/seybold_lab/seyboldlab/tools/webserver/static/'
app_dir     = '/home/bryan/python_dev/seybold_lab/seyboldlab/tools/webserver/'

for key in app_dict.keys():
    try:
        app_name = app_dict[key]['name']
        app_app  = app_dict[key]['app']
        app = importlib.import_module('.'+app_name,'seyboldlab.tools.webserver').load_app()
        root_app.mount('/{name}/'.format(name=key),app.__getattribute__(app_app))
        app_dict[key]['mounted']='successfully'
    except Exception as e:
        import traceback
        print(traceback.format_exc())
        app_dict[key]['error'] = traceback.format_exc()

#serve static files from static_dir
@root_app.route('/static/<filename:path>')
def server_static(filename):
    return bottle.static_file(filename, root=static_dir)

#serve a json file of the status of loading various applications
@root_app.route('/server_status')
def list_current_apps():
    #dicts are automatically serialized to json
    return app_dict

bottle.run(root_app, host=server_host, port=server_port, debug=debug)
