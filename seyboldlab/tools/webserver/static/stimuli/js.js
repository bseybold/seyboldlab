function handle_error(xhr,error){
	alert('a server error occurred')	
};

function handle_response(response){
	if(response.stimuli){
		injection = '<div id="stimuli"><h2>Available Stimuli</h2><ul>';
		for(var i=0; i <response.stimuli.length; i++){
			injection += '<li>'+response.stimuli[i]+'</li>';
		};			
		injection += '</ul></div>';
		$('#content').html(injection);
		$('#stimuli').on('click','li',stimuli_clicked);
	} else if(response.stimulus){
		injection = '<div id="stimulus"><h2>'+response.stimulus+
			'</h2><div id="parameters"></div></div>';
		$('#content').html(injection);
		injection = '<h3>parameters</h3><ul>';
		for(var key in response['static parameters']){
			if(key in response['dynamic parameters']){
				injection += '<li><strong>'+key+'</strong>: '+response['dynamic parameters'][key].join(', ')+'</li>'
			} else {
				injection += '<li><strong>'+key+'</strong>: '+response['static parameters'][key]+'</li>'
			};
		};
		$('#parameters').html(injection);
		$('#parameters').on('click','li',parameter_clicked);
//	} else if(response.error) {
//		injection = '<div id="error"><h2>Error:</h2><p>'+
//			response.error+'</p></div>';
//		var re = new RegExp('\n','g')
//		injection = injection.replace(re,'<br>');
//		//$('#content').append(injection);
//		$('#messages').html(injection);
	};

	if(response.message) {
		injection = '<div id="message"><p>'+
			response.message+'</p></div>';
		var re = new RegExp('\n','g')
		injection = injection.replace(re,'<br>');
		$('#messages').html(injection);
	} else {
		$('#messages').html('<p>operating normally</p>');
	};
};

function parameter_clicked(which){
	parameter_line = which.currentTarget.textContent;
	parameter = parameter_line.substr(0, parameter_line.indexOf(':'));
	var value = window.prompt('Choose a new value for '+parameter);
	if(value != null){
		request = {
			'main':'set_parameters',
			'stimulus':$('#stimulus h2').text(),
			'parameter':parameter,
			'value':value,
		};
		query_server(request);
	};
};

function stimuli_clicked(which){
	stimulus = which.currentTarget.textContent;
	request = {
		'main':'initialize',
		'stimulus':stimulus
	};
	query_server(request);
};

function query_server(request){
	$.ajax({
		url:'/json',
		data:request,
		success:handle_response,
		error:handle_error
	});
};

function new_call(){
	request = {'main':'get_sweep_stimuli'};	
	query_server(request);
};

function load_call(){
	request = {'main':'get_loadable_files'};	
	if(request.filename){
		query_server(request);
	};
};

function save_call(){
	request = {'main':'save'};	
	var value = window.prompt('Choose a filename ');
	if(value != null){
		request.filename=value;
		query_server(request);
	};
};

$( document).ready(function(){
	$('#button_new').on('click',new_call);	
	$('#button_load').on('click',load_call);	
	$('#button_save').on('click',save_call);	
	new_call()
});
