from ...stimuli import stimulus_descriptors as sd
from .bottle import Bottle, static_file, redirect, run, request
from . import server_settings
import traceback
import numpy as np
import scipy as sp

class StimuliHandler():
    def __init__(self):
        self.all_stimuli = {
                stimulus_module for stimulus_module in sd.__dict__.values()
                if hasattr(stimulus_module, 'valid_stimulus') and
                stimulus_module.valid_stimulus == True
                }
        self.sweep_stimuli = {
                stimulus_module for stimulus_module in self.all_stimuli
                if sd.SweepBase in stimulus_module.__mro__
                }
        self.continuous_stimuli = {
                stimulus_module for stimulus_module in self.all_stimuli
                if sd.ContinuousBase in stimulus_module.__mro__
                }
        self.stimuli = {stimulus_module.__name__:stimulus_module
                        for stimulus_module in self.all_stimuli }
        self.current_stimulus = None

    def handle(self,request):
        response = {}
        try:
            if request['main'] == 'get_continuous_stimuli':
                response['stimuli'] = [ stimulus.__name__ for stimulus in
                                        self.continuous_stimuli ]
            if request['main'] == 'get_sweep_stimuli':
                response['stimuli'] = [ stimulus.__name__ for stimulus in
                                        self.sweep_stimuli ]
            if request['main'] == 'initialize':
                response['stimulus'] = request['stimulus']
                #create a new instance
                self.current_stimulus = self.stimuli[request['stimulus']]()
                response['static parameters'] = self.current_stimulus.static_parameters
                response['dynamic parameters'] = self.current_stimulus.dynamic_parameters
                print('initializing stimulus')

            if (request['main'] == 'set_parameters' and
                    self.current_stimulus.__class__.__name__ == request['stimulus']):
                    #import pdb; pdb.set_trace()  # XXX BREAKPOINT
                #for parameter,value in request['parameters']:
                    parameter,value = request['parameter'],request['value']
                    if type(value) == str or type(value) == unicode:
                        #convert values in a dangerous way (eval is not safe for public use)
                        value = eval(value)
                    try:
                        if len(value) > 1 and not (type(value) == str or type(value) == unicode) :
                            #handle dynamic parameters
                            self.current_stimulus.dynamic_parameters[parameter] = value.tolist()
                        else:
                            #handle newly modified, static parameters
                            self.current_stimulus.static_parameters[parameter] = value
                            if parameter in self.current_stimulus.dynamic_parameters:
                                del self.current_stimulus.dynamic_parameters[parameter]
                    except TypeError:
                        print('Error parsing parameter value: {}'.format(value))
                        #assume it was supposed to be static
                        self.current_stimulus.static_parameters[parameter] = value
                        if parameter in self.current_stimulus.dynamic_parameters:
                            del self.current_stimulus.dynamic_parameters[parameter]
                    response['stimulus'] = request['stimulus']
                    response['static parameters'] = self.current_stimulus.static_parameters
                    response['dynamic parameters'] = self.current_stimulus.dynamic_parameters
                #return parameters
                #response['parameters'] = self.current_stimulus.default_parameters

            if request['main'] == 'save':
                pass
                #self.current_stimulus.save(request['filename'])

            if request['main'] == 'get_loadable_files':
                pass

            if request['main'] == 'load':
                pass
                #if 'additional_directories' in request:
                #    self.current_stimulus = sd.load_stimulus(request['filename'], request['additional_directories'])
                #else:
                #    self.current_stimulus = sd.load_stimulus(request['filename'])
                #response['stimulus'] = self.current_stimulus.__class__.__name__
                #response['static parameters'] = self.stimuli[request['stimulus']].static_parameters
                #response['dynamic parameters'] = self.stimuli[request['stimulus']].dynamic_parameters

            if request['main'] == 'play':
                pass
        except Exception:
            response['message'] = traceback.format_exc()
            print(response['message'])

        return response

def load_app():
    app = Bottle()
    current_handler = StimuliHandler()
    @app.route('/')
    def base():
        #return 'testing two'
        return static_file('stimuli/index.html',root=server_settings.static_dir)
    @app.route('/js')
    def base():
        #return 'testing two'
        return static_file('stimuli/js.js',root=server_settings.static_dir)
    @app.route('/css')
    def base():
        #return 'testing two'
        return static_file('stimuli/css.css',root=server_settings.static_dir)
    @app.route('/json')
    def json():
        request_dict = {key:value for key,value in request.params.items()}
        return current_handler.handle(request_dict)
    return app

def main():
    app = load_app()
    run(app,
        host     = server_settings.server_host,
        port     = server_settings.server_port,
        reloader = server_settings.reloader,
        debug    = server_settings.debug)

