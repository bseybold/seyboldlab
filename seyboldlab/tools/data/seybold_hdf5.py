import tables
import numpy

class RecordingFile():
    '''
    A class for recording files that use HDF5 to store data.

    Data is stored in HDF5 files via PyTables. Each group of channels is given
    a new table mounted on the root. Individual tables are accessed via
    RecordingChannelSets and created with open_recording_channels().
    '''
    def __init__(self,filename,mode='r',title=None,allow_overwrite=False):
        '''
        open a recording file for reading or writing

        inputs:
            filename:
                the name of the hdf5 file
            mode:
                read/write/append flags for pytables
            title:
                optional - set the title of the file
        '''
        self.filename = filename
        self.mode = mode
        if title is None:
            title = self.filename
        self.title = title
        if mode == 'w':
            import os.path
            if os.path.exists(filename) and not allow_overwrite:
                raise ValueError('File already exists and overwriting is not allowed')
        self.file = tables.open_file(filename, mode=mode, title=self.title)
        self.recording_channels = {}
        self.recording_events = {}
        self.recording_waveforms = {}
        self.is_closed = False
        self.attrs = self.file.root._v_attrs

    def __del__(self):
        '''ensure that we close the file'''
        if not self.is_closed:
            self.close()

    def __str__(self):
        '''return the hdf5 file's string method'''
        return self.file.__str__()

    def open_recording_channels(self, group, node, n_channels, title='', include_time=True,
                                dtype=numpy.float32,time_dtype=numpy.float32):
        '''create a new object for recording channels in the file

        inputs:
            group:
                the hdf5 group the data belongs to
            title:
                the title of the table the data will belong to
            n_channels:
                the number of points/channels at each sample time
            include_time:
                optional - default: True - whether or not to include the time
            dtype:
                optional - default: float32 - the datatype for the table
            time_dtype:
                optional - default: float32 - the datatype for the timepoints
        '''
        if self.is_closed:
            raise IOError('hdf5 file is closed')
        self.recording_channels[title] = RecordingChannelSet(
                parent_file=self,
                group=group,
                node=node,
                n_channels=n_channels,
                title=title,
                dtype=dtype,
                include_time=include_time,
                time_dtype=time_dtype)
        return self.recording_channels[title]

    def open_recording_events(self, group, node, n_columns, title='', include_time=True,
                                dtype=numpy.int16,time_dtype=numpy.float32):
        '''create a new object for recording events in the file

        same arguments as open_recording_channels
        '''
        if self.is_closed:
            raise IOError('hdf5 file is closed')
        self.recording_events[title] = RecordingEventSet(
                parent_file=self,
                group=group,
                node=node,
                n_columns=n_columns,
                title=title,
                dtype=dtype,
                include_time=include_time,
                time_dtype=time_dtype)
        return self.recording_events[title]

    def open_recording_waveforms(self, group, node, n_points, title='', include_time=True,
                                dtype=numpy.float32,time_dtype=numpy.float32):
        '''create a new object for recording waveforms in the file

        same arguments as open_recording_channels
        '''
        if self.is_closed:
            raise IOError('hdf5 file is closed')
        self.recording_waveforms[title] = RecordingWaveformSet(
                parent_file=self,
                group=group,
                node=node,
                n_points=n_points,
                title=title,
                dtype=dtype,
                include_time=include_time,
                time_dtype=time_dtype)
        return self.recording_waveforms[title]

    def close(self):
        '''close the hdf5 file'''
        self.file.flush()
        self.file.close()
        self.is_closed = True

    def set_attrs_from_dict(self, dict):
        '''
        set multiple attributes of the hdf5 table

        inputs:
            dict:
                the attributes to set and their values
        '''
        for key,item in dict.items():
            self.attrs.__setitem__(key,item)

    def get_file_structure(self):
        '''
        returns the file structure including Seybold types

        output:
            a list of tuples containing the title of the node, the type of data
            stored in the node, and the node itself.
            [ (title, type, node), ... ]
        '''
        type_list = []
        node_list = []
        title_list = []
        for node in self.file.walk_nodes():
            title_list.append(node._v_title)
            type_list.append(node._v_attrs.bs_table_type)
            node_list.append(node)
        return zip(title_list,type_list,node_list)

class RecordingChannelSet(object):
    '''
    A class for manipulated channel recordings using tables

    Channels recorded at the same time are stored as a single table. You should
    always include timestamps with your data, but this is not strictly enforced
    Data can only be appended to the table or removed from it. To store event
    data, append data as an "event type" and use the timestamps to record times.
    These tables do not need to be closed directly, instead close the
    RecordingFile.
    '''
    def __init__(self, parent_file, group, node, n_channels, title = '', dtype=numpy.float32,
                    include_time = True, time_dtype=numpy.float32):
        '''
        open a table corresponding to recording samples across channels

        inputs:
            parent_file:
                the RecordingFile object (for referencing the hdf5 file)
            group:
                the group this data will belong to
            title:
                the title for the table
            n_channels:
                the number of channels being recorded
            dtype:
                optional - default: float32 - sample value format
            include_time:
                optional - default: true - whether to store time information
            time_dtype:
                optional - default: float32 - timestamp value format
        '''
        #make persistent copies
        self.parent_file = parent_file
        self.group = group
        self.title = title
        self.n_channels = n_channels
        self.dtype = dtype
        self.include_time = include_time
        self.time_dtype = time_dtype

        if n_channels < 0:
            raise ValueError('cannot assign negative channels')
        #determine the columns in the hdf5 file
        columns = {}
        self.column_names = []
        self.column_formats = []
        offset = 0
        if include_time:
            offset = 1
            if time_dtype is numpy.float32:
                columns['time'] = tables.Float32Col( pos = 0)
                self.column_formats.append('f4')
            elif time_dtype is numpy.float64:
                columns['time'] = tables.Float64Col( pos = 0)
                self.column_formats.append('f8')
            else:
                raise TypeError('time_dtype is not supported')
            self.column_names.append('time')
        #determine the column types in the hdf5 file
        if dtype is numpy.float32:
            column_type = tables.Float32Col
            column_numpy_format = 'f4'
        elif dtype is numpy.float64:
            column_type = tables.Float64Col
            column_numpy_format = 'f8'
        elif dtype is numpy.int16:
            column_type = tables.Int16Col
            column_numpy_format = 'i2'
        else:
            raise TypeError('dtype is not supported')
        #add each channel as an appropriate column
        for ind in xrange(n_channels):
            column_name = self.channel_name(ind)
            self.column_names.append(column_name)
            columns[column_name] = column_type( pos =(ind + offset))
            self.column_formats.append(column_numpy_format)
        self.description = type('private_definition',(tables.IsDescription,),columns)
        #need to be careful of time
        self.row_dtype = numpy.format_parser(self.column_formats,self.column_names,'')
        #get/add group
        if '/{}'.format(group) in self.parent_file.file:
            self.group = parent_file.file.get_node('/{}'.format(group))
        else:
            self.group = parent_file.file.create_group('/',group,title)
        #get/add table
        if '/{}/{}'.format(group,node) in self.parent_file.file:
            self.table = parent_file.file.get_node('/{}/{}'.format(group,node))
            self.attrs = self.table.attrs
            self.confirm_type()
        else:
            self.table = parent_file.file.create_table(self.group,node,self.description,title)
            #alias the table's attrs for hdf5 attributes
            self.attrs = self.table.attrs
            self.attrs['bs_table_type'] = 'raw_multichannel_samples'

    def confirm_type(self):
        '''returns wether the open table is a multichannel recording'''
        if self.attrs['bs_table_type'] != 'raw_multichannel_samples':
            raise TypeError('Table is not multichannel recording: {}'.format(self))
        if 'time' in self.table.cols._v_colnames and self.include_time == False:
            raise ValueError('Table was originally created with timing')
        if 'time' not in self.table.cols._v_colnames and self.include_time == True:
            raise ValueError('Table was not originally created with timing')
        if set(self.table.cols._v_colnames) != set(self.column_names):
            raise ValueError('Table lists the wrong number of columns')

    def channel_name(self,index):
        '''returns a properly formatted channel name from an index'''
        return 'ch{:0>3}'.format(index)

    def append_data(self,data,timestamps=None):
        '''append data to the table (with timestamps)

        inputs:
            data:
                an Samples-by-Channels array of data, not including timestamp
            timestamps:
                optional - a Samples length vector of timestamps
        '''
        if 'r' in self.parent_file.mode:
            raise ValueError('cannot write to a file open for reading')
        if len(data.shape) == 1: #coerce into column vector
            data.shape = (data.shape[0],1)
        if data.shape[1] < self.n_channels or data.shape[1] > self.n_channels+1:
            raise TypeError('incoming data dimensionality does not match')
        #records need to be added in only as rows or columns unfortunately
        rec_arr = numpy.empty(data.shape[0],dtype=self.row_dtype)
        if self.include_time:
            rec_arr['time'] = timestamps
        for index in range(data.shape[1]):
            rec_arr[self.channel_name(index)] = data[:,index]
        self.table.append(rec_arr)
        self.table.flush()
        return data.shape[0]

    def read_data(self,time_min=-numpy.inf,time_max=numpy.inf,channels=None,return_records=False):
        '''read a select time range from the table

        inputs:
            time_min:
                optional - default: -inf - first time (inclusive) to return
            time_max:
                optional - default: +inf - last time (exclusive) to return
            channels:
                optional - default: all -  a list of channels to include
            return_records:
                optional - default: False - return the full record array instead
        '''
        search_set = []
        if time_min != -numpy.inf:
            search_set.append('(time >= {}) '.format(time_min))
        if time_max != numpy.inf:
            search_set.append('(time < {}) '.format(time_max))
        if search_set:
            search_string = ' & '.join(search_set)
            data = self.table.read_where(search_string)
        else:
            data = self.table.read()
        if return_records:
            return data
        else:
            if channels is None:
                channels = range(self.n_channels)
            out_data = numpy.empty((data.shape[0],len(channels)))
            for ind,chan in enumerate(channels):
                out_data[:,ind] = data[self.channel_name(chan)]
            if self.include_time:
                return (out_data, data['time'])
            else:
                return out_data

    def set_attrs_from_dict(self, dict):
        '''set multiple attributes of the hdf5 table'''
        for key,item in dict.items():
            self.attrs.__setitem__(key,item)

class RecordingWaveformSet(RecordingChannelSet):
    def __init__(self, parent_file, group, node, n_points, title='', dtype=numpy.float32,
                 include_time = True, time_dtype=numpy.float32):
        super(RecordingWaveformSet,self).__init__(parent_file, group, node, n_points, title,
                dtype, include_time, time_dtype)
        if self.attrs['bs_table_type'] != 'raw_waveform_snippets':
            #should only happen when adding the table, otherwise an error gets thrown
            self.attrs['bs_table_type'] = 'raw_waveform_snippets'

    def confirm_type(self):
        if self.attrs['bs_table_type'] != 'raw_waveform_snippets':
            raise TypeError('Table is not waveform snippets: {}'.format(self))
        if 'time' in self.table.cols._v_colnames and self.include_time == False:
            raise ValueError('Table was originally created with timing')
        if 'time' not in self.table.cols._v_colnames and self.include_time == True:
            raise ValueError('Table was not originally created with timing')
        if set(self.table.cols._v_colnames) != set(self.column_names):
            raise ValueError('Table lists the wrong number of columns')

    def channel_name(self,index):
        '''returns a properly formatted channel name from an index'''
        return 'pt{:0>3}'.format(index)

class RecordingEventSet(RecordingChannelSet):
    def __init__(self, parent_file, group, node, n_columns, title='', dtype=numpy.int16,
                 include_time = True, time_dtype=numpy.float32):
        '''
        set an attribute with the conversions for each column
        '''
        super(RecordingEventSet,self).__init__(parent_file, group, node, n_columns,
                title, dtype, include_time, time_dtype)
        if self.attrs['bs_table_type'] != 'raw_events':
            #should only happen when adding the table, otherwise an error gets thrown
            self.attrs['bs_table_type'] = 'raw_events'

    def confirm_type(self):
        if self.attrs['bs_table_type'] != 'raw_events':
            raise TypeError('Table is not raw events: {}'.format(self))
        if 'time' in self.table.cols._v_colnames and self.include_time == False:
            raise ValueError('Table was originally created with timing')
        if 'time' not in self.table.cols._v_colnames and self.include_time == True:
            raise ValueError('Table was not originally created with timing')
        if set(self.table.cols._v_colnames) != set(self.column_names):
            raise ValueError('Table lists the wrong number of columns')

    def channel_name(self,index):
        '''returns a properly formatted channel name from an index'''
        return 'ev{:0>3}'.format(index)
