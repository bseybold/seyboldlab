import numpy as np

def fft_cut(signal,fs,low_f,high_f):
    '''
    remove the specified frequencies in the frequency domain

    inputs:
        signal
            the waveform to remove frequencies from
        fs
            the sampling rate of the waveform
        low_f
            the lowest frequency to remove (inclusive)
        high_f
            the highest frequency to remove (exclusive)

    output:
        the filtered signal
    '''
    fft_o = np.fft.fft(signal)
    freqs = np.fft.fftfreq(signal.size, 1/float(fs))
    fft_o[ (freqs >= low_f) & (freqs < high_f) ] = 0
    fft_o[ (freqs <= -low_f) & (freqs > -high_f) ] = 0 #repeat for neg coefficient
    return np.fft.ifft(fft_o)

def power_at_frequency(signal,fs,frequency):
    '''
    return the power of a single frequency

    Note: this assumes that the signal is real, and therefore the positive and
    negative coefficients are equivalent in magnitude

    inputs:
        signal
            the waveform to measure from
        fs
            the samping rate of the waveform
        frequency
            the frequency of interest (will be rounded to the nearest frequency
            present in the dFFT)

    output:
        signal power (in dB/octave?)
    '''
    fft_o = np.fft.fft(signal)
    freqs = np.fft.fftfreq(signal.size, 1/float(fs))
    index = np.argmin( abs(freqs-frequency))
    r_val = abs(fft_o[index])*2 #due to matching of negative components
    return r_val
