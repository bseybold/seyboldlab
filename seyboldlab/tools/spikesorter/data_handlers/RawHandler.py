import numpy

class RawHandler(object):
    def get_next_samples(self,target):
        str = self.file.read(target*self.type_len)
        if str == '':
            return None
        else:
            return numpy.fromstring(str,dtype = self.datatype)

    def __init__(self, filename, type, fs):
        self.file = open(filename,'rb')
        self.datatype = type
        self.sampling_rate = fs
        if type == numpy.int16:
            self.type_len = 2

    def __del__(self):
        self.file.close()

class RawHandlerNeuralynx(RawHandler):
    def __init__(self, filename):
        super(RawHandlerNeuralynx,self).__init__(filename,numpy.int16,30303)
