import os
import numpy
import pythoncom
import win32com.client


signal_name = 'Wave'
trigger_name = 'Trig'
dtype = 'float32'
simult_blocks = 1000

def get_all_files(directory):
    ttank = open_block_from_dir(directory)
    groups = parse_block_notes(ttank)
    files = []
    for group in groups:
        for i in xrange(int(group['NumChan'])):
            files.append(group['StoreName']+str(i))
    ttank.CloseTank()
    ttank.ReleaseServer()
    del ttank
    return files

def get_file_list(directory):
    files = get_all_files(directory)
    files = [file for file in files if signal_name in file]
    return files

def get_trig_file(directory):
    files = get_all_files(directory)
    files = [file for file in files if trigger_name in file]
    return files

def get_handler(file = ''):
    return TDTHandler


def safe_split(directory):
    head, tail = os.path.split(directory)
    if not tail: #only happens for trailing slash
        head, tail = os.path.split(head) 
    return head, tail

def open_block_from_dir(directory):
    tank_path, block_name = safe_split(directory)
    pythoncom.CoInitialize()
    ttank = win32com.client.Dispatch('TTank.X')
    try:
        if not ttank.ConnectServer('Local','Me'):
            raise RuntimeError('Could not connect to TTank Server')
        if not ttank.OpenTank(tank_path,'R'):
            raise RuntimeError('Could not open TTank')
        if not ttank.SelectBlock(block_name):
            raise RuntimeError('Could not open block')
    except RuntimeError:
        ttank.CloseTank()
        ttank.ReleaseServer()
    return ttank
     
def parse_block_notes(ttank):
    text = ttank.CurBlockNotes.split('\r\n')
    if text == ['']:
        raise ValueError('This is not a TDT block: {}'.format(
                        ttank.CurBlockName))
    notes = []
    current_dict = {}
    for line in text:
        if line[0] == '[':
            if current_dict: #skip this the first time
                notes.append(current_dict)
                current_dict = {}  #the first line will always trigger this
            line = line[line.find(']')+1:] #trim off header
        if not line[0] == '[': #only on last line is there a double [][]
            parts = line.split(';')
            name, type, value = [p.split('=')[1] for p in parts[:3]]
            if type == 'L':
                value = float(value)
            current_dict[name] = value
    return notes


class TDTHandler():
    def get_next_samples(self,target):
        if self.at_eof:
            return None
        if numpy.all(self.last_block):
            remains_of_current_block = self.valid_samples-self.current_index
        else:
            remains_of_current_block = 0
        needed_blocks = (target-remains_of_current_block)/float(
                self.valid_samples)
        data = numpy.zeros(target+self.valid_samples,dtype=dtype)
        # assign the tail to the current data block
        if numpy.all(self.last_block):
            data_start = self.last_block[self.current_index:]
            data[:len(data_start)] = data_start
            current_samples = len(data_start)
        else:
            current_samples = 0
       
        for counter in range(int(numpy.ceil(needed_blocks))):
            # note that calling next_block modifies self.last_block
            next_block = self.read_next_block()
            if next_block == None:
                self.at_eof == True
                break
            data[current_samples:current_samples+len(next_block)] = next_block
            current_samples += len(next_block)
        data = data[:current_samples]
        self.current_index += target
        #you can't just use modulo because you need to wait till the
        #next block is read first
        if self.current_index > self.valid_samples:
            self.current_index %= self.valid_samples
            if self.current_index == 0:
                self.read_next_block()
        return data[:int(target)]

    def read_next_block(self):
        if self.at_eof:
            return None
        evs = 0
        while evs == 0:
            #evs = self.ttank.ReadEvents(simult_blocks,
            #                        self.ev,self.channel_number+1, 0,
            #                        self.current_time,0,0)
            evs = self.ttank.ReadEvents(simult_blocks,
                            self.ev, self.channel_number+1, 0,
                            0, 0, 1) #only ask for new blocks las parameter
            self.current_time += (self.valid_samples / self.sampling_rate)
            data = numpy.array(self.ttank.ParseEvV(0,evs), dtype=dtype)
            if data.shape != ():
                data = data.T.reshape(data.shape[0]*data.shape[1])
                if (numpy.any(self.last_block) and
                        numpy.all(data == self.last_block)):
                    evs = 0
                else:
                    self.last_block = data
            else:
                print 'got nothing'
                break
        print self.stop_time - self.current_time 
        if self.current_time >= self.stop_time:
            self.at_eof = True
        return data
        


    def to_raw(self, out_file):
        self.skip_to_block(0)
        block = self.read_next_block()
        with open(out_file,'wb') as ofile:
            while block:
                ofile.write(block.tostring())
                block = self.read_next_block()

            
    def skip_to_block(self,target):
        self.current_index = target%self.valid_samples
        self.current_time = (self.starting_time + 
                target * self.valid_samples / self.sampling_rate)

    def get_data(self):
        data = {'block_notes':self.notes}
        data['group'] = self.group
        data['filename'] = self.filename
        data['channel_number'] = self.channel_number
        data['starting_time'] = self.starting_time
        data['stoping_time'] = self.stop_time
        data['sampling_rate'] = self.sampling_rate
        return data

        
    def __init__(self, filename):
        directory, channel = safe_split(filename)
        self.at_eof = False
        if not directory:
            raise ValueError('filename must be a full path')
        self.ttank = open_block_from_dir(directory)
        self.filename = filename
        self.group = channel[:4] #first four characters are which store
        self.channel_number = int(channel[4:]) #rest are channel number
        self.filename = filename #must be full path
        self.current_index = 0

        self.ev = self.ttank.StringToEvCode(self.group)
        self.starting_time = self.ttank.CurBlockStartTime
        self.stop_time = self.ttank.CurBlockStopTime
        self.max_return = self.ttank.GetGlobalB('MaxReturn')
        self.current_time = self.starting_time
        
        self.notes = parse_block_notes(self.ttank)
        for note in self.notes:
            if note['StoreName'] == self.group:
                self.sampling_rate = note['SampleFreq']
                self.valid_samples = note['NumPoints'] * simult_blocks 
        
        self.last_block = None
        self.skip_to_block(0)

    def __del__(self):
        if self.ttank:
            self.ttank.CloseTank()
            self.ttank.ReleaseServer()
