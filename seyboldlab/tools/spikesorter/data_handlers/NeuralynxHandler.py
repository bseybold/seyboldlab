import struct
import array
import math
import numpy
import os
    # block format for Neuralynx .csc files
    # uInt64 - timestamp (us)
    # uInt32 - channel number
    # uInt32 - sampling rate
    # uInt32 - number of valid samples
    
    # Int16 [] - all of the sample values (generally 512)

def get_file_list(directory):
    files = os.listdir(directory)
    return [file for file in files if 
            ('.ncs' in file and 'trig' not in file.lower())]

def get_trig_file(directory):
    files = os.listdir(directory)
    return [file for file in files if 
            ('.ncs' in file and 'trig' in file.lower())]

def get_handler(file):
    return NeuralynxHandler

class NeuralynxHandler():
    def get_next_samples(self,target):
        if self.at_eof:
            return None
        if self.last_block:
            remains_of_current_block = self.valid_samples-self.current_index
        else:
            remains_of_current_block = 0
        needed_blocks = (target-remains_of_current_block)/float(
                self.valid_samples)
        data = numpy.zeros(target+self.valid_samples,dtype=numpy.int16)
        # assign the tail to the current data block
        if self.last_block:
            data_start = self.last_block['data'][self.current_index:]
            data[:len(data_start)] = data_start
            current_samples = len(data_start)
        else:
            current_samples = 0
       
        for counter in range(int(math.ceil(needed_blocks))):
            # note that calling next_block modifies self.last_block
            next_block = self.read_next_block()
            if next_block == None:
                self.at_eof == True
                break
            data[current_samples:current_samples+len(next_block)] = next_block
            current_samples += len(next_block)
        data = data[:current_samples]
        self.current_index += target
        #you can't just use modulo because you need to wait till the next block
        #is read first
        if self.current_index > self.valid_samples:
            self.current_index %= self.valid_samples
            if self.current_index == 0:
                self.read_next_block()
        #this seems wrong once the next block is read
        return data[:int(target)]

    def read_next_block(self, just_data = True, reopen_file = False):
        if reopen_file:
            with open(self.filename,'rb') as file:
                file.seek(self._file_pos)
                raw_data = file.read(self.headerSize)
                if raw_data:
                    data = struct.unpack(self.headerStructString,raw_data)
                    block = {'timestamp':data[0],'channel number':data[1],
                             'sample rate':data[2], 'valid samples':data[3]}
                    data = file.read(2*block['valid samples'])
                    self._file_pos = file.tell()
                    data = struct.unpack('h'*(len(data)/2),data)
                    block['data'] = numpy.array(data,dtype=numpy.int16)
                    self.last_block = block
                    if just_data:
                        return block['data']
                    else:
                        return block
                else:
                    self.at_eof = True
                    return None
        else:
            raw_data = self.file.read(self.headerSize)
            if raw_data:
                data = struct.unpack(self.headerStructString,raw_data)
                block = {'timestamp':data[0],'channel number':data[1],
                         'sample rate':data[2], 'valid samples':data[3]}
                data = self.file.read(2*block['valid samples'])
                self._file_pos = self.file.tell()
                data = struct.unpack('h'*(len(data)/2),data)
                block['data'] = array.array('h',data)
                self.last_block = block
                if just_data:
                    return block['data']
                else:
                    return block
            else:
                self.at_eof = True
                return None

    def to_raw(self, out_file):
        self.skip_to_block(0)
        block = self.read_next_block()
        with open(out_file,'wb') as ofile:
            while block:
                ofile.write(block.tostring())
                block = self.read_next_block()

            
    def skip_to_block(self,target):
        if not self._assume_512_blocks:
            self._file_pos = 16384
            count = 1
            while count < target:
                self.read_next_block()
            #raise ValueError('Cannot skip blocks without assuming 512 size')
        else:
            self._file_pos = self.blockSize*target + 16384
        if self.file:
            self.file.seek(self._file_pos)
            
    def __init__(self,filename, reopen_file = False):
        self.at_eof = False
        self.filename = filename
        self.current_block = 0
        self.headerStructString = 'QLLL'
        self.headerSize = 20
        self.current_index = 0
        self._assume_512_blocks = False
        if not reopen_file:
            self.file = open(filename,'rb')
        else:
            self.file = None

        self.skip_to_block(0)
        block = self.read_next_block(just_data = False)
        self.starting_time = block['timestamp']
        self.channel_number = block['channel number']
        if block['sample rate'] == 30303:
            self.sampling_rate = 30303.13
        else:
            self.sampling_rate = block['sample rate']
        if block['valid samples'] == 512:
            self._assume_512_blocks = True
            self.blockSize = self.headerSize + 2*512 # int16 * 512
        self.valid_samples = 512
        self.skip_to_block(0)
        self.last_block = None

    def __del__(self):
        if self.file:
            self.file.close()
