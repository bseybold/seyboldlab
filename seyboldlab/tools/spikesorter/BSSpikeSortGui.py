from PyQt4 import QtCore, QtGui
from BSSpikeSortGuiWindow import Ui_MainWindow
import BSSpikeSort
import os
import importlib
import matplotlib.pyplot as pyplot
import sys
import numpy
import pythoncom

# plugins for loading data ('string name',module)
data_import_modules = [
        ('BatchResults.mat','os'),
        ('NeuralynxCSC.ncs','NeuralynxHandler'),
        ('TDT.ttank','TDTHandler'),
        ]

clustering_methods = [
        ('Klusta Kwik','klusta_kwik'),
        ('Mean Shift','sk_mean_shift'),
        ('Dirlicht Process Gaussian Mixture Model','sk_DPGMM'),
        ('Affinity Propagation','sk_affinity_prop'),
        ]

# description of colors for plotting (rgb, [0,1])
colors = [(0,0,1),
          (1,0,0),
          (0,1,0),
          (0,1,1),
          (1,1,0),
          (1,0,1),
          (0.25,0.25,0.25),
          (0.5,0.75,0),
          (0,0.75,0.5),
          (0.75,0,0.5),
          (0,0.75,0.5),]

completions = {
        'Folder Selected':False,
        'Channel Selected':False,
        'Snippets Collected':False,
        'Features Calculated':False,
        'Clustering Run':False,
        'Analysis Completed':False}

sample_points = 500

#this exists in numpy 1.7 not the version I'm using now
def choice(arr,num):
    return arr[numpy.int16(numpy.floor(arr.shape[0]*numpy.random.random(num)))]
numpy.random.choice = choice

class BSworker(QtCore.QThread):
    def __init__(self, parent = None):
        QtCore.QThread.__init__(self, parent)
        self.exiting = False
        self.rval = None
        self.method = None
        self.args = None
        self.sig_string = None

    def __del__(self):
        self.exiting = True
        self.wait()

    def run(self):
        self.rval = apply(self.method,self.args)
        self.emit( QtCore.SIGNAL(self.sig_string), 'worker finished')
        return
    
    def run_clustering(self, features, method):
        self.method = BSSpikeSort.run_clustering
        self.args = (features, method)
        self.sig_string = 'clustering(QString)'
        self.start()

    def run_thresholding(self, file, threshold, handler):
        self.method = BSSpikeSort.collect_std_events
        self.args = ( file, threshold, handler)
        self.sig_string = 'thresholding(QString)'
        self.start()

    def run_features(self, waveforms, feature_string):
        self.method = BSSpikeSort.determine_features
        self.args = ( waveforms, feature_string)
        self.sig_string = 'features(QString)'
        self.start()

class BSSpikeSortUI(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        #the directory with our data
        self.data_directory = None
        #the module with the data handling components
        self.data_module = None
        #other bits
        self.features = None
        self.snippets = None
        self.labels = None
        self.unclassified = []
        self.single_units = []
        self.multi_units = []
        self.noise = []
        self.completions = completions

        self.worker = BSworker()
    
        #plot some constants for plotting
        self.zoom = 1.0
        self.updating = False
        self.plot_single_events = False
        self.current_permuation = [0]
        self.current_perm_index = 0
        self.vgraphs = [[self.ui.plot_1v2, self.ui.plot_1v3, 
                        self.ui.plot_1v4, self.ui.plot_1v5,],
                       [self.ui.plot_2v3, self.ui.plot_2v4,
                        self.ui.plot_2v5], [self.ui.plot_3v4,
                        self.ui.plot_3v5], [self.ui.plot_4v5]]
        self.ui.combo_rec_type.clear()
        self.ui.combo_rec_type.addItems( 
                [mod[0] for mod in data_import_modules])
        self.ui.combo_cluster_method.clear()
        self.ui.combo_cluster_method.addItems( 
                [method[0] for method in clustering_methods])
        self._connect_slots()
        self._setup_gui()
    
    def _connect_slots(self):
        # tab 1
        self.ui.button_rec_select.clicked.connect( self.slot_select_folder)
        self.ui.button_rec_enter.clicked.connect( self.slot_update_directory)
        self.ui.button_chan_enter.clicked.connect( self.slot_select_channel)
        self.ui.button_collect_snippets.clicked.connect(
                self.slot_collect_snippets)
        self.ui.button_feature_collect.clicked.connect(
                self.slot_calculate_features)
        self.ui.button_run_clustering.clicked.connect(
                self.slot_run_clustering)
        self.ui.button_chan_next.clicked.connect(
                self.slot_next_channel)
        # tab 2
        self.ui.button_combine_clusters.clicked.connect(
                self.slot_combine_clusters)
        self.ui.button_reset_clusters.clicked.connect(
                self.slot_reset_all_clusters)
        self.ui.button_switch_clusters.clicked.connect(
                self.slot_switch_clusters)
        self.ui.button_single_unit.clicked.connect( lambda:
                self.slot_cluster_to_list(self.single_units))
        self.ui.button_multi_unit.clicked.connect( lambda:
                self.slot_cluster_to_list(self.multi_units))
        self.ui.button_noise_unit.clicked.connect( lambda:
                self.slot_cluster_to_list(self.noise))
        self.ui.button_previous_event.clicked.connect( lambda:
                self.slot_move_event( -1))
        self.ui.button_next_event.clicked.connect( lambda:
                self.slot_move_event( 1))
        self.ui.list_cluster_1.currentRowChanged.connect( lambda:
                self.slot_change_cluster( 1))
        self.ui.list_cluster_2.currentRowChanged.connect( lambda:
                self.slot_change_cluster( 2))
        self.ui.check_single_events.stateChanged.connect( 
                self.slot_check_single_event)
        self.ui.button_save.clicked.connect(self.slot_save)
        self.ui.button_zoom_in.clicked.connect( lambda:
                self.slot_change_zoom(-0.1))
        self.ui.button_zoom_out.clicked.connect( lambda:
                self.slot_change_zoom(0.1))
        # add some methods for when the worker finishes
        self.connect( self.worker, QtCore.SIGNAL('thresholding(QString)'),
                self.slot_receive_snippets)
        self.connect( self.worker, QtCore.SIGNAL('features(QString)'),
                self.slot_receive_features)
        self.connect( self.worker, QtCore.SIGNAL('clustering(QString)'),
                self.slot_receive_clusters)

    def _setup_gui(self):
        #TODO set up list for data_import_modules in combo_rec_type
        #set up list of clustering methods
        #disable all of the things we cant use yet
        #if we have everything on the front page, set up the second page
        if not self.completions['Clustering Run']: #update the front page
            #print 'disabling save'
            self.ui.button_save.setEnabled(False) #set second tab to inaccessable
            if self.completions['Folder Selected']:
                self.ui.button_chan_enter.setEnabled(True)
            else:
                self.ui.button_chan_enter.setEnabled(False)
            if self.completions['Channel Selected']:
                self.ui.group_snippets.setEnabled(True)
            else:
                self.ui.group_snippets.setEnabled(False)
            if self.completions['Snippets Collected']:
                self.ui.group_features.setEnabled(True)
                self.ui.label_details.setText(' We collected '
                        +str(self.snippets.shape[0]) + ' events')
            else:
                self.ui.group_features.setEnabled(False)
            if self.completions['Features Calculated']:
                self.ui.group_cluster.setEnabled(True)
            else:
                self.ui.group_cluster.setEnabled(False)
            report = '\n'.join(['{} ... {}'.format(key, value) for 
                    key, value in self.completions.items()])
            self.ui.label_progress.setText(report)

        else: #update the second page
            #print 'enabling save'
            self.ui.button_save.setEnabled(True) #set second tab to inaccessable
            self.maxs = []
            self.mins = []
            for label in self.unique_labels:
                self.maxs.append(numpy.max(self.snippets[self.labels==label,:]))
                self.mins.append(numpy.min(self.snippets[self.labels==label,:]))
            self._updating = True
            self.ui.list_cluster_1.clear()
            self.ui.list_cluster_1.addItems( [str(x) for x in
                self.unique_labels])
            self.ui.list_cluster_2.clear()
            self.ui.list_cluster_2.addItem(' ')
            self.ui.list_cluster_2.addItems( [str(x) for x in
                self.unique_labels])
            self._setup_classes()
            #call the update_all_plots
            self.ui.list_cluster_1.setCurrentRow(0)
            self.ui.list_cluster_2.setCurrentRow(0)
            self.updating = False
            cluster1 = self.unique_labels[
                    self.ui.list_cluster_1.currentRow()]
            self.current_perm_index = 0
            indicies = numpy.nonzero(self.labels == cluster1)
            self.current_permutation = numpy.random.permutation(indicies)[0]
            print ('about to set up graphs')
            self.update_all_graphs()

    def _setup_classes(self):
        #load the units into the lists
        self.ui.list_unsorted.clear()
        self.ui.list_unsorted.addItems(
                [str(x) for x in self.unclassified])
        self.ui.list_single_unit.clear()
        self.ui.list_single_unit.addItems(
                [str(x) for x in self.single_units])
        self.ui.list_multiunit.clear()
        self.ui.list_multiunit.addItems(
                [str(x) for x in self.multi_units])
        self.ui.list_noise.clear()
        self.ui.list_noise.addItems(
                [str(x) for x in self.noise])
        
        

# slots for tab 1
    def slot_select_folder(self):
        '''have the user select a folder and then pretend enter was pressed'''
        if not self.data_directory:
            self.data_directory = '.'
        new_dir = QtGui.QFileDialog.getExistingDirectory(
                    self, 'Select Data Directory', self.data_directory)
        if new_dir:
            self.ui.line_rec_path.setText(new_dir)
            self.slot_update_directory()
            self._setup_gui()

    def slot_update_directory(self):
        '''update the directory and set the available channels'''
        self.data_directory = str(self.ui.line_rec_path.text())
        #get the files in this directory
        if self.ui.combo_rec_type.currentText() == 'BatchResults.mat':
            #self.files = ['clusters.mat']
            self.files = []
            for file in os.listdir(self.data_directory):
                if 'clustering_' in file and '_results' not in file:
                    self.files.append(file)
        else:
            mod = self.ui.combo_rec_type.currentIndex()
            self.data_module = importlib.import_module(
                    data_import_modules[mod][1])
            self.files = self.data_module.get_file_list(
                    self.data_directory)
            #print 'debug line is present to add files in slot_update_directory'
            #self.files = self.data_module.get_trig_file(
            #        self.data_directory)
        self.ui.combo_rec_chan.clear()
        self.ui.combo_rec_chan.addItems(self.files)
        
        self.completions['Folder Selected'] = True
        self.completions['Channel Selected'] = False
        self.completions['Snippets Collected'] = False
        self.completions['Features Calculated'] = False
        self.completions['Clustering Run'] = False
        self.completions['Analysis Completed'] = False
        self._setup_gui()

    def slot_select_channel(self):
        self.input_channel = str(os.path.join(self.data_directory,
                    self.files[self.ui.combo_rec_chan.currentIndex()]))
        if self.ui.combo_rec_type.currentText() == 'BatchResults.mat':
            import scipy.io
            res = scipy.io.loadmat(self.input_channel)
            self.snippets = res['clusters']['waveforms'][0,0]
            self.times = res['clusters']['times'][0,0][0,:]
            self.features = res['clusters']['features'][0,0]
            self.initial_labels = res['clusters']['labels'][0,0][0,:]
            #self.labels = self.initial_labels
            #self.unique_labels = numpy.unique(self.labels)
            self.worker.rval = self.initial_labels
            self.std_threshold = res['clusters']['threshold'][0,0][0,0]
            self.feature_string = res['clusters']['feature_string'][0,0][0]
            self.clustering_method = res['clusters']['sort_method'][0,0]
            self.slot_receive_clusters()
        if len(self.input_channel) > 4 and self.input_channel[-4] is '.':
            self.output_file = (os.path.join( self.data_directory,
                self.input_channel[:-4]) + '_results.mat')
        else:
            self.output_file = (os.path.join( self.data_directory, 
                self.input_channel) + '_results.mat')
            self.completions['Folder Selected'] = True
            self.completions['Channel Selected'] = True
            self.completions['Snippets Collected'] = False
            self.completions['Features Calculated'] = False
            self.completions['Clustering Run'] = False
            self.completions['Analysis Completed'] = False
        self.ui.line_output_path.setText(self.output_file)
        self._setup_gui()

    def slot_next_channel(self):
        current_i = self.ui.combo_rec_chan.currentIndex()
        if current_i < self.ui.combo_rec_chan.maxCount():      
            self.ui.combo_rec_chan.setCurrentIndex(current_i+1)
            self.slot_select_channel()

    def slot_collect_snippets(self):
        '''read the standard deviatons to use and then collect from the file'''
        self.std_threshold = float(self.ui.line_stdevs.text())
        self.worker.run_thresholding(self.input_channel, self.std_threshold,
                self.data_module.get_handler(self.input_channel))

    def slot_receive_snippets(self):
        self.snippets, self.times = self.worker.rval
        #self.snippets, self.times = BSSpikeSort.collect_std_events(
        #        self.input_channel, self.std_threshold,
        #        self.data_module.get_handler(self.files[0]))
        try:
            indicies = numpy.random.choice(numpy.arange(
                    self.snippets.shape[0]), sample_points)
            self.ui.p1_left_plot.axes.plot(self.snippets[indicies,:].T)
            self.ui.p1_left_plot.draw()
        except ZeroDivisionError:
            pass
        
        self.completions['Folder Selected'] = True
        self.completions['Channel Selected'] = True
        self.completions['Snippets Collected'] = True
        self.completions['Features Calculated'] = False
        self.completions['Clustering Run'] = False
        self.completions['Analysis Completed'] = False
        self._setup_gui()

    def slot_calculate_features(self):
        '''read the feature string and calculate from snippets'''
        self.feature_string = str(self.ui.line_feature_string.text())
        self.worker.run_features(self.snippets, self.feature_string)

    def slot_receive_features(self):
        #self.features = BSSpikeSort.determine_features(
        #        self.snippets,self.feature_string)
        self.features = self.worker.rval
        try:
            
            indicies = numpy.random.choice(numpy.arange(
                    self.features.shape[0]), sample_points)
            self.ui.p1_right_plot.axes.plot(self.features[indicies,:].T)
            self.ui.p1_right_plot.draw()
        except ZeroDivisionError:
            pass

        self.completions['Folder Selected'] = True
        self.completions['Channel Selected'] = True
        self.completions['Snippets Collected'] = True
        self.completions['Features Calculated'] = True
        self.completions['Clustering Run'] = False
        self.completions['Analysis Completed'] = False
        self._setup_gui()

    def slot_run_clustering(self):
        '''determine the clustering method and run it'''
        self.clustering_method = clustering_methods[
                self.ui.combo_cluster_method.currentIndex()]
        self.worker.run_clustering(self.features, self.clustering_method[1])

    def slot_receive_clusters(self):
        self.initial_labels = self.worker.rval
        #self.initial_labels = BSSpikeSort.run_clustering(
        #        self.features,self.clustering_method[1])
        self.labels = self.initial_labels.copy()
        self.unique_labels = numpy.unique(self.labels)
        # whether each label is single unit or whatever
        self.unclassified = self.unique_labels.copy().tolist()
        self.single_units = []
        self.multi_units = []
        self.noise = []
        #assign all to unclassifed to begin with
        self.classification = [self.unclassified]*len(self.unique_labels)
        #allow us to move on
        self.completions['Folder Selected'] = True
        self.completions['Channel Selected'] = True
        self.completions['Snippets Collected'] = True
        self.completions['Features Calculated'] = True
        self.completions['Clustering Run'] = True
        self.completions['Analysis Completed'] = False
        print ('should be set to save')
        print (self.completions['Clustering Run'])
        self._setup_gui()

    def slot_save(self):
        res = {}
        res['directory'] = self.data_directory
        res['file'] = self.input_channel
        res['waveforms'] = self.snippets
        res['times'] = self.times
        res['features'] = self.features
        res['labels'] = self.labels
        res['threshold'] = self.std_threshold
        res['clustering_method'] = self.clustering_method
        res['feature_string'] = self.feature_string
        res['single_units'] = self.single_units
        res['mulit_units'] = self.multi_units
        res['noise'] = self.noise
        res['unclassified'] = self.unclassified
        import scipy.io
        scipy.io.savemat(self.output_file,res,oned_as='row')

    # slots for buttons on second tab
    def slot_combine_clusters(self):
        print ('entering combine clusters')
        cluster1 = self.unique_labels[self.ui.list_cluster_1.currentRow()]
        index2 = self.ui.list_cluster_2.currentRow()-1
        if index2 == -1:
            cluster2 = None
        else:
            cluster2 = self.unique_labels[index2]
        if cluster2 is not None and cluster1 != cluster2:
            self.updating = True
            self.labels[self.labels == cluster2] = cluster1
            li = self.classification[index2]
            index = [i for i,x in enumerate(li) if x == cluster2][0]
            del li[index]
            del self.classification[index2]
            self.unique_labels = numpy.unique(self.labels)
            print('setting up gui in combine clusters')
            self._setup_gui()

    def slot_reset_all_clusters(self):
        self.labels = self.initial_labels.copy()
        self.unique_labels = numpy.unique(self.labels)
        self.unclassified = self.unique_labels.copy().tolist()
        self.classification = [self.unclassified]*len(self.unique_labels)
        self.single_units = []
        self.multi_units = []
        self.noise = []
        self._setup_gui()

    def slot_switch_clusters(self):
        cluster1 = self.ui.list_cluster_1.currentRow()
        cluster2 = self.ui.list_cluster_2.currentRow()-1 
        if cluster2 > -1 and cluster1 != cluster2:
            self.ui.list_cluster_1.setCurrentRow(cluster2)
            self.ui.list_cluster_2.setCurrentRow(cluster1+1)
            self.update_unit_graphs()

    def slot_cluster_to_list(self,new_list):
        row1 = self.ui.list_cluster_1.currentRow()
        cluster1 = self.unique_labels[row1]
        li = self.classification[row1]
        del li[li.index(cluster1)]
        new_list.append(cluster1)
        self.classification[row1] = new_list
        self._setup_classes()

    def slot_check_single_event(self):
        self.plot_single_events = self.ui.check_single_events.isChecked()
        self.update_mean_waveforms()
        self.update_single_event_plot()

    def slot_move_event(self,value):
        if self.plot_single_events:
            self.current_perm_index += value
            if self.current_perm_index < 0:
                self.current_perm_index = len(self.current_permutation)-1
            elif self.current_perm_index >= len(self.current_permutation):
                self.current_perm_index = 0
            self.update_single_event_plot()

    def slot_change_cluster(self,which):
        if not self.updating:
            if which == 1:
                cluster1 = self.unique_labels[
                        self.ui.list_cluster_1.currentRow()]
                self.current_perm_index = 0
                indicies = numpy.nonzero(self.labels == cluster1)
                self.current_permutation = numpy.random.permutation(indicies)[0]
            self.update_unit_graphs()

    def slot_change_zoom(self,value):
        self.zoom += value
        if self.zoom < 0.1:
            self.zoom = 0.1
        if self.zoom > 1:
            self.zoom = 1.0
        self.update_unit_graphs()

    # functions for updating graphs

    def update_all_graphs(self):
        '''update all of the graphs on the sorting tab'''
        self.maxs = []
        self.mins = []
        for label in self.unique_labels:
            self.maxs.append(numpy.max(self.snippets[self.labels == label,:]))
            self.mins.append(numpy.min(self.snippets[self.labels == label,:]))
        self.update_mean_waveforms()
        self.update_single_event_plot()
        self.update_feature_plots()
        self.update_unit_graphs()
        #pyplot.draw() at end of update_unit_graphs

    def update_mean_waveforms(self):
        print('updating mean plot')
        #plot the mean waveform of each unit
        if not self.plot_single_events:
            self.ui.plot_units.axes.clear()
            self.ui.plot_units.axes.hold(True)
            for k in self.unique_labels:
                self.ui.plot_units.axes.plot(
                        numpy.mean(self.snippets[self.labels==k,:],axis=0),
                        color = colors[k])
            self.ui.plot_units.axes.set_ylim(
                    min(self.mins)*self.zoom, max(self.maxs)*self.zoom)
            self.ui.plot_units.draw()

    def update_feature_plots(self):
        print('updating feature plots')
        #update feature graphs
        for i, group1 in enumerate(self.vgraphs):
            for j, mplw in enumerate(group1):
                if j+i+1 < self.features.shape[1]:
                    mplw.axes.clear()
                    mplw.axes.hold(True)
                    for k in self.unique_labels:
                        #print [i,j,k,self.labels.shape,self.features.shape] 
                        indicies = numpy.random.choice(
                                numpy.nonzero(self.labels == k)[0],sample_points)
                        xdata = self.features[indicies, i]
                        ydata = self.features[indicies, i+j+1]
                        mplw.axes.plot(xdata,ydata,color=colors[k],marker='.',
                                linestyle = 'None',markersize=2,
                                markeredgecolor = colors[k])
                    mplw.draw()

    def update_single_event_plot(self):
        #update single unit plots
        print( 'updating single unit plot')
        if self.plot_single_events:
            self.ui.plot_units.axes.hold(False)
            self.ui.plot_units.axes.plot(
                    self.snippets[self.current_permutation[
                    self.current_perm_index],:].T)
            self.ui.plot_units.axes.set_ylim(
                    min(self.mins)*self.zoom ,max(self.maxs)*self.zoom)
            self.ui.plot_units.draw()


    def update_unit_graphs(self):
        '''update all of the graphs pertaining to selected units'''
        #just in case update the event plot
        self.update_single_event_plot()
        
        #determine which clusters we have selected
        cluster1 = self.unique_labels[self.ui.list_cluster_1.currentRow()]
        index = self.ui.list_cluster_2.currentRow()
        if index == 0:
            cluster2 = None
        else:
            cluster2 = self.unique_labels[index-1]
        print( 'updating mean plot')
        #update average plots
        indicies = numpy.random.choice(
                numpy.nonzero(self.labels == cluster1)[0], sample_points)
        self.ui.plot_unit_avg.axes.hold(False)
        self.ui.plot_unit_avg.axes.plot(
                numpy.mean(self.snippets[indicies,:],axis=0),
                color = colors[cluster1])
        if cluster2 is not None:
            indicies = numpy.random.choice(
                    numpy.nonzero(self.labels == cluster2)[0], sample_points)
            self.ui.plot_unit_avg.axes.hold(True)
            self.ui.plot_unit_avg.axes.plot(
                    numpy.mean(self.snippets[cluster2 == self.labels,:],axis=0),
                    color = colors[cluster2])
        self.ui.plot_unit_avg.axes.set_ylim(
                min(self.mins) * self.zoom ,max(self.maxs) * self.zoom)
        self.ui.plot_unit_avg.draw()
        print('updating plots over time')
        #update the change over time plots
        #find the relevant peak (e.g. minimum for negative thresholds)
        if self.std_threshold < 0:
            index = self.feature_string.find('m')
            if index == -1:
                index = 0
        else:
            index = self.feature_string.find('M')
            if index == -1:
                index = 0
        #do the updating
        xdata = numpy.nonzero(self.labels == cluster1)[0]
        ydata = self.features[self.labels == cluster1,index]
        self.ui.plot_over_time.axes.hold(False)
        self.ui.plot_over_time.axes.plot( xdata, ydata, 
                color = colors[cluster1], marker = '.',linestyle = 'None')
        if cluster2 is not None:
            xdata = numpy.nonzero(self.labels == cluster2)[0]
            ydata = self.features[self.labels == cluster2,index]
            self.ui.plot_over_time.axes.hold(True)
            self.ui.plot_over_time.axes.plot( xdata, ydata, 
                    color = colors[cluster2], marker = '.',linestyle = 'None')
        self.ui.plot_over_time.draw()


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    window = BSSpikeSortUI()
    window.show()
    sys.exit(app.exec_())
