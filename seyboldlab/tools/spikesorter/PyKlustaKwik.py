import numpy
import csv
import subprocess

path_to_kk = r'C:\Users\Bryan\Documents\pythonProjects\SchreinerLabCode'

def write_fet_file(features, fullname = None, basename = 'Klusta', run = 1):
    if not fullname:
        fullname = basename + '.fet.' + str(run)
    if not '.fet.' in fullname:
        fullname = fullname + '.fet.' + str(run)
    with open(fullname, 'w') as ofile:
        ofile.write(str(features.shape[1]) + '\n')
        ocsv = csv.writer(ofile, delimiter=' ')
        for row in features:
            ocsv.writerow(row)

def run_klusta_kwik(infile = 'Klusta', run = 1, minClusters = 3,
        maxClusters = 5, maxPossibleClusters = 10, nStarts = 3, 
        useFeatures = '', screen = 1):
    if '.fet.' in infile:
        index = infile.find('.fet.')
        run = infile.fine[index+len('.fet.'):] #if a full name save run
        infile = infile[:index] #trim off .fet.
    if not useFeatures: #default features to use all features
        with open(infile + '.fet.' + str(run),'r') as ifile:
            num_features = int(ifile.readline())
        useFeatures = '1'*num_features

    args = [path_to_kk+r'\KlustaKwik.exe',
             infile,
             str(run),
             '-MinClusters',str(minClusters),
             '-MaxClusters',str(maxClusters),
             '-MaxPossibleClusters',str(maxPossibleClusters),
             '-UseFeatures',str(useFeatures),
             '-nStarts',str(nStarts),
             '-Screen',str(screen)]
    subprocess.call(args)
    #subprocess.call(path_to_kk+r'\KlustaKwik')

def read_clu_file(fullname = None, basename = 'Klusta', run = 1):
    if not fullname:
        fullname = basename + '.clu.' + str(run)
    if not '.clu.' in fullname:
        fullname = fullname + '.clu.' + str(run)
    with open(fullname, 'r') as ifile:
        num_clusters = int(ifile.readline())
        icsv = csv.reader(ifile, delimiter=' ')
        clusters = []
        for row in icsv:
            clusters.append(int(row[0]))
        return numpy.array(clusters)
