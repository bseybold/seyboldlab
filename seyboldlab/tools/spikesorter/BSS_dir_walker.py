import os
import BSSpikeSort
import numpy
import scipy.io
import datetime
import multiprocessing


# in py2.7 with scipy0.9, can't handle unicode dict keys
def convert_keys_from_unicode(d):
    r = {}
    for key,value in d.items():
        r[key.encode('ascii')] = value
    return r
# walk over all the containers to replace all the unicode keys
def walk_containers_to_convert_keys(top):
    if type(top) == type({}): #handle dicts
        top = convert_keys_from_unicode(top)
        for key,value in top.items():
            if hasattr(value,'__iter__'):
                top[key] = walk_containers_to_convert_keys(value)
    else:  #handle lists or arrays
        for i,item in enumerate(top):
            if hasattr(item,'__iter__'):
                top[i] = walk_containers_to_convert_keys(item)
    return top

def get_neighboring_sites(channel_number,probemap = None):
    if probemap == '1x16x50':
        neighbors = {1:[16,14],
         2:[15,16],
         3:[14,11],
         4:[12,13],
         5:[12,15],
         6:[11],
         7:[10,13],
         8:[9,10],
         9:[8],
         10:[8,7],
         11:[3,6],
         12:[4,5],
         13:[7,4],
         14:[1,3],
         15:[2,5],
         16:[1,2],
        }
    else:
        return None
    return neighbors[channel_number]


def handle_file(directory, file, handler, first_thresh = -4, max_events = 30000,
        feature_string = 'mMiiiiiiii', sort_method = 'klusta_kwik',
        id_string = '',use_adjacent_channels = True):
    results = {}
    files_to_threshold = [os.path.join(directory,file)]
    if 'trig' in file.lower():
        first_thresh = 100
    elif use_adjacent_channels:
        #TDT only code
        print('TDT only code is present')
        index = file.find('Wave')
        #python/TDT indicies are off by 1
        channel = int(file[index+4:])+1
        neighbors = get_neighboring_sites(channel,'1x16x50')
        for channel in neighbors:
            files_to_threshold.append(
                    os.path.join(directory,'Wave'+str(channel-1)))
    threshold = first_thresh


    results['directory'] = directory
    results['filename'] = file
    results['parameters'] = handler(os.path.join(directory,file)
                ).get_data()

    results['waveforms'],results['times'] = (
            BSSpikeSort.collect_std_events(
            files_to_threshold, threshold, handler = handler))

    # if we have too many events increase the threshold
    while results['times'].shape[0] > max_events:
        threshold += numpy.sign(threshold)
        results['waveforms'],results['times'] = (
            BSSpikeSort.collect_std_events(
                files_to_threshold, threshold,
                handler = handler))
    
    results['threshold'] = threshold
    try:
        results['features'] = BSSpikeSort.determine_features( 
                results['waveforms'], feature_string,
                center_limit =
                results['waveforms'].shape[1] / len(files_to_threshold))
    except:
        feature_string.replace('i','p')
        results['features'] = BSSpikeSort.determine_features( 
                results['waveforms'], feature_string,
                center_limit =
                results['waveforms'].shape[1] / len(files_to_threshold))
    results['feature_string'] = feature_string
    results['labels'] = BSSpikeSort.run_clustering( 
            results['features'], sort_method)
    results['sort_method'] = sort_method

    #dir_results.append(results)
    # store as a matlab cell of structs
    #mat_results = {'clusters':numpy.array(dir_results,dtype=numpy.object)}
    mat_results = {'clusters':results}
    mat_results = walk_containers_to_convert_keys(mat_results)
    scipy.io.savemat(os.path.join(directory,
            'clustering_{0}_{1}.mat'.format(file,id_string)), 
            mat_results,oned_as='row')
    print 'Completed file '+file


if __name__ == '__main__':
    print 'in the dir walker'
    #top_dir = 'E:\\Data\\TDTData\\2012-04-12'
    date = str(datetime.date.today())
    import TDTHandler
    module = TDTHandler


    handler = module.get_handler('')
    #dirs = []
    unprocessed_files = []
    #walk the directory path looking for things with DMR in them
    #for (path,dirnames,filenames) in os.walk(top_dir):
    #    for name in dirnames:
    #        if 'DMR' in name:
    #            dirs.append(os.path.join(path,name))

    #print 'DEBUG TO DO SINGLE DIR'
    #dirs = [top_dir]
    dirs = [
            r'J:\TDT DMR TTL data\2012-04-09\rec9 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-09\rec12 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-09\rec15 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-09\rec18 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-09\rec21 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-10\rec15 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-10\rec19 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-10\rec21 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-10\rec28 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-11\rec23 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-11\rec25 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-11\rec35 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-11\rec39 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-11\rec43 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-11\rec48 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-12\rec10 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-12\rec14 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-12\rec18 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-12\rec23 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-12\rec33 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-12\rec37 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-12\rec42 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-12\rec46 DMR 4_09 no TTL',
            #r'J:\TDT DMR TTL data\2012-04-12\rec50 DMR 4_09 no TTL', 
            # should have used rec48 instead
            r'J:\TDT DMR TTL data\2012-04-13\rec6 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-13\rec22 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-13\rec26 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-14\rec7 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-14\rec11 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-14\rec18 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-16\rec11 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-16\rec15 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-16\rec19 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-16\rec32 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-17\rec15 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-17\rec19 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-17\rec23 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-17\rec25 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-18\rec26 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-18\rec31 DMR 4_09 no TTL',
            #should also have 2012-04-18\rec35 in here...
            r'J:\TDT DMR TTL data\2012-04-18\rec39 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-04-18\rec43 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-03\rec18 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-03\rec22 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-03\rec26 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-03\rec28 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-03\rec34 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-07\rec9 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-07\rec14 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-07\rec18 DMR 4_09 no TTL',
            #r'J:\TDT DMR TTL data\2012-05-07\rec22 DMR 4_09 no TTL',
            #should have used rec20 instead
            r'J:\TDT DMR TTL data\2012-05-08\rec11 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-08\rec15 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-08\rec19 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-08\rec23 DMR 4_09 no TTL',
            r'J:\TDT DMR TTL data\2012-05-08\rec27 DMR 4_09 no TTL',
            #r'J:\TDT DMR TTL data\2012-05-08\rec32 DMR 4_09 no TTL',
            #should have used #31 and #35
            r'J:\TDT DMR TTL data\2012-05-08\rec37 DMR 4_09 no TTL',
            ]


    for directory in dirs:
        print '\n\n\nWE\'RE IN DIRECTORY '+ directory +' \n\n\n'
        try:
            files = module.get_file_list(directory)
            files.extend(module.get_trig_file(directory))
        except ValueError:
            print 'Block '+ directory+' does not seem to be a recording set'
            continue 

        #sort each file and make into a .mat for the whole directory
        for file in files:
            print '\n WE\'RE IN FILE '+ file+'\n'

            try:
                p = multiprocessing.Process(
                        target = handle_file, args = (directory,file,handler),
                        kwargs = {'id_string':date})
                p.start()
                p.join()
            except Exception as e:
                print 'could not access file '+os.path.join(directory,file)
                print e
                unprocessed_files.append(os.path.join(directory,file))
                continue
    print unprocessed_files

