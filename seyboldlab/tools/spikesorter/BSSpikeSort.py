# -*- coding: utf-8 -*-

'''code to do spikesorting'''

#assume data is filtered

#collect spikes
#determine features
#cluster

#supervise, handled in other gui code

import numpy
#eventually inport data handlers

window = [-0.0005, 0.001]
censor = window[1]
aln = 100000 #an arbitrarily large number

def rec_at_times(handler,times,window):
    #convert times to samples
    window = [window[0]*handler.sampling_rate, window[1]*handler.sampling_rate]
    samples_per_window = window[1]-window[0]
    times = times*handler.sampling_rate
    #preallocate arrays for speed
    waveforms = numpy.zeros((times.shape[0],samples_per_window))
    databank = numpy.zeros((aln*2))
    #preload initial data
    next_data = handler.get_next_samples(aln)
    # set up some values
    samples_read = 0
    last_crossing = 0
    count = 0
    #could instead only get exactly enough samples per event
    while next_data.shape != ():
        #shift data down one block
        databank = numpy.concatenate([databank[aln:],next_data])
        for event in times[numpy.logical_and(times > samples_read-window[1],
                times < samples_read + aln - window[1])]:
            waveforms[count,:] = databank[event - samples_read + aln + 
                    window[0] : event - samples_read + aln + window[1] -1]
            #print waveforms[count-1,:]
            count +=1
        samples_read += aln
        next_data = numpy.array(handler.get_next_samples(aln))
    return waveforms

def threshold_rec(handler,threshold,window,censor,align_peak = True):
    #convert times to samples
    window = [numpy.ceil(window[0]*handler.sampling_rate),
            numpy.ceil(window[1]*handler.sampling_rate)]
    censor = numpy.ceil(censor*handler.sampling_rate)
    samples_per_window = window[1]-window[0]
    #preload initial data
    next_data = numpy.array(handler.get_next_samples(aln))
    #prealocate arrays for speed
    times = numpy.zeros(aln)
    waveforms = numpy.zeros((aln,samples_per_window), dtype = next_data.dtype)
    databank = numpy.zeros((aln*2), dtype = next_data.dtype)
    # set up some values
    offset = window[1]+censor
    samples_read = 0
    last_crossing = 0
    count = 0
    while next_data.shape != ():
        #shift data down one block
        databank = numpy.concatenate([databank[aln:],next_data])
        indicies = [aln-offset, databank.shape[0]-offset]
        #find threshold crossings
        if threshold > 0:
            crossings = numpy.logical_and(databank[indicies[0]:indicies[1]] <
                threshold, databank[indicies[0]+1:indicies[1]+1] > threshold)
        else:
            crossings = numpy.logical_and(databank[indicies[0]:indicies[1]] >
                threshold, databank[indicies[0]+1:indicies[1]+1] < threshold)
        #rewind by window
        crossings = numpy.nonzero(crossings)[0]
        #import pdb
        #pdb.set_trace()
        for event in crossings:
            # check if at least censor time has elapsed
            if (event + samples_read) > (last_crossing + censor):
                if align_peak:
                    if threshold > 0:
                        ind = numpy.argmax(databank[int(event+indicies[0]):
                            int(event+censor+indicies[0])])
                    else:
                        ind = numpy.argmin(databank[int(event+indicies[0]):
                            int(event+censor+indicies[0])])
                else:
                    ind = 0
                waveforms[count,:] = databank[
                        int(event+window[0]+ind+indicies[0])
                        :int(event+window[1]+ind+indicies[0])]
                times[count] = event + ind + samples_read - offset
                last_crossing = times[count]
                count +=1
        samples_read += len(next_data)
        next_data = numpy.array(handler.get_next_samples(aln))
    #limit arrays and convert back into seconds
    times = times[:count]/float(handler.sampling_rate)
    waveforms = waveforms[:count,:]
    return times, waveforms


def get_rec_std(handler,length,return_mean = False):
    samples = numpy.ceil(length*handler.sampling_rate)
    if return_mean:
        samples = handler.get_next_samples(samples)
        return (numpy.std(samples),numpy.mean(samples))
    else:
        return numpy.std(handler.get_next_samples(samples))

def collect_std_events(file_list,stds, handler = None, datatype = None):
    '''collect the spikes from the data files using thresholding'''
    if type(file_list) == type('a string') or type(file_list) == type(u' '):
        file_list = [file_list]
    
    if handler is None:
        if datatype is None:
            raise ValueError('either handler or datatype must be defined')
        if datatype == '.sw':
            import RawHandler
            handler = RawHandler.RawHandlerNeuralynx
        if datatype == '.ncs':
            import NeuralynxHandler
            handler = NeuralynxHandler.NeuralynxHandler
        elif datatype == '.tdt':
            import TDTHandler
            handler = TDTHandler.TDTHandler

    (threshold,mean) = get_rec_std(handler(file_list[0]),1,True)
    #print 'threshold is ' + str(threshold)
    times, waveforms = threshold_rec(handler(file_list[0]),
            threshold*stds+mean, window, censor)
    for i in range(1,len(file_list)):
        extra_waveforms = rec_at_times(handler(file_list[i]), times, window)
        waveforms = numpy.concatenate((waveforms, extra_waveforms),axis = 1)
        
    return waveforms, times
    
        
    
def determine_features(waveforms,feature_description, center_limit = None):
    '''given an array of waveforms calculate the features for each
    features can be requested by the following markup:
    i - add an ICA component
    p - add a  PCA component
    M - add the maximum
    m - add the minimum
    
    the center_limit parameter can limit the computation of min and max to a 
    particular range of the waveform vector
    '''
    if 'i' in feature_description or 'p' in feature_description:
        import mdp
    #check inputs
    if not all([c in 'ipMm' for c in feature_description]):
        raise ValueError('Unknown character in feature_description string')
    num_ica_components = sum([c == 'i' for c in feature_description])
    num_pca_components = sum([c == 'p' for c in feature_description])
    if center_limit is None:
        center_limit = waveforms.shape[1]
    
    #compute requested features
    if num_ica_components > 0:
        #controling the output_dim is not directly supported as an argument
        icas = mdp.fastica( numpy.float64(waveforms[:,:center_limit]),
                max_it = 2000, failures = 10, limit = 0.005)
    if num_pca_components > 0:
        pcas = mdp.pca( numpy.float64(waveforms[:,:center_limit]), 
                output_dim = num_pca_components)
    if 'm' in feature_description:
        mins = numpy.min( waveforms[:,:center_limit], axis = 1)
    if 'M' in feature_description:
        maxs = numpy.max( waveforms[:,:center_limit], axis = 1)
    
    #package requested features
    features = numpy.empty((waveforms.shape[0],len(feature_description)))    
    cur_ica = 0
    cur_pca = 0    
    for i, c in enumerate(feature_description):
        if c == 'm':
            data = mins
        if c == 'M':
            data = maxs
        if c == 'i':
            data = icas[ :, cur_ica]
            cur_ica += 1
        if c == 'p':
            data = pcas[ :, cur_pca]
            cur_pca += 1
            
        features[:,i] = data
        
    return features
    
def run_clustering(features,method,**kwargs):
    '''given a range of features cluster on those features
    Current methods:
    sk_DPGMM - dirlicht process gaussian mixture model
    sk_affinity_prop - affinity propagation
    sk_mean_shift - mean shift
    '''
    
    if method == 'sk_DPGMM':
        import sklearn.mixture.dpgmm
        # can set 'alpha' number of clusters ~= alpha*log(N)
        model = sklearn.mixture.DPGMM(
                n_components= kwargs.get('n_components',10), 
                cvtype= kwargs.get('cvtype','full'), 
                alpha = kwargs.get('alpha',0.1))
        model.fit(features)
        labels = model.predict(features)
        #probability = model.predict_proba(features)
    
    elif method == 'sk_affinity_prop':
        import sklearn.cluster.affinity_propagation_
        #first compute similarities
        normalized = numpy.sum(features ** 2, axis = 1)
        S = (-normalized[:, numpy.newaxis] - normalized[numpy.newaxis, :] +
                2 * numpy.dot(features, features.T))
        p = 10 * numpy.median(S)
        #now cluster as normal
        #can manually determine 'damping' to limit spread
        model = sklearn.cluster.AffinityPropagation()
        model.fit(S,p)
        labels = model.labels_
        
    elif method == 'sk_mean_shift':
        #can manually estimate bandwidth and seeding params
        import sklearn.cluster.mean_shift_
        import sklearn.cluster
        bandwidth = sklearn.cluster.estimate_bandwidth(
                features, n_samples = 1000)
        model = sklearn.cluster.MeanShift( bandwidth = bandwidth)
        model.fit(features)
        labels = model.labels_

    elif method == 'klusta_kwik':
        import PyKlustaKwik
        PyKlustaKwik.write_fet_file(features)
        PyKlustaKwik.run_klusta_kwik(screen = 0)
        labels = PyKlustaKwik.read_clu_file()
    
    return labels
        
    
if __name__ == '__main__':
    waveforms, times = collect_std_events('.ncs','filename',-4)
    features = determine_features(waveforms,'mMppiiiiii')
    labels = run_clustering(features,'sk_mean_shift')
