from __future__ import division
import numpy as np
import importlib, sys, pprint
from numbers import Number
from itertools import product, izip_longest
from os import path
from ..tools.signals import fft_cut, power_at_frequency
from ..tools.common import ensure_group, StringIO, exec23

################################################################################
#validation functions for parameters
def validate_none(instance):
    '''do no validation'''
    pass

def validate_number(instance):
    '''ensure that the instance is a number'''
    instance = ensure_group(instance)
    if not all( [isinstance(x, (Number, np.number, np.ndarray))
                for x in instance]):
        raise ValueError('Validation for numbers failed')

def validate_enum(instance,values):
    '''ensure that the instance is one of a select few values passed via lambda'''
    instance = ensure_group(instance)
    values = ensure_group(values)
    if not all([ (x in values) for x in instance]):
        raise ValueError('Validation for enumuration failed')

def validate_string(instance):
    '''ensure that the instance is a string'''
    instance = ensure_group(instance)
    if not all( [ isinstance(x,basestring) for x in instance]):
        raise ValueError('Validation for string failed')

class Parameter():
    '''the base class for parameters that allows validation'''
    def __init__(self, default_value, validator=validate_number):
        self.default_value = default_value
        self.validator = validator
        self.name=None

    def __repr__(self):
        return "<Parameter: {self.name}, default: {self.default_value}>".format(self=self)

    def __str__(self):
        return "Parameter: {0}".format(self.name)

################################################################################
#delcarative metaclassing components
class DeclarativeMeta(type):
    '''classes under this metaclass automatically call __classinit__.im_func()'''
    def __new__(meta, class_name, bases, new_attrs):
        cls = type.__new__(meta, class_name, bases, new_attrs)
        cls.__classinit__.im_func(cls, bases, new_attrs)
        return cls

all_valid_stimuli = []
#workaround to support python2 and python3 metaclasses
def skip(*args,**kwargs):
    pass
DeclarativeBase = DeclarativeMeta('DeclarativeBase',(object,),{'__classinit__':skip})
class ParametricBase(DeclarativeBase):

    def __classinit__(cls, bases, new_attrs):
        '''pulls all parameters from base classes together'''
        cls.parameters = {}
        for base in bases:
            #include previously found parameters
            try:
                cls.parameters.update( base.__dict__['parameters'])
            except:
                pass
        for key, value in [(key, value) for key,value
                in new_attrs.items() if isinstance(value, Parameter)]:
            value.name = key
            value.validator(value.default_value)
            cls.parameters[key] = value
        #creates a default set of parameters for when other values are not assigned
        cls.default_parameters = {}
        for key, value in cls.parameters.items():
            cls.default_parameters[key] = value.default_value
        try:
            if cls.valid_stimulus:
                all_valid_stimuli.append(cls)
        except AttributeError:
            pass #didn't have a valid stimulus attribute

    def __init__(self, static_parameters = None):
        '''validates all parameters that do not vary'''
        self.static_parameters = self.__class__.default_parameters.copy()
        if static_parameters is not None:
            #validate new parameters
            for key in static_parameters:
                self.__class__.parameters[key].validator(static_parameters[key])
            self.static_parameters.update(static_parameters)

############################################################################
#IO
def save_stimulus(stimulus, filename=None):
    #could save __dict__ as a string
    stringfile = StringIO("")
    type_string = '{}.{}'.format(
                stimulus.__module__,
                stimulus.__class__.__name__)
    stringfile.write('import {}\n'.format(type_string))
    stringfile.write('stimulus = {}()\n'.format(type_string))
    stringfile.write('stimulus.__dict__ = \\\n')#allow to continue on a new line with a  backslash
    pprint.pprint(stimulus.__dict__,stream=stringfile)
    if filename is not None:
        with open(filename,'w') as outfile:
            outfile.write(stringfile.read())
    return stringfile.read()

    ##or do it manually
    #type_string = '{}.{}'.format(
    #            self.current_stimulus.__module__,
    #            self.current_stimulus.__class__.__name__)
    #file.write('import {}.{}\n'.format(type_string))
    #file.write('stimulus = {}()\n'.format(type_string))
    #file.write('stimulus.static_parameters = {\n')
    #for parameter,value in self.current_stimulus.static_parameters.items():
    #    file.write('"{}":{},\n'.format(parameter,value))
    #file.write('}\n')
    #file.write('stimulus.dynamic_parameters = {\n')
    #for parameter,value in self.current_stimulus.dynamic_parameters.items():
    #    file.write('"{}":{},\n'.format(parameter,value))
    #file.write('}\n')
    #if request['generate_combinations'] == True:
    #    self.current_stimulus.combinations = self.current_stimulus.get_combinations()
    #if hasattr(self.current_stimulus,'combinations'):
    #    file.write('stimulus.combinations = [\n')
    #    for combination in combinations:
    #        file.write('{\n')
    #        for parameter,value in combination.items():
    #            file.write('\t"{}":{},\n'.format(parameter,value))
    #        file.write('},\n')
    #    file.write(']\n')

def load_stimulus_from_file(filename, additional_dirs=None):
    with open(filename,'r') as infile:
        return load_stimulus_from_string(infile.read(),additional_dirs)
    #head, tail = path.split(filename)
    #mod_name = tail.split('.')[0]
    ##pollutes the path, but shouldn't be too terrible
    #if head not in sys.path:
    #    sys.path.append(head)
    ##handle single directory or sequence
    #if additional_dirs is not None:
    #    additional_dirs = ensure_group(additional_dirs)
    #    for path in additional_dirs:
    #        if path not in sys.path:
    #            sys.path.append(path)
    #module = importlib(mod_name)
    #return module.stimulus

def load_stimulus_from_string(string, additional_dirs=None):
    if additional_dirs is not None:
        additional_dirs = ensure_group(additional_dirs)
        for path in additional_dirs:
            if path not in sys.path:
                sys.path.append(path)
    local_globals = {}
    #requires 2/3 shiv
    exec23(string, local_globals)
    return local_globals['stimulus']



################################################################################
#defining stimuli and their parameters
class SoundBase(ParametricBase):
    '''all sounds have a sampling rate, maximum output, and can share a random seed'''
    sampling_rate = Parameter(96000)
    max_output_value = Parameter(1)
    random_seed = Parameter(1987)

    def save(self, filename):
        save_stimulus(self,filename)

class FrequencyBanked(SoundBase):
    '''A baseclass for stimuli that modulate a frequency bank'''
    frequency_start = Parameter(4000)
    frequency_stop  = Parameter(32000)
    frequency_scale = Parameter('log', validator= lambda x:
            validate_enum(x,{'log','linear'}))
    num_signals = Parameter(200)

    @staticmethod
    def generate_frequency_bank(parameters):
        ''' generates a list of frequencies starting from start_f and going to
        stop_f inclusive with the desired frequency step pattern '''
        start_f = parameters['frequency_start']
        stop_f  = parameters['frequency_stop']
        scale_f = parameters['frequency_scale']
        num_signals = parameters['num_signals']
        if stop_f < start_f:
            raise ValueError('frequency banke parameters must be increasing')
        if scale_f == 'linear':
            return np.arange(start_f,stop_f,(stop_f-start_f)/num_signals)
        elif scale_f == 'log':
            octaves = np.log2(stop_f/start_f)
            points = np.arange(0,octaves,octaves/num_signals)
            return start_f*2**points
        else:
            raise ValueError('Incorrect frequency bank scale parameter')

class SweepBase(SoundBase):
    '''all sounds with sweeps have a sweep duration and number of overall repeats'''
    sweep_duration = Parameter(1)
    runs = Parameter(1)

    @classmethod
    def get_buffer(cls, trial_parameters):
        '''returns a buffer of zeros long enough for a sweep'''
        length = np.ceil( trial_parameters['sampling_rate'] *
                          trial_parameters['sweep_duration'])
        return np.zeros((length,))

    @classmethod
    def get_timing(cls, trial_parameters):
        '''returns a buffer with the time since trial start in seconds'''
        return np.arange(0, trial_parameters['sweep_duration'],
                1.0 / trial_parameters['sampling_rate'])

    def __init__(self, dynamic_parameters = None, static_parameters = None):
        '''pools all dynamic parameters'''
        super(SweepBase, self).__init__(static_parameters)
        if dynamic_parameters is None:
            dynamic_parameters = {}
        self.dynamic_parameters = dynamic_parameters
        #convert numpy arrays to lists so itertools can work on them and validate
        for key in self.dynamic_parameters.keys():
            if isinstance(self.dynamic_parameters[key],np.ndarray):
                self.dynamic_parameters[key] = self.dynamic_parameters[key].tolist()
            self.__class__.parameters[key].validator(self.dynamic_parameters[key])

    def get_combinations(self):
        '''returns all combinations of dynamic parameters'''
        combinations = product(*self.dynamic_parameters.values())
        trial_combinations = []
        for x in range(self.static_parameters['runs']):
            trial_combinations.extend([dict(zip(self.dynamic_parameters.keys(),row))
                              for row in combinations])
        return trial_combinations

    def get_trial_parameters(self, combination = None):
        '''returns all of the parameters for one combination of dynamic parameters'''
        if combination is None:
            combination = {}
        trial_dict = self.static_parameters.copy()
        trial_dict.update(combination)
        return trial_dict

class EnvelopedSound(SweepBase):
    '''sounds that have a gating envelope and overall gain'''
    envelope_duration  = Parameter(0.5)
    envelope_offset    = Parameter(0.0)
    gate_type = Parameter("cosine",validator=
            lambda string:validate_enum(string,(None,'cosine','linear')))
    gate_time = Parameter(0.005)
    gain      = Parameter(0)

    @classmethod
    def get_envelope(cls, trial_parameters):
        '''returns a buffer with the envelope of the sound'''
        if (trial_parameters['sweep_duration']
              < trial_parameters['envelope_duration']
              + trial_parameters['envelope_offset']):
            raise ValueError('durations of envelopes must be <= sweep_duration')
        buffer = cls.get_buffer(trial_parameters)
        length = int(np.ceil(trial_parameters['sampling_rate'] *
                          trial_parameters['envelope_duration']))
        inner_buffer = np.ones((length,))
        gate_length = int(np.ceil(
                trial_parameters['gate_time'] *
                trial_parameters['sampling_rate']))
        steps = np.arange(0,1,1/gate_length)
        if trial_parameters['gate_type'] is not None:
            if trial_parameters['gate_type'] is 'cosine':
                gate = 0.5-np.cos(steps*np.pi)/2
            if trial_parameters['gate_type'] is 'linear':
                gate = steps
            indicies_front = range(0,gate_length)
            indicies_end   = range(length-1,length-gate_length-1,-1)
            inner_buffer[indicies_front] = gate
            inner_buffer[indicies_end] = gate
        buffer_offset = int(np.ceil(trial_parameters['sampling_rate']*
                                    trial_parameters['envelope_offset']))
        buffer[buffer_offset:buffer_offset+inner_buffer.size] += inner_buffer
        buffer *= 10**float(trial_parameters['gain']/20)
        return buffer


class Tone(EnvelopedSound):
    '''pure sinesoids'''
    frequency = Parameter(4000)
    valid_stimulus = True

    @classmethod
    def get_waveform(cls, trial_parameters):
        '''returns the waveform for a tone'''
        envelope = cls.get_envelope(trial_parameters)
        timing   = cls.get_timing(trial_parameters)
        frequency = trial_parameters['frequency']
        sound = np.sin(frequency*2*np.pi*timing)
        return sound*envelope

class Noise(EnvelopedSound):
    '''bursts of noise'''
    normalized_RMS = Parameter(0.5)
    valid_stimulus = True

    @classmethod
    def get_waveform(cls, trial_parameters):
        '''returns the waveform for the burst of noise'''
        envelope = cls.get_envelope(trial_parameters)
        sound = np.random.rand(envelope.size)#uniform amplitude distribution
        sound -= sound.mean() #center at zero
        sound *= envelope
        rms = np.sqrt(np.mean(np.square(sound)))
        sound *= trial_parameters['normalized_RMS']/rms
        return sound

class BandpassNoise(Noise):
    '''bursts of noise that are band limited'''
    cutoff_high = Parameter(64000)
    cutoff_low  = Parameter(4000)
    valid_stimulus = True

    @classmethod
    def get_waveform(cls, trial_parameters):
        '''returns the waveform of band limited noise'''
        sound = super(BandpassNoise,cls).get_waveform(trial_parameters)
        sound = fft_cut(sound,trial_parameters['sampling_rate'],
                0,trial_parameters['cutoff_low'])
        sound = fft_cut(sound,trial_parameters['sampling_rate'],
                trial_parameters['cutoff_high'],trial_parameters['sampling_rate'])
        rms = np.sqrt(np.mean(np.square(sound)))
        sound *= trial_parameters['normalized_RMS']/rms
        return sound

    @staticmethod
    def compute_from_center(center_frequency,octaves):
        '''
        a method to compute a high and low frequency from a center frequency

        This method functions in logrithmic space
        '''
        low = center_frequency* 2**(-1*octaves/2)
        high = center_frequency* 2**(octaves/2)
        return (low, high)

class FrequencyBankNoise(Noise, FrequencyBanked):
    '''a busrt of simultaneous tones with random phase'''

    @classmethod
    def get_waveform(cls, trial_parameters):
        '''returns the waveform for the burst of simultaneous tones'''
        envelope = cls.get_envelope(trial_parameters)
        timing   = cls.get_timing(trial_parameters)
        frequencies = cls.generate_frequency_bank(trial_parameters)
        phases = np.tile(np.random.rand(frequencies.size),(timing.size,1)).T
        #phases = np.tile(np.zeros(frequencies.size),(timing.size,1)).T
        sounds = np.sin(np.outer(frequencies,timing)*2*np.pi+phases)
        sound = sounds.sum(axis=0)
        sound *= envelope
        rms = np.sqrt(np.mean(np.square(sound)))
        sound *= trial_parameters['normalized_RMS']/rms
        return sound

class GapInNoise(Noise):
    pass

class MultiComponentSweep(SweepBase):
    '''sounds made up of multiple simple components'''
    components = Parameter([],validator=validate_none)

    def get_waveform(self, trial_parameters):
        '''returns the combined waveforms of simplier components'''
        buffer = self.get_buffer(trial_parameters)
        for component in self.static_parameters['components']:
            component_trial = trial_parameters[component]
            w = component.get_waveform(component_trial)
            buffer += w
        return buffer

    def get_combinations(self):
        '''get combinations of parameters accross compontents'''
        #start with sweep level parameters assigned to none
        group_dynamics = dict([((None, key),value) for key,value in
                                self.dynamic_parameters.items()])
        for component in self.static_parameters['components']:
            for key, value in component.dynamic_parameters.items():
                group_dynamics[(component,key)] = value
                if key in self.default_parameters.keys():
                    raise ValueError('Components cannot overide sweep parameters')
        combinations_raw = product(*group_dynamics.values())
        trial_combinations_raw = [dict(zip(group_dynamics.keys(),row))
                                  for row in combinations_raw]
        trial_combinations = []
        #could make into a really confusing [zip[for]] statement probably
        for trial in trial_combinations_raw:
            trial_combination = {}
            #assign sweep level parameters
            for key in self.dynamic_parameters.keys():
                trial_combination[key] = trial[(None,key)]
            for component in self.static_parameters['components']:
                component_trial = {}
                for key in component.dynamic_parameters.keys():
                    component_trial[key] = trial[(component,key)]
                trial_combination[component] = component_trial
            trial_combinations.append(trial_combination)
        return trial_combinations

    def get_trial_parameters(self, combination = None):
        '''get all the trial parameters across components'''
        if combination is None:
            #set default as empty dicts
            combination = dict(izip_longest(
                    self.static_parameters['components'],
                    ({},),fillvalue={}))
        trial_dict = self.static_parameters.copy()
        trial_dict.update(combination)
        for component in self.static_parameters['components']:
            #get the default dict for this component
            component_trial = component.static_parameters.copy()
            #overwrite the global sweep parameters
            component_trial.update(trial_dict)
            #overwrite the dynamic parameters
            component_trial.update(combination[component])
            trial_dict[component] = component.get_trial_parameters(combination)
        return trial_dict

class ContinuousBase(SoundBase):
    '''Baseclass for long continuous stimuli'''
    duration = Parameter(1)


class ChunkedStimuli(ContinuousBase):
    '''A mix-in for generating coninuous stimuli in chunks'''
    #user values for parameter_start_time and _end_time are ignored
    parameter_start_time = Parameter(None, validator=validate_none)
    parameter_end_time   = Parameter(None, validator=validate_none)
    parameter_chunk_time_step  = Parameter(1)
    output_file = Parameter('out.bin', validator=validate_string)
    spectrogram_file = Parameter('out-spectrogram.bin', validator=validate_string)

    #instance method to allow other methods to maintain state
    def write_waveform_in_chunks(self, parameters):
        '''generates a long stimulus in chunks and saves sequentially to disk'''
        step = parameters['parameter_chunk_time_step']
        dur  = parameters['duration']
        out_file = open(parameters['output_file'],'wb')
        for i in xrange( np.floor( dur / step)):
            parameters['parameter_start_time'] = i*step
            parameters['parameter_end_time'] = (i+1)*step
            waveform = self.get_waveform(parameters)
            out_file.write(waveform.tostring())
        #compute the tailing chunck as well
        parameters['parameter_start_time'] = np.floor(dur/step)
        parameters['parameter_end_time']   = dur
        waveform = self.get_waveform(parameters)
        out_file.write(waveform.tostring())
        out_file.close()

    def write_waveform_and_spectrogram_in_chunks(self, parameters):
        '''generates a long stimulus in chunks and saves waveform and spectrogram'''
        step = parameters['parameter_chunk_time_step']
        dur  = parameters['duration']
        waveform_file = open(parameters['output_file'],'wb')
        spectrogram_file = open(parameters['spectrogram_file'],'wb')
        for i in xrange( int(np.floor( dur / step))):
            parameters['parameter_start_time'] = i*step
            parameters['parameter_end_time'] = (i+1)*step
            print('Working on timestep {}'.format(parameters['parameter_start_time']))
            waveform,spectrogram = self.get_waveform_and_spectrogram(parameters)
            waveform_file.write(waveform.tostring())
            spectrogram_file.write(spectrogram.tostring(order='F'))
        #compute the tailing chunck as well
        if not dur%step <0.0000001:
            parameters['parameter_start_time'] = np.floor(dur/step)
            parameters['parameter_end_time']   = dur
            waveform,spectrogram = self.get_waveform_and_spectrogram(parameters)
            waveform_file.write(waveform.tostring())
            spectrogram_file.write(spectrogram.tostring(order='F'))
        waveform_file.close()
        spectrogram_file.close()

class ContinuouslyVarying(ChunkedStimuli):
    parameter_min = Parameter(0)
    parameter_max = Parameter(1)
    parameter_rate = Parameter(1)
    parameter_interpolation = Parameter('cosine',validator= lambda x:
            validate_enum(x,{'cosine','linear'}))

    @classmethod
    def interpolate_points(cls,parameters):
        fs = parameters['sampling_rate']
        points = parameters['parameter_points']
        rate = parameters['parameter_rate']
        start_time = parameters['parameter_start_time']
        end_time = parameters['parameter_end_time']
        parameter_interpolation = parameters['parameter_interpolation']

        first_point = np.floor(start_time*rate)
        n_points    = np.ceil((end_time-(first_point/rate))*rate)
        last_point  = first_point+n_points+1
        points = points[int(first_point):int(last_point)]

        # should be [pos:neg], values need to be inclusive for integrate code
        valid_start = np.floor(fs*(start_time-(first_point/rate)))
        valid_end = np.floor(fs*(end_time-(first_point/rate)))#+1
        interp_vec = np.arange(0,1,rate/fs)
        interp_len = len(interp_vec)
        if parameter_interpolation is 'linear':
            #anything that goes from 0 to 1
            pass
        elif parameter_interpolation is 'cosine':
            interp_vec = 0.5-0.5*np.cos(np.pi*interp_vec)
        else:
            raise ValueError('interpolation type not supported')
        values = np.empty(np.ceil(fs*(len(points)-1)/rate))
        for i in xrange(len(points)-1):
            first_ind = np.floor(i*(fs/rate))
            last_ind  = np.ceil((i+1)*(fs/rate))

            #normalize to change per second
            values[first_ind:last_ind] = (
                    points[i]+interp_vec*(points[i+1]-points[i]))
        #for cases where end_time == last point, we need to have this appended
        #values[-1] = points[-1]
        return values[valid_start:valid_end]


    #@classmethod
    #def generate_continuous_parameter(cls,parameters):
    #    '''generate a continously varying parameter between time points'''
    #    parameter_interpolation = parameters['parameter_interpolation']
    #    rate = parameters['parameter_rate']
    #    start_time = parameters['parameter_start_time']
    #    end_time = parameters['parameter_end_time']
    #    fs = parameters['sampling_rate']

    #    points = cls.generate_parameter_points(parameters)
    #    interp_vec = np.arange(0,1,1/(fs*rate)) #anything that goes from 0 to 1
    #    interp_len = len(interp_vec)
    #    if parameter_interpolation is 'linear':
    #        pass
    #    elif parameter_interpolation is 'cosine':
    #        interp_vec = 0.5-0.5*np.cos(np.pi*interp_vec)
    #    else:
    #        raise ValueError('interpolation type not supported')
    #    values = np.empty(np.ceil(fs*((end_time-start_time)+rate)))
    #    for i in xrange(len(points)-1):
    #        values[i*interp_len:(i+1)*interp_len] = (
    #                points[i]+interp_vec*points[i+1])
    #    return values[:np.ceil(fs*(end_time-start_time))]

    #@classmethod
    #def generate_integrated_parameter(cls,parameters):
    #    '''generate and integrate a continuously varrying parameter between time points'''
    #    #TODO needs a "starting value" parameter
    #    fs = parameters['sampling_rate']
    #    values = cls.generate_continuous_parameter(parameters)
    #    return np.cumsum(values/fs) #normalize to integrate over seconds

    @staticmethod
    def generate_parameter_points(parameters):
        '''generate control points for parameters'''
        min_p = parameters['parameter_min']
        max_p = parameters['parameter_max']
        rate  = parameters['parameter_rate']
        duration = parameters['duration']
        arr_len = np.ceil(duration*rate)+1
        #seed the random generator so we always have the same result
        np.random.seed(parameters['seed'])
        return (max_p-min_p)*np.random.rand(arr_len)

class DynamicMovingRipple(
        ContinuouslyVarying, FrequencyBanked):
    valid_stimulus = True
    cycles_per_octave_max = Parameter(4)
    cycles_per_octave_rate = Parameter(0.5)
    octaves_per_second_max = Parameter(40)
    octaves_per_second_rate = Parameter(0.3)
    modulation_depth = Parameter(40)
    spectrogram_filename = Parameter('spectrogram.bin',validator=validate_string)
    seed = Parameter(19870305)

    def __init__(self,parameters=None):
        '''generate cycles/octave and octaves/second'''
        super(self.__class__,self).__init__(parameters)
        self.parameters = self.default_parameters.copy()
        self.parameters.update(parameters)
        parameters = self.parameters

        parameters['parameter_max'] = parameters['cycles_per_octave_max']
        parameters['parameter_min'] = 0
        parameters['parameter_rate'] = parameters['cycles_per_octave_rate']
        self.cycles_per_octave = self.generate_parameter_points(parameters).tolist()

        parameters['parameter_max'] = parameters['octaves_per_second_max']
        parameters['parameter_min'] = 0
        parameters['parameter_rate'] = parameters['octaves_per_second_rate']
        self.octaves_per_second = self.generate_parameter_points(parameters).tolist()
        self.integrated_temporal_modulation = np.empty_like(self.octaves_per_second)
        self.integrated_temporal_modulation.fill(np.nan) #must be done on two lines
        self.integrated_temporal_modulation[0] = 0
        self.integrated_end = 0

        self.frequency_bank = self.generate_frequency_bank(parameters)
        self.initial_phases = 2*np.pi*np.random.rand(self.frequency_bank.shape[0])

    def integrate_temporal_modulation(self,parameters):
        parameters['parameter_rate'] = parameters['octaves_per_second_rate']
        parameters['parameter_points'] = self.octaves_per_second
        int_tm_list = self.integrated_temporal_modulation
        rate = parameters['parameter_rate']
        requested_start_time = parameters['parameter_start_time']
        end_time   = parameters['parameter_end_time']
        fs = parameters['sampling_rate']
        #find the nearest known point
        reference_point = min(self.integrated_end,int(np.floor(requested_start_time*rate)))
        reference_value = self.integrated_temporal_modulation[reference_point]
        #calculate tm from there
        actual_start_time = reference_point/rate
        parameters['parameter_start_time'] = actual_start_time
        tm = self.interpolate_points(parameters)
        #integrate and adjust to reference level
        int_tm = np.cumsum(tm)/fs
        int_tm += reference_value - int_tm[0]
        #update known points
        points = np.arange((end_time-actual_start_time)*rate,dtype=np.int16) #how many points
        local_points = np.int16(points/rate*fs) #where points are in integrated tm array
        self.integrated_temporal_modulation[reference_point+points] = int_tm[local_points]
        self.integrated_end = max(reference_point+points[-1],self.integrated_end)
        #now drop the part that wasn't requested
        requested_index = int((requested_start_time-actual_start_time)*fs)
        return int_tm[requested_index:]

    def get_waveform_and_spectrogram(self, parameters):
        parameters = parameters.copy() #don't let changes propagate out
        if parameters['parameter_start_time'] is None:
            parameters['parameter_start_time'] = 0
        if parameters['parameter_end_time'] is None:
            parameters['parameter_end_time'] = parameters['duration']
        start_time = parameters['parameter_start_time']
        end_time = parameters['parameter_end_time']
        mod_depth = parameters['modulation_depth']
        #generate the modulations
        parameters['parameter_rate'] = parameters['cycles_per_octave_rate']
        parameters['parameter_points'] = self.cycles_per_octave
        cyc_octave = self.interpolate_points(parameters)
        oct_sec = self.integrate_temporal_modulation(parameters)
        #generate the ripple
        mod_depth = parameters['modulation_depth']
        octaves = np.log2(self.frequency_bank/self.frequency_bank[0])
        spectrogram = ( mod_depth/2 * np.sin( 2*np.pi *
                np.outer(cyc_octave, octaves).T +
                np.tile(oct_sec, (octaves.shape[0],1))) - mod_depth/2)
        #convert to wave
        #advance frequency bank to phase at parameter_start_time
        start_phases = self.frequency_bank*2*np.pi+self.initial_phases
        time = np.arange(start_time,end_time,1/parameters['sampling_rate'])
        waveform = np.sum( 10**(spectrogram/20)*np.sin(2*np.pi*
                np.outer(self.frequency_bank,time) +
                np.tile(start_phases, (time.shape[0],1)).T), axis=0)
        return (waveform,spectrogram)

    def get_waveform(self, parameters):
        waveform, spectrogram = self.get_waveform_and_spectrogram(parameters)
        return waveform

    #def __init__(self,parameters=None):
    #    '''generate complete cycles/octave and octaves/second'''
    #    if parameters is None:
    #        parameters = self.default_parameters.copy()
    #    super(self.__class__,self).__init__(parameters)

    #    parameters['parameter_max'] = parameters['cycles_per_octave_max']
    #    parameters['parameter_min'] = 0
    #    parameters['parameter_rate'] = parameters['cycles_per_octave_rate']
    #    parameters['parameter_start_time'] = 0
    #    parameters['parameter_end_time'] = parameters['duration']
    #    self.cyc_octave = self.generate_continuous_parameter(parameters)

    #    parameters['seed'] += 1987
    #    parameters['parameter_max'] = parameters['octaves_per_second_max']
    #    parameters['parameter_min'] = -1*parameters['octaves_per_second_max']
    #    parameters['parameter_rate'] = parameters['octaves_per_second_rate']
    #    parameters['parameter_start_time'] = 0
    #    parameters['parameter_end_time'] = parameters['duration']
    #    self.oct_sec = self.generate_integrated_parameter(parameters)


    #def get_waveform_and_spectrogram(self, parameters):
    #    '''generate a block of the DMR, returns waveform and spectrogram'''
    #    if parameters['parameter_start_time'] is None:
    #        parameters['parameter_start_time'] = 0
    #    if parameters['parameter_end_time'] is None:
    #        parameters['parameter_end_time'] = parameters['duration']

    #    f_bank = self.generate_frequency_bank(parameters)
    #    f_phases = 2*pi*np.random.rand(len(f_bank))
    #    time = np.arange(0,parameters['duration'],1/parameters['sampling_rate'])

    #    mod_depth = parameters['modulation_depth']
    #    octaves = np.log2(f_bank/f_bank[0])
    #    spectrogram = ( mod_depth/2 * np.sin( 2*np.pi *
    #        np.outer(cyc_octave, octaves) +
    #        np.tile(oct_sec, (octaves,1))) - mod_depth/2 )
    #    waveform = np.sum( 10**(spectra/20)*np.sin(2*np.pi*
    #            np.outer(f_signals,t) + np.tile(f_phases, (1,len(time)))))
    #    return (waveform, spectrogram)

    #def get_waveform(self,parameters):
    #    '''generate only the waveform of the spectrogram'''
    #    waveform, spectrogram = self.get_waveform_and_spectrogram(parameters)
    #    return waveform

    ##instance method to allow other methods to maintain state
    #def write_dmr_in_chunks(self, parameters):
    #    self.cyc_octave.tofile(parameters['cycles_per_octave_filename'])
    #    self.cyc_sec.tofile(parameters['cycles_per_second_filename'])
    #    self.write_waveform_and_spectrogram_in_chuncks(parameters)
    #    '''step through and generate dmr in chuncks saving waveform and spectrogram'''
    #    step = parameters['parameter_chunk_time_step']
    #    dur  = parameters['duration']
    #    self.cyc_octave.tofile(parameters['cycles_per_octave_filename'])
    #    self.cyc_sec.tofile(parameters['cycles_per_second_filename'])
    #    waveform_file = open(parameters['output_file'],'wb')
    #    spectrogram_file = open(parameters['spectrogram_file'],'wb')
    #    for i in xrange( np.floor( dur / step)):
    #        parameters['parameter_start_time'] = i*step
    #        parameters['parameter_end_time'] = (i+1)*step
    #        waveform,spectrogram = self.get_waveform_and_spectrogram(parameters)
    #        waveform_file.write(waveform.tostring())
    #        spectrogram_file.write(spectrogram.tostring())
    #    #compute the tailing chunck as well
    #    parameters['parameter_start_time'] = np.floor(dur/step)
    #    parameters['parameter_end_time']   = dur
    #    waveform,spectrogram = self.get_waveform_and_spectrogram(parameters)
    #    waveform_file.write(waveform.tostring())
    #    spectrogram_file.write(spectrogram.tostring())
    #    waveform_file.close()

