# -*- coding: utf-8 -*-

'''Code to create a Poission distributed set of triggers'''

import numpy
import scipy.io

def generate_poission_times(scale,length):
    times = []
    cur_time = 0
    while cur_time < length:
        cur_dist = numpy.cumsum(numpy.random.exponential(scale,1000)) + cur_time
        cur_time = cur_dist[-1] #store last value before trimming excess times       
        if cur_dist[-1] > length: 
            cur_dist = cur_dist[cur_dist < length]
        times.extend(cur_dist)
    return times

def generate_regular_triple_block_times(regular_interval,triple_interval,
                        events_per_block,blocks,return_regular_times = False):
    times = []
    regular_times = []
    current_time = 0.0    
    for i in range(blocks):
        #set up the triple trigger first
        times.extend(
            [current_time,
             current_time+triple_interval,
             current_time+triple_interval*2])
        regular_times.append(current_time) #leave off the triple triggers
        current_time += regular_interval
        for j in range(events_per_block-1):
            times.append(current_time)
            regular_times.append(current_time)
            current_time += regular_interval
    if return_regular_times:
        return times, regular_times
    else:
        return times

def write_sw_in_blocks(times,length,fs,ttl_length,ttl_level,filename_sw,padding = 0):
    fh = open(filename_sw,'wb+')
    ttl_dur = numpy.ceil(ttl_length*fs)
    times = numpy.array(times) 
    #extend everything by the padding amount
    times += padding 
    length += padding*2    
    
    last_time = 0
    samples = 0.0
    for t in times:
        interval = t-last_time
        if interval > 0:
            #write any space for zeros
            segment = numpy.zeros(numpy.floor(interval*fs), dtype=numpy.int16)
            samples += segment.shape[0]            
            fh.write(segment.tostring())
            #write the TTL
            segment = ttl_level*numpy.ones(ttl_dur, dtype=numpy.int16)
            samples += segment.shape[0]             
            fh.write(segment.tostring())
        else:
            #write the rest of the TTL
            segment = ttl_level*numpy.ones(numpy.ceil(interval+ttl_dur), dtype=numpy.int16)
            samples += segment.shape[0]   
            fh.write(segment.tostring())
        last_time = samples/fs #use the actual samples written to make sure we're right
    #fill out to the end of the stimulus, covers time till end
    segment = numpy.zeros(numpy.floor((length-last_time)*fs), dtype=numpy.int16)
    fh.write(segment.tostring())


poission_scale = 0.25 #expected wait for the Poission distribution

regular_interval = 1
triple_interval = 0.05
events_per_block = 10
blocks = 120

length = 1199.666666666666666666666#60*20 #length in seconds
fs = 96000
ttl_length = 0.001
ttl_level = 2**(16-1)-1

random_seed = 2012
filename_sw = 'poission_L250ms_2012_04_09.sw'
filename_mat = 'poission_L250ms_2012_04_09.mat'

if __name__ == '__main__':
    if False:  
        length = regular_interval*events_per_block*blocks
        ttl_length = 0.005
        filename_sw = 'triggers_2012_02_22.sw'
        times = generate_regular_triple_block_times(
            regular_interval,triple_interval,events_per_block,blocks)
        write_sw_in_blocks(times,length,fs,ttl_length,ttl_level,filename_sw)
    else:    
        numpy.random.seed(random_seed)
        times = generate_poission_times(poission_scale,length)
        write_sw_in_blocks(times,length,fs,ttl_length,ttl_level,filename_sw)
        
        poission_notes = {
            'length':length,
            'fs':fs,
            'ttl_length':ttl_length,
            'ttl_level':ttl_level,
            'filename_sw':filename_sw,
            'poission_scale':poission_scale,        
            }
        scipy.io.savemat(filename_mat,poission_notes,oned_as='row')
    
