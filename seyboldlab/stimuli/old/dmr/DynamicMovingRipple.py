

'''DynamicMovingRipple.py 

Code to generate a dynamic moving ripple stimulus or a ripple noise stimulus.
For a detailed description of the method and controls, see Escabi and Schreiner
(2002).

Depends on Numpy, Scipy.io'''

import numpy
import scipy.io.wavfile
import time

# a heavily optomized version of the above
def generate_dmr_block(cyc_octave, oct_int, freqeuncy_rates, temporal_rates, t):    
    ''' generates the spectral profile at any block of time t '''
    #determine the length of everything
    f_spot = t/( 1.0/frequency_rates)
    t_spot = t/( 1.0/temporal_rates)
    f_unique, f_indicies = numpy.unique(numpy.floor(f_spot),return_inverse = True)
    t_unique, t_indicies = numpy.unique(numpy.floor(t_spot),return_inverse = True)
    r1 = numpy.zeros(t.shape)
    r2 = numpy.zeros(t.shape)
    
    for i,u in zip(range(len(f_unique)),f_unique):
        #zero pad the array for cubic interpolation
        f_p = cyc_octave[max(u-1,0):min(u+3,len(cyc_octave))]
        if len(f_p) < 4:
            if f_p[0] == cyc_octave[0]:
                f_p = numpy.concatenate([ numpy.zeros(1), f_p])
            else:
                f_p = numpy.concatenate([ f_p, f_p[-2:]])
                
        #hardcode cubic interpolation
        f_a0 = (f_p[3] - f_p[2]) - (f_p[0] - f_p[1])
        f_a1 = (f_p[0] - f_p[1]) - f_a0
        f_a2 = (f_p[2] - f_p[0])
        f_a3 = f_p[1]
        #scipy.interpolate wasn't working well. not sure why
        x = f_spot[f_indicies == i]
        r1[f_indicies == i] = f_a0*x**3 + f_a1*x**2 + f_a2*x + f_a3
    
    for i,u in zip(range(len(t_unique)),t_unique):
        #zero pad the array for cubic interpolation
        t_p = oct_int[max(u-1,0):min(u+3,len(oct_int))]
        if len(t_p) < 4:
            if t_p[0] == oct_int[0]:
                t_p = numpy.concatenate([ numpy.zeros(1), t_p])
            else:
                t_p = numpy.concatenate([ t_p, t_p[-2:]])
                
        #hardcode cubic interpolation
        t_a0 = (t_p[3] - t_p[2]) - (t_p[0] - t_p[1])
        t_a1 = (t_p[0] - t_p[1]) - t_a0
        t_a2 = (t_p[2] - t_p[0])
        t_a3 = t_p[1]
        #scipy.interpolate wasn't working well. not sure why
        x = t_spot[t_indicies == i]
        r2[t_indicies == i] = t_a0*x**3 + t_a1*x**2 + t_a2*x + t_a3
    return (r1,r2)

def generate_dmr_spectra(f_signals, modulation_depth, f_, t_):
    s1 = f_signals.shape[0]
    s2 = f_.shape[0]
    bank = numpy.tile(numpy.log2( f_signals/ f_signals[0]),(s2,1))
    return modulation_depth/2 * numpy.sin( 2* numpy.pi* 
        numpy.tile(f_,(s1,1))* bank.T  + numpy.tile(t_,(s1,1))) - modulation_depth/2    
    
#this method needs to be sped up
def generate_fast_dmr_waveform(f_signals,f_phases,generator,
        fs,modulation_depth,length):
    ''' given the frequency bank and a generator, requests the frequencies of
    the desired signal at each time step of fs '''
    for count in range(length[0],length[1]):
        print(count) 
        t = numpy.arange(count,count+1,1.0/fs)
        # get current dmr params
        f_,t_ = generator(t)
        # find the spectra
        spectra = generate_dmr_spectra(f_signals, modulation_depth, f_, t_)
        # convert to time domain
        values = (10**spectra/20)*numpy.sin(2*numpy.pi*
            numpy.outer(f_signals,t) + numpy.tile(f_phases,(spectra.shape[1],1)).T)
        signal = numpy.sum(values,axis=0)
    return signal

def generate_frequency_bank(start_f, stop_f, num_signals, spacing = 'log'):
    ''' generates a list of frequencies starting from start_f and going to
    stop_f inclusive with the desired frequency step pattern '''
    if stop_f < start_f:
        raise ValueError('frequency banke parameters must be increasing')
                
    if spacing == 'linear':
        return range(start_f,stop_f,(stop_f-start_f)/num_signals)
    elif spacing == 'log':
        octaves = numpy.log2(stop_f/start_f)
        points = numpy.arange(0,octaves,octaves/num_signals)
        return start_f*2**points
    else:
        raise ValueError('Incorrect frequency bank spacing parameter')


def generate_dmr_params(max_cyc_octave, frequency_rates, max_oct_sec,
        temporal_rates, length):
    ''' given the following values, computes the walk in ripple density and
    ripple rate

    max_cyc_octave - the maximum ripple density in cycles per octave
    frequency_rates - the sampling rate for changing ripple density
    max_oct_sec - the maximum slope of the ripples in octaves per second
    temporal_rates - the sampling rate for changing ripple rate
    length - the number of seconds of recording, will independently scale the
       other parameters.
    '''
    # calculate lengths
    f_len = numpy.ceil(frequency_rates*length) + 1
    t_len = numpy.ceil(temporal_rates*length) + 1
    # fill with random variables
    cyc_octave = max_cyc_octave*numpy.random.rand(f_len)
    oct_sec = max_oct_sec*numpy.random.rand(t_len)
    oct_int = numpy.cumsum(oct_sec)
    return (cyc_octave, oct_sec, oct_int)
    
def generate_complete_wav_file(f_signals,f_phases,generator,fs,modulation_depth,
                               length):
    return  generate_fast_dmr_waveform(
        f_signals,
        f_phases,
        generator,
        fs,
        modulation_depth,
        [0, length],
        )
        
def write_incremental_sw_file(f_signals, f_phases,generator,fs,modulation_depth,
                              length,filename_sw,max_value):
    #allow length to either be an integer or a list of one or two elements
    if type(length) == type(5):
        length = [0, length]
    elif len(length) == 1:
        length = [0, length[0]]
        
    fh = open(filename_sw,'wb')
    for i in range(length[0],length[1]):
        #signal = generate_dmr_waveform(
        signal = generate_fast_dmr_waveform(
            f_signals,
            f_phases,
            generator,
            fs,
            modulation_depth,
            [i, i+1],
            )
        #signal.tofile(fh)
        signal = numpy.int16(signal*max_value)      
        fh.write(signal.tostring())
        fh.flush()
    fh.close()

#defaults
max_cyc_octave = 4  #ripple density - cycles per octave
max_oct_sec = 20    #ripple speed - octaves per second
frequency_rates = 0.7 #rate of change in ripple density (Hz)
temporal_rates = 1.1  #rate of change in ripple speed (Hz)
length = 6#60*20        #approximate length in seconds of the stimulus.
modulation_depth = 40 #total modulation depth of the stimulus.

start_f = 4000 #base frequency in the frequency bank
stop_f = 36000 #top frequency in the frequency bank
num_signals = 200 #number of frequencies in the frequency bank

fs = 96000 #the sampling rate of the stimulus
max_value = 2**(16-1)-1 #the peak data value in the file

rand_seed = 2010 # a random seed for the random number generator
numpy.random.seed(rand_seed)

#filename_wave = 'DMR_2012_02_20.wav' #the filename to save as
#filename_mat = 'DMR_2012_02_20.mat'
#filename_sw = 'DMR_2012_02_20.sw'

if __name__=='__main__':
    from datetime import date
    today = date.strftime(date.today(),'%y_%m_%d')
    filename_wave ='DMR_'+today+'.wav'
    filename_mat  ='DMR_'+today+'.mat'
    filename_sw   ='DMR_'+today+'.sw'

    #at a minimum, length*rate > 2 must be true for both rates, 
    #otherwise interpolation will  not work
    if length*frequency_rates <= 2 or length*temporal_rates <= 2:
        raise ValueError(
              'For cubic interpolation to work, length*rate > 2 must be true')
    t1 = time.time()
    cyc_octave, oct_sec, oct_int = generate_dmr_params(
            max_cyc_octave,
            frequency_rates,
            max_oct_sec,
            temporal_rates,
            length)
    f_signals = generate_frequency_bank(
            start_f,
            stop_f,
            num_signals,
            )
    f_phases = numpy.random.rand(len(f_signals))*2*numpy.pi
    

    ripple_params = {
        'max_cyc_octave' : max_cyc_octave,
        'max_oct_sec' : max_oct_sec,
        'cyc_octtave' : cyc_octave,
        'oct_sec' : oct_sec,
        'oct_int' : oct_int,
        'frequency_rates' : frequency_rates,
        'temporal_rates' : temporal_rates,
        'length' : length,
        'modulation depth' : modulation_depth,
        'start_f' : start_f,
        'stop_f' : stop_f,
        'num_signals' : num_signals,
        'fs' : fs,
        'max_value' : max_value,
        'filename_wave' : filename_wave,
        'frequency_bank_f' : f_signals,
        'frequency_bank_phase' : f_phases, 
        'rand_seed' : rand_seed,
        }
    scipy.io.savemat(filename_mat,ripple_params,oned_as='row')    
    
    if True: #write the whole .wav for testing
        signal = generate_complete_wav_file(
                f_signals,
                f_phases,
                lambda t:generate_dmr_block(cyc_octave, oct_int, frequency_rates,
                    temporal_rates, t),
                fs,
                modulation_depth,
                length,
                )
        scipy.io.wavfile.write(filename_wave,fs,numpy.int16(signal))  
    else: #generate a .sw file for working with further
        write_incremental_sw_file(
            f_signals,
            f_phases,
            lambda t:generate_dmr_block(cyc_octave, oct_int, frequency_rates,
                temporal_rates, t),
            fs,
            modulation_depth,
            length,
            filename_sw,
            max_value,
            )
    print('total elapsed time is ' + str(time.time()-t1))
    



