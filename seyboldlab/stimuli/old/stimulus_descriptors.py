from __future__ import division
from numbers import Number
from collections import Iterable
import numpy as np

def validate_none(iterable):
    pass

def validate_number(iterable):
    if isinstance(iterable, Iterable):
        if False in [isinstance(x, Number, np.number, np.ndarray) 
                    for x in iterable]:
            raise ValueError('Validation for numbers failed')
    elif not isinstance(iterable, Number):
        raise ValueError('Validation for numbers failed')

def validate_enum(iterable,values):
    if not isinstance(iterable,str) and isinstance(iterable, Iterable):
        if False in [ (x in values) for x in iterable]:
            raise ValueError('Validation for enumuration failed')
    elif not iterable in values:
        raise ValueError('Validation for enumuration failed')

def validate_string(iterable):
    if isinstance(iterable, Iterable):
        if False in [ isinstance(x,str) for x in iterable]:
            raise ValueError('Validation for enumeration failed')
    elif not iterable in values:
        raise ValueError('Validation for enumeration failed')

class Parameter():
    def __init__(self, default_value, validator=validate_number):
        self.default_value = default_value
        self.validator = validator
        self.name=None

    def __repr__(self):
        return "<Parameter: {self.name}, default: {self.default_value}>".format(self=self)

    def __str__(self):
        return "Parameter: {0}".format(self.name)

class DeclarativeMeta(type):
    def __new__(meta, class_name, bases, new_attrs):
        import pdb; pdb.set_trace()
        cls = type.__new__(meta, class_name, bases, new_attrs)
        cls.__classinit__.im_func(cls, bases, new_attrs)
        return cls

class declaration_base():
    __metaclass__ = DeclarativeMeta

    def __classinit__(cls, bases, new_attrs):
        cls.parameters = {}
        for base in bases:
            try:
                cls.parameters.update( base.__dict__['parameters'])
            except:
                pass
        for key, value in [(key, value) for key,value 
                in new_attrs.items() if isinstance(value, Parameter)]:
            value.name = key
            print value
            value.validator(value.default_value)
            cls.parameters[key] = value
        cls.current_parameters = {}
        for key, value in cls.parameters.items():
            cls.current_parameters[key] = value.default_value

    def __init__(self):
        self.current_parameters = {}
        for key, value in self.parameters.items():
            self.current_parameters[key] = value.default_value
            #TODO update everything to instance methods

class sound_base(declaration_base):
    sampling_rate = Parameter(96000)
    max_output_value = Parameter(1)
    random_seed = Parameter(1987)

class sweep_base(sound_base):
    sweep_length = Parameter(1)
    runs = Parameter(1)

    @classmethod
    def get_buffer(cls):
        length = np.ceil( cls.current_parameters['sampling_rate'] * 
                          cls.current_parameters['sweep_length'])
        return np.zeros((length,))

    @classmethod
    def get_timing(cls):
        return np.arange(0, cls.current_parameters['sweep_length'],
                1.0/cls.current_parameters['sampling_rate'])


class enveloped_sound(sweep_base):
    duration  = Parameter(0.5)
    #ramp_type = Parameter("cosine",validator=validate_string)
    gate_type = Parameter("cosine",validator=
            lambda string:validate_enum(string,('cosine','ramp')))
    gate_time = Parameter(0.005)
    gain      = Parameter(0)

    @classmethod
    def get_envelope(cls):
        buffer = cls.get_buffer()
        length = int(np.ceil(cls.current_parameters['sampling_rate'] *
                          cls.current_parameters['duration']))
        inner_buffer = np.ones((length,))
        inner_buffer *= 10**(cls.current_parameters['gain']/20)        

        gate_length = int(np.ceil(
                cls.current_parameters['gate_time'] *
                cls.current_parameters['sampling_rate']))
        steps = np.arange(0,1,1/gate_length)
        if cls.current_parameters['gate_type'] is 'cosine':
            gate = 0.5-np.cos(steps*np.pi)/2
        if cls.current_parameters['gate_type'] is 'ramp':
            gate = steps
        indicies_front = range(0,gate_length)
        indicies_end   = range(length-1,length-gate_length,-1)
        inner_buffer[indicies_front] = gate
        inner_buffer[indicies_end] = gate
        buffer[0:inner_buffer.size] += inner_buffer
        return buffer
        

class tone(enveloped_sound):
    frequency = Parameter(4000)

    @classmethod
    def get_sound(cls):
        envelope = cls.get_envelope()
        timing   = cls.get_timing()
        frequency = cls.current_parameters['frequency']
        sound = np.sin(frequency*2*np.pi*timing)
        return sound*envelope

class noise(enveloped_sound):
    normalized_RMS = Parameter(0.5)

    @classmethod
    def get_sound(cls):
        envelope = cls.get_envelope()
        sound = np.random.randn(envelope.size)
        rms = np.sqrt(np.mean(np.square(sound)))
        sound *= cls.current_parameters['normalized_RMS']/rms
        return sound*envelope

class bandpass_noise(noise):
    cutoff_high = Parameter(64000)
    cutoff_low  = Parameter(4000)

    @classmethod
    def get_sound(cls):
        import pdb; pdb.set_trace()
        sound = cls.__bases__[0].get_sound() #had trouble with super
        #TODO filter out unwanted frequencies with an FFT cut
        rms = np.sqrt(np.mean(np.square(sound)))
        sound *= cls.current_parameters['normalized_RMS']/rms
        return sound

class continuous_base(sound_base):
    duration = Parameter(1)
    
class frequency_banked(sound_base):
    frequency_start = Parameter(4000)
    frequency_stop  = Parameter(32000)
    frequency_scale = Parameter('log', validator= lambda x:
            validate_enum(x,{'log','linear'}))
    num_signals = Parameter(200)

    def generate_frequency_bank(start_f, stop_f, num_signals, scale_f = 'log'):
        ''' generates a list of frequencies starting from start_f and going to
        stop_f inclusive with the desired frequency step pattern '''
        if stop_f < start_f:
            raise ValueError('frequency banke parameters must be increasing')
        if spacing == 'linear':
            return np.arange(start_f,stop_f,(stop_f-start_f)/num_signals)
        elif spacing == 'log':
            octaves = np.log2(stop_f/start_f)
            points = np.arange(0,octaves,octaves/num_signals)
            return start_f*2**points
        else:
            raise ValueError('Incorrect frequency bank spacing parameter')

class chunked_stimuli(continous_base):
    parameter_start_time = Parameter(None, validator=validate_none)
    parameter_end_time   = Parameter(None, validator=validate_none)
    parameter_time_step  = Parameter(1)
    
    def generate_sound_in_chunks(*args, **kwargs):
        step = kwargs['parameter_time_step']
        dur  = kwargs['duration']
        for i in xrange(np.floor(dur/step))
            kwargs['parameter_start_time'] = i*step
            kwargs['parameter_end_time'] = (i+1)*step
            self.get_sound(*args, **kwargs)
            #should ouput to a file or something...
        #compute the tail as well
        kwargs['parameter_start_time'] = np.floor(dur/step)
        kwargs['parameter_end_time']   = dur
        self.get_sound(*args, **kwargs)
        #should output to a file or something...


class continuously_varying(continuous_base, chunked_stimuli):
    parameter_min = Parameter(0)
    parameter_max = Parameter(1)
    parameter_rate = Parameter(1)
    parameter_interpolation = Parameter('cosine',validator= lambda x:
            validate_enum(x,{'cosine','linear'}))

    @staticmethod
    def generate_continuous_parameter(*args,**kwargs):
        parameter_interpolation = kwargs['parameter_interpolation']
        rate = kwargs['parameter_rate']
        start_time = kwargs['parameter_start_time']
        end_time = kwargs['parameter_end_time']
        fs = kwargs['sampling_rate']

        points = self.generate_parameter_points(*args,**kwargs)
        values = np.empty(np.ceil(fs*((end_time-start_time)+rate)))
        interp_vec = np.arange(0,1,rate/fs) #anything that goes from 0 to 1
        interp_len = len(interp_vec)
        if interpolation is 'linear':
            pass
        if interpolation is 'cosine':
            interp_vec = 0.5-0.5*np.cos(pi*interp_vec)
        else:
            raise ValueError('interpolation type not supported')
        for i in xrange(len(points)-1):
            values[i*interp_len:(i+1)*interp_len] = (
                    points[i]+interp_vec*points[i+1])
        return values[:np.ceil(fs*(end_time-start_time))]

    @staticmethod
    def generate_integrated_parameter(*args, **kwargs):
        fs = kwargs['sampling_rate']
        values = self.generate_continous_parameter(*args, **kwargs)
        return np.cumsum(values/fs) #normalize to integrate over seconds

    @staticmethod
    def generate_parameter_points(*args, **kwargs):
        min_p = kwargs['parameter_min']
        max_p = kwargs['parameter_max']
        rate  = kwargs['parameter_rate']
        duration = kwargs['duration']
        arr_len = np.ceil(duration*rate) + 1
        #seed the random generator so we always have the same result
        np.random.seed(kwargs['seed']) 
        return (max_p-min_p)*np.random.rand(arr_len)+1

class dynamic_moving_ripple(
        continuously_varying, frequency_banked, chunked_stimuli):
    cycles_per_octave_max = Parameter(4)
    cycles_per_octave_rate = Parameter(0.5)
    octaves_per_second_max = Parameter(40)
    octaves_per_second_rate = Parameter(0.3)
    modulation_depth = Parameter(40)

    def generate_dmr(*args, **kwargs):
        if kwargs['parameter_start_time'] is None:
            kwargs['parameter_start_time'] = 0
        if kwargs['parameter_end_time'] is None:
            kwargs['parameter_end_time'] = kwargs['duration']

        f_bank = self.generate_frequency_bank(*args, **kwargs)
        f_phases = 2*pi*np.random.rand(len(f_bank))
        time = np.arange(0,kwargs['duration'],1/kwargs['sampling_rate'])
        
        kwargs['parameter_max'] = kwargs['cycles_per_octave_max']
        kwargs['parameter_min'] = 0
        kwargs['parameter_rate'] = kwargs['cycles_per_octave_rate']
        cyc_octave = self.generate_continuous_parameter(*args, **kwargs)

        kwargs['parameter_max'] = kwargs['octaves_per_second_max']
        kwargs['parameter_min'] = -1*kwargs['octaves_per_second_max']
        kwargs['parameter_rate'] = kwargs['octaves_per_second_rate']
        oct_sec = self.generate_integrated_parameter(*args, **kwargs)

        mod_depth = kwargs['modulation_depth']
        octaves = np.log2(f_bank/f_bank[0])       
        spectrogram = ( mod_depth/2 * np.sin( 2*np.pi * 
            np.outer(cyc_octave, octaves) +
            np.tile(oct_sec, (octaves,1))) - mod_depth/2 )
        waveform = np.sum( 10**(spectra/20)*np.sin(2*np.pi*
                np.outer(f_signals,t) + np.tile(f_phases, (1,len(time)))))
        return (waveform, spectrogram)

    def get_sound(*args, **kwargs):
        waveform, spectrogram = self.generate_dmr(*args, **kwargs)
        return waveform
