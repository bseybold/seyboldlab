# -*- coding: utf-8 -*-

'''code to handle the .sw files that the schreiner lab routinely uses'''

import numpy
import os

Block_size = 10000000

def add(file_list, out_file):
    '''vector add the .sw files in the the input files and save as the output'''
    
    if type(file_list) is type('a string'):
        file_list = [file_list]
        
    if out_file in file_list:
        raise ValueError('Outfile cannot be the same as an input file.')
    
    if (len(numpy.unique([os.path.getsize(file_name) 
            for file_name in file_list])) != 1):
        raise ValueError('Input files must be the same size.')
        
    input_files = []
    for file_name in file_list:
        input_files.append(open(file_name,'rb'))
    fpo = open(out_file,'wb+')
    
    read_count = Block_size
    while read_count == Block_size:  
        blocks = []
        for ifile in input_files:
            blocks.append(ifile.read(Block_size))
        read_count = len(blocks[0])
        data = numpy.zeros(read_count/2)
        for block in blocks:
            data += numpy.fromstring(block, dtype=numpy.int16)
        fpo.write(data.tostring())
        
    fpo.close()
    for ifile in input_files:
        ifile.close()
    
def join(file_list, out_file):
    '''concatonate the .sw files in the the input files and save as the output'''
    
    if type(file_list) is type('a string'):
        file_list = [file_list]
        
    if out_file in file_list:
        raise ValueError('Outfile cannot be the same as an input file.')
        
    input_files = []
    for file_name in file_list:
        input_files.append(open(file_name,'rb'))
    fpo = open(out_file,'wb+')

    for ifile in file_list:    
        read_count = Block_size
        while read_count == Block_size:  
            block = ifile.read(Block_size)
            read_count = len(block)
            fpo.write(block)
        ifile.close()
    fpo.close()        
    
def scale(input_file, out_file, scale_factor):
    '''scale the .sw file in the the input file and save as the output'''
        
    if out_file == input_file:
        raise ValueError('Outfile cannot be the same as an input file.')

    input_file = open(input_file,'rb')
    fpo = open(out_file,'wb+')
    
    read_count = Block_size
    while read_count == Block_size:  
        block = input_file.read(Block_size)
        read_count = len(block)
        data = numpy.fromstring(block, dtype=numpy.int16) * scale_factor
        fpo.write(data.tostring())
        
    input_file.close()
    fpo.close()
    
def zeropad(input_file, output_file, samples):
    '''zero pads the .sw file at the beginning and end then saves as the output'''
        
    if output_file == input_file:
        raise ValueError('Outfile cannot be the same as an input file.')
        
    input_file = open(input_file,'rb')
    fpo = open(output_file,'wb+')
    
    fpo.write('\x00'*samples*2)
    read_count = Block_size
    while read_count == Block_size:  
        block = input_file.read(Block_size)
        read_count = len(block)
        data = numpy.fromstring(block, dtype=numpy.int16)
        fpo.write(data.tostring())
        
    input_file.close()
    fpo.write('\x00'*samples*2)
    fpo.close()    
    
    
def towave(file_list,out_file = None,fs = 96000, padding = 0):
    '''convert a .sw files to a .wav file. Channel order is order in file_list.
    This function is not Python3 compatable'''
    
    if type(file_list) is type('a string'):
        file_list = [file_list]
    # unfortunately not ready for release
    if len(file_list) > 1:
        raise NotImplementedError(
            'Sorry, we cannot yet handle more than 1 channel')

    if out_file == None:
        out_file = file_list[0] 
        cutoff = out_file.find('.sw')
        if cutoff != -1:
            out_file = out_file[:cutoff]
        out_file += '.wav'

    if out_file in file_list:
        raise ValueError('Outfile cannot be the same as an input file.')
    
    if (len(numpy.unique([os.path.getsize(file_name) 
            for file_name in file_list])) != 1):
        raise ValueError('Input files must be the same size.')
        
    
    
    frame_size = 2 #we are working with sixteen bit files (2 bytes)
    
    # blocks must be frame aligned
    local_block_size = Block_size - (Block_size % frame_size) 
        
    
    import wave
    #open the files
    input_files = []
    for file_name in file_list:
        input_files.append(open(file_name,'rb'))
    ofile = wave.open(out_file,'wb')
    #set the parameters
    ofile.setnchannels(len(input_files))
    ofile.setsampwidth(frame_size)
    ofile.setframerate(fs)
    #apply any padding
    ofile.writeframes('\x00'*frame_size*numpy.ceil(padding*fs))
    #convert to .wav format
    read_count = local_block_size
    total = 0
    while read_count == local_block_size:
        #iterate through each file for the next round of input blocks
        blocks = []
        for ifile in input_files:
            blocks.append(ifile.read(local_block_size))
        read_count = len(blocks[0])
        total += read_count
        #interleave all of the channels, but keep working with the raw bytes
        data = []
        for i in range(read_count/frame_size):
            data.extend([block[(i*frame_size):(i*frame_size+frame_size)]
                for block in blocks]) 
        #join all the frames with nothing
        ofile.writeframes(''.join(data)) 
    #apply any padding
    ofile.writeframes('\x00'*frame_size*numpy.ceil(padding*fs))
    ofile.close()
    
