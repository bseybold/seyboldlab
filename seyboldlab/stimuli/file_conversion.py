'''file_conversion.py

Module to work with binary files on disk and convert to .wav format'''

import numpy as np
import os


BLOCK_SIZE = 1000000

def write(file, data, dtype=np.int16):
    '''
    convenience function to write data to file with a specified dtype

    (Default: numpy.int16). If no file extension is specified, this function
    will automatically append one as appropriate (equivalent to the numpy dtype
    as a string (int16='h',int32='l',float32='f',float64='d'). Returns the new
    filename string. If file is a file object, this will report back the name
    of the file (or the file object if no file.name method)

    inputs:
        file
            the name of the file to write data to
        data
            the data to write to the file
        dtype
            default:numpy.int16 - the format to save the data in
    '''
    if isinstance(file,str) and '.' not in os.path.basename(file):
        file += '.'+np.sctype2char(dtype)
    np.array(data,dtype=dtype).tofile(file)
    if not isinstance(file,str):
        try:
            file = file.name
        except:
            pass
    return file

def add(file_list, out_file, dtype=np.int16):
    '''
    vector add the files in the input list and save as the output

    file_list can be either a single string or an interable. Files must be the
    same length
    dtype specifies the binary format to interpret the file as. defaults to int16

    inputs:
        file_list
            a single string or an iterable of file names to add together
        out_file
            a string specifying the file to save the output in
        dtype
            default:numpy.int16 - the format to read and write the data as
    '''
    if type(file_list) is type('a string'):
        file_list = (file_list,)
    if out_file in file_list:
        raise ValueError('Outfile cannot be the same as an input file.')
    if (len(np.unique([os.path.getsize(file_name)
            import pdb; pdb.set_trace()  # XXX BREAKPOINT
            for file_name in file_list])) != 1):
        raise ValueError('Input files must be the same size.')

    input_files = []
    for file_name in file_list:
        input_files.append(open(file_name,'rb'))
    fpo = open(out_file,'wb+')

    read_count = BLOCK_SIZE
    while read_count == BLOCK_SIZE:
        blocks = []
        for ifile in input_files:
            blocks.append(ifile.read(BLOCK_SIZE))
        read_count = len(blocks[0])
        data = np.zeros(read_count/2,dtype=dtype)
        for block in blocks:
            data += np.fromstring(block, dtype=dtype)
        fpo.write(data.tostring())
    fpo.close()
    for ifile in input_files:
        ifile.close()

def join(file_list, out_file, dtype=None):
    '''
    concatonate the files in the input list and save as the output

    file_list can be either a single string or an interable
    dtype is ignored for this function

    inputs:
        file_list
            a single string or an iterable of file names to concatonate
        out_file
            a string specifying the file to save the output in
        dtype
            ignored for this function
    '''
    if type(file_list) is type('a string'):
        file_list = (file_list,)
    if out_file in file_list:
        raise ValueError('Outfile cannot be the same as an input file.')

    input_files = []
    for file_name in file_list:
        input_files.append(open(file_name,'rb'))
    fpo = open(out_file,'wb+')

    for ifile in file_list:
        read_count = BLOCK_SIZE
        while read_count == BLOCK_SIZE:
            block = ifile.read(BLOCK_SIZE)
            read_count = len(block)
            fpo.write(block)
        ifile.close()
    fpo.close()

def scale(input_file, out_file, scale_factor, dtype=np.int16):
    '''
    scale the input file in the the input file and save as the output

    inputs:
        input_file
            a string specifying the file to scale
        out_file
            a string specifying the file to save the output in
        scale_factor
            a number to linearly scale the input by
        dtype
            default:numpy.int16 - the format to read and write the data as
    '''
    if out_file == input_file:
        raise ValueError('Outfile cannot be the same as an input file.')

    input_file = open(input_file,'rb')
    fpo = open(out_file,'wb+')

    read_count = BLOCK_SIZE
    while read_count == BLOCK_SIZE:
        block = input_file.read(BLOCK_SIZE)
        read_count = len(block)
        data = np.fromstring(block, dtype=dtype) * scale_factor
        fpo.write(data.tostring())
    input_file.close()
    fpo.close()

def zeropad(input_file, output_file, samples, tail_samples=None, dtype=np.int16):
    '''
    zero pads the file at the beginning and end then saves as the output

    inputs:
        input_file
            a string specifying the input file
        output_file
            a string specifying the file to save the output to
        samples
            the number of samples to pad with zeros at the front of the file
        tail_samples
            optional - number of samples to pad at the end of the file
            defaults to be the same as samples
        dtype
            default:numpy.int16 - the format to read and write the data as
    '''
    if output_file == input_file:
        raise ValueError('Outfile cannot be the same as an input file.')
    if tail_samples is None:
        tail_samples = samples

    input_file = open(input_file,'rb')
    fpo = open(output_file,'wb+')

    head_padding = np.zeros(samples,dtype=dtype)
    fpo.write(head_padding.tostring())
    read_count = BLOCK_SIZE
    while read_count == BLOCK_SIZE:
        block = input_file.read(BLOCK_SIZE)
        read_count = len(block)
        data = np.fromstring(block, dtype=dtype)
        fpo.write(data.tostring())
    input_file.close()
    tail_padding = np.zeros(tail_samples, dtype=dtype)
    fpo.write(tail_padding.tostring())
    fpo.close()

def towave(file_list, out_file = None, fs = 96000, padding_time = 0,
        dtype=np.int16):
    '''convert files to a .wav file.

    Channel order is order in file_list.
    Floating point files are scaled and rounded into the 16 or 32 bit integers

    inputs:
        file_list
            a single string or list with the files representing channels
        out_file
            optional - defaults to original filename with extension replaced by .wav
        fs
            optional - default:96000 Hz - the sampling rate
        padding_time
            optional - defualt:0 s - seconds to pad the stimulus with zeros
        dtype
            optional - defualt:numpy.int16 - the binary format to use
    '''

    if type(file_list) is type('a string'):
        file_list = (file_list,)
    # unfortunately not ready for release
    #if len(file_list) > 1:
    #    raise NotImplementedError(
    #        'Sorry, we cannot yet handle more than 1 channel')

    # generate out_file from first input file name
    if out_file == None:
        out_file = file_list[0]
        cutoff = out_file.rfind('.') #get the filetype .
        if cutoff != -1:
            out_file = out_file[:cutoff]
        out_file += '.wav'
    if out_file in file_list:
        raise ValueError('Outfile cannot be the same as an input file.')
    if (len(np.unique([os.path.getsize(file_name)
            for file_name in file_list])) != 1):
        raise ValueError('Input files must be the same size.')
    if isinstance(dtype, np.integer):
        scale_factor = None
    else:
        #scale floating point values into integers
        scale_factor = 2**(frame_size-1)-1
    #calculate the frame size from the datatype size
    frame_size = 2*np.ceil(dtype.itemsize/2)
    if frame_size not in (2,4):
        raise TypeError('Only dtypes for 2 and 4 words are implemented')

    # blocks must be frame aligned
    local_block_size = BLOCK_SIZE - (BLOCK_SIZE % frame_size)
    import wave
    #open the files
    input_files = []
    for file_name in file_list:
        input_files.append(open(file_name,'rb'))
    ofile = wave.open(out_file,'wb')
    #set the parameters
    ofile.setnchannels(len(input_files))
    ofile.setsampwidth(frame_size)
    ofile.setframerate(fs)
    #apply any padding
    ofile.writeframes('\x00'*frame_size*np.ceil(padding*fs))
    #convert to .wav format
    read_count = local_block_size
    while read_count == local_block_size:
        #iterate through each file for the next round of input blocks
        blocks = []
        for ifile in input_files:
            block = np.fromstring(ifile.read(local_block_size),dtype=dtype)
            if scale_factor is not None:
                block *= scale_factor
                if frame_size == 2:
                    block = np.array(block,dtype=np.int16)
                else:
                    block = np.array(block,dtype=np.int32)
            blocks.append(block.tolist())
        read_count = len(blocks[0]) #used to test when we're done with the file
        data = zip(*blocks)
        data_string = np.array(data).tostring()
        ofile.writeframes(data_string)
    #apply any padding
    ofile.writeframes('\x00'*frame_size*np.ceil(padding*fs))
    ofile.close()

