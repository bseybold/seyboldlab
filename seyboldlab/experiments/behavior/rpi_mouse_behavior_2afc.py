import sys
sys.coint_flags = 0x8
import random
import time
import os
import pygame
import scipy.io

debug = 20
class dummy_pin():
    def __init__(self):
        self.value = 0

#stimuli distributions to draw from. all values in each are considered equal
target_left_stimuli = ['left.wav']
target_right_stimuli = ['right.wav']
adversive_stim = None

#probability of drawing from the left or right target distributions
p_target_left = 0.5

#polling frequency, how often to poll for io
polling_freq = 0.01

#the maximum number of trials
n_trials_max = 2000

#min and max inter-trial-interval after success or failure
initiation_waittime = 1
response_period = 5
iti_success = [0,1]
iti_failure = [3,4]

#inputs and outputs to use. the number refers to the RPi gpio pin
import rpi_mouse_behavior_firmata as firmata
firmata.PORT = '\\.\COM4'

inputs = { 
    'left_nosepoke':firmata.Pin(2,firmata.INPUT),#dummy_pin()
    'center_nosepoke':firmata.Pin(6,firmata.INPUT),#dummy_pin(),
    'right_nosepoke':firmata.Pin(10,firmata.INPUT),#dummy_pin(),
}
outputs = {
    'left_light':firmata.Pin(4,firmata.OUTPUT),#dummy_pin(),
    'center_light':firmata.Pin(8,firmata.OUTPUT),#dummy_pin(),
    'right_light':firmata.Pin(12,firmata.OUTPUT),#dummy_pin(),
    'reward':firmata.Pin(13,firmata.OUTPUT),#dummy_pin(),
}

class Experiment():

    def __init__(self,savepath):
        self.savepath = savepath
        self.trials = []
        self.total_trials = int(0)
        self.successes = int(0)
        self.failures = int(0)
        self.timeouts = int(0)
        #create the save dict, shouldn't need to update due to references
        str_inputs = {}
        for key in inputs.keys():
            str_inputs[key] = str(inputs[key])
        str_outputs = {}
        for key in outputs.keys():
            str_outputs[key] = str(outputs[key])
        self.save_struct = {'inputs':str_inputs,
                            'outputs':str_outputs,
                            'targets_left':target_left_stimuli,
                            'targets_right':target_right_stimuli,
                            'p_target_left':p_target_left,
                            'polling_freq':polling_freq,
                            'iti_success':iti_success,
                            'iti_failure':iti_failure,
                            'trials':self.trials,
                            'successes':self.successes,
                            'failures':self.failures,
                            'total_trials':self.total_trials,
                            'timeouts':self.timeouts,
                            'savepath':self.savepath,
                            'time_experiment_start':time.time(),
                           }
        pygame.mixer.init()
        if debug > 0:
            print('Starting experiment')        

        waittime = iti_failure[1]
        #run the program loop, can exit by returning None
        try:
            while waittime:
                waittime = self.queue_next_trial(waittime)
        finally:
            for pin in outputs.values():
                pin.value = 0
            firmata.exit()
            

    def queue_next_trial(self,waittime):
        self.save()
        #exit if we've completed all the trials we wanted
        if self.total_trials >= n_trials_max:
            return None
        self.next_trial = {}
        #determine if it's a left or right stimulus, then store information
        #based on that choice
        if random.random() > p_target_left:
            stimulus = random.choice(target_left_stimuli)
            target = 'left'
            target_io = inputs['left_nosepoke']
        else:
            stimulus = random.choice(target_right_stimuli)
            target = 'right'
            target_io = inputs['right_nosepoke']
        self.next_trial['stimulus'] = stimulus
        self.next_trial['target'] = target
        self.next_trial['target_io'] = target_io
        #wait a random time and then wait for a nosepoke
        return self.wait_for_initiation(waittime)

    def wait_for_initiation(self,waittime):
        #store how long we're waiting for
        self.next_trial['waittime'] = waittime
        if debug > 2:
            print('Waiting '+str(waittime)+' seconds')
        time.sleep(waittime)
        if debug > 1:
            print('starting trial '+str(self.total_trials+1))
        self.next_trial['time_center_port_on'] = time.time()
        outputs['center_light'].value = 1
        #wait until there's a nosepoke on center
        while inputs['center_nosepoke'].value == 0:
            time.sleep(polling_freq)
            if debug > 5 and random.random() > 0.99:
                inputs['center_nosepoke'].value = 1 
        if debug > 5:
            inputs['center_nosepoke'].value = 0
        outputs['center_light'].value = 0
        time.sleep( initiation_waittime)
        self.next_trial['time_center_port_activated'] = time.time()
        return self.run_trial(response_period)

    def run_trial(self,timeout = 3):
        '''poll the inputs until a response is discovered or a timeout'''
        self.play_stimulus(self.next_trial['stimulus'])
        outputs['left_light'].value = 1
        outputs['right_light'].value = 1
        t = 0
        while t < timeout:
            for key,pin in inputs.items():
                if debug > 5 and random.random() > 0.998:
                    pin.value = 1
                if pin.value == 1:
                    if debug > 2:
                        print t
                    self.next_trial['time_response_time'] = time.time()
                    self.next_trial['response_port'] = key
                    outputs['left_light'].value = 0
                    outputs['right_light'].value = 0
                    if debug > 2:
                        pin.value = 0
                    if pin is self.next_trial['target_io']:
                        #need to save a string representation of IO
                        self.next_trial['target_io'] = str(
                                self.next_trial['target_io'])
                        return self.success(key)
                    else:
                        self.next_trial['target_io'] = str(
                                self.next_trial['target_io'])
                        return self.failure(key)
            t += polling_freq
            time.sleep(polling_freq)
        if debug > 1:
            print('timeout')
        outputs['left_light'].value = 0
        outputs['right_light'].value = 0
        self.next_trial['target_io'] = str(
                self.next_trial['target_io'])
        self.next_trial['time_response_time'] = -1
        self.next_trial['response_port'] = 'None'
        return self.failure(None)

    def success(self,input_key):
        if debug > 1:
            print('success')
            self.play_stimulus('success.wav')
        self.next_trial['outcome'] = 'success'
        self.trials.append(self.next_trial)
        self.successes += 1
        self.total_trials += 1
        #toggle reward to the animal
        outputs['reward'].value = 1
        outputs['reward'].value = 0
        #wait for the next trial
        return (random.random()*(iti_success[1]-iti_success[0]) +
                    iti_success[0])
        

    def failure(self,input_key):
        if debug > 1:
            print('failure')
            self.play_stimulus('failure.wav')
        self.next_trial['outcome'] = 'failure'
        self.trials.append(self.next_trial)
        self.total_trials += 1
        self.failures += 1
        self.play_stimulus(adversive_stim)
        if input_key is None: #we timed out
            self.timeouts += 1
        #wait for the next trial
        return (random.random()*(iti_failure[1]-iti_failure[0]) +
                    iti_failure[0])

    def play_stimulus(self,stimulus):
        '''play the given stimulus to the animal'''
        if not stimulus:
            return
        if debug > 1:
            print(stimulus)
        if debug < 5:
            pygame.mixer.Sound(stimulus).play()

    def save(self):
        '''mediates saving through a temporary file for atomaticity'''
        if '.mat' != self.savepath[-4:]:     
            self.savepath += '.mat'
        temp_savepath = self.savepath + '_temp.mat'
        #it is necessary to manually update integers, not lists
        self.save_struct['failures'] = self.failures
        self.save_struct['successes'] = self.successes
        self.save_struct['timeouts'] = self.timeouts
        self.save_struct['total_trials'] = self.total_trials
        if debug > 4:
            print(self.save_struct)
        #put this in a try block to prevent ^c problems
        try:
            scipy.io.savemat(temp_savepath,self.save_struct,oned_as='row')
            if os.access(self.savepath, os.F_OK):
                os.remove(self.savepath)
            os.rename(temp_savepath,self.savepath)
        except KeyboardInterrupt as e:
            scipy.io.savemat(temp_savepath,self.save_struct,oned_as='row')
            if os.access(self.savepath, os.F_OK):
                os.remove(self.savepath)
            os.rename(temp_savepath,self.savepath)
            raise e
            


if __name__ == '__main__':
    import sys
    #use the last argument for a filename, either this script or something
    Experiment(sys.argv[-1])
