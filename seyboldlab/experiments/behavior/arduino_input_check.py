import pyfirmata
import time

DELAY = 0.1 # A 2 seconds delay

# Adjust that the port match your system, see samples below:
# On Linux: /dev/tty.usbserial-A6008rIF, /dev/ttyACM0, 
# On Windows: \\.\COM1, \\.\COM2
PORT = '\\.\COM4'

# Creates a new board 
print('setting up connection to the board')
board = pyfirmata.Arduino(PORT)

digital_4 = board.get_pin('d:4:o')
digital_8 = board.get_pin('d:8:o')
digital_12 = board.get_pin('d:12:o')
outputs = [digital_4, digital_8, digital_12]

digital_2 = board.get_pin('d:2:i')
digital_6 = board.get_pin('d:6:i')
digital_10 = board.get_pin('d:10:i')
inputs = [digital_2, digital_6, digital_10]

io = zip(inputs,outputs)

it = pyfirmata.util.Iterator(board)
it.start()
for pin in inputs:
    pin.enable_reporting()
print('ready to start')
# Loop for blinking the led
try:
    while True:
        for i, pair in enumerate(io):
            if pair[0].read():
                pair[1].write(1)
            else:
                pair[1].write(0)
            print('%i: %s'%(i, pair[0].read()))
        time.sleep(DELAY)
finally:
    for pin in outputs:
        pin.write(0)
    board.exit()
