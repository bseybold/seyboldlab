import os
import TDTHandler
import BSSpikeSort
import scipy.io

large_number = 100000
trial_store = 'Tria'
stores_that_change_each_trial = ['Tria','LiGt','LiOn','LiP1','LiP2',
    'Gate','Freq','Band','Atte','NSIn','Prm1','Prm2','Prm3','Prm4','Prm5']
epoc_stores = ['Tria','PadE','Stim']

class TTankMiner():

    def __init__(self,path):
        self.blocks = os.listdir(path)
        self.curBlock = None
        self.open_block(self.blocks[0])

    def __del__(self):
        if self.curBlock:
            self.curBlock.CloseTank()
            self.curBlock.ReleaseServer()

    def open_block(self,name):
        if self.curBlock:
            self.curBlock.CloseTank()
            self.curBlock.ReleaseServer()
        self.curBlock = TDTHandler.open_block_from_dir(path,name)
        self.curNotes = TDTHandler.parse_block_notes(self.curBlock)
        self.stores   = [x[u'StoreName'] for x in self.curNotes]
        self.channels = TDTHandler.get_file_list(path+'\\'+name)

    def get_epocs(self,store_name):
        return self.curBlock.GetEpocsV(store_name,0,0,large_number)

    def convert_epocs(self,epocs):
        return tuple([ (epocs[0][x],epocs[1][x],epocs[2][x],epocs[3][x]) for
                        x in xrange(len(epocs[0]))])

    def get_channel_spike_times(self,channel_file):
        return BSSpikeSort.collect_std_events(channel_file,-4,
                    TDTHandler.TDTHandler)

    def spikes_by_epoc(self,channel_file):
        waveforms, times = self.get_channel_spike_times(channel_file)

        main_epoc = self.convert_epocs(self.get_epocs(trial_store))
        
        timed_epocs = {}
        for store in self.stores if store in epoc_stores:
            times_epocs[store] = self.convert_epocs(self.get_epocs(store))

        other_epocs = {}
        for store in self.stores if store in stores_that_change_each_trial:
            other_epocs[store] = self.convert_epocs(self.get_epocs(store))
        
        by_epoc = []   # may need to be a numpy object array
        for x in xrange(len(main_epoc)):
            epoc = {}
            for store in timed_epocs.keys():
                epoc[store] = time_epocs[store][x]
            for store in other_epocs.keys():
                epoc[store] = other_epocs[store][x]
            epoc['spiketimes'] = (times[ (times > main_epoc[x][1] and
                times < main_epoc[x][2]) ] - main_epoc[x][1])
            by_epoc.append(epoc)
        return by_epoc

    def save_by_epoc(self,by_epoc,filename='by_epoc.mat'):
        scipy.io.savemat(filename,{'by_epoc':by_epoc},oned_as:'row')

    def save_all_channels(self,filename='all_channels_by_epoc'):
        channels_by_epoc = [self.spikes_by_epoc(channel_file) for 
                        channel_file in self.channels] 
        scipy.io.savemat(filename,{'all_channels':channels_by_epoc},oned_as
                        ='row')

