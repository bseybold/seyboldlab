
'''Code to find the maximally informative dimensions of a stimulus for a spike
train'''
from numpy import *
from scipy.optimize import fmin_bfgs
import pdb

def find_sta(s,spikes):
    # subtract mean rate from rate at each point
    # reshape into the same
    return mean(s_shape(spikes-mean(spikes),s) * s,1)

def v_norm(v):
    ''' normalize a vector '''
    return v/sqrt(dot(v,v))

def s_shape(arr,s):
    ''' tile a vector to shape s. it must be same length as s '''
    if arr.shape[-1] == s.shape[1]:
        return tile(arr,(s.shape[0],1))
    else:
        raise ValueError('vectors must be the same length as array')


class MID:
    def __init__(self, stimulus = None, spikes = None):
        self.use_STA = True
        self.max_iterations = 30
        self.n_bins = 15
        self.display = True
        self.n_mids = 1

        self.found_mids = []
        self.I_tot = None

        self.v = None
        self.s = None
        self.spikes = None
        self.P_x = None
        self.P_x_spikes = None
        self.x_ind = None
        self.last_v = None

        if stimulus is not None and spikes is not None:
            self.set_inputs(stimulus,spikes)

    def set_inputs(self,stimulus,spikes,num_bins = 15):
        self.s = stimulus
        self.spikes = spikes
        self.n_bins = num_bins

        #determine the total information in the spike train
        #TODO why divide by max(spikes)?
        mean_rate = float(sum(spikes))/len(spikes)/max(spikes)
        pdb.set_trace()
        self.I_tot = -log2(mean_rate) + sum(spikes[spikes!=0] * \
                        log2(spikes[spikes!=0]))/sum(spikes)

    def find_next_mid(self):
        if len(self.found_mids) == 0 and self.use_STA:
            self.v = find_sta(self.s,self.spikes)
        else:
            self.v = random.rand(self.s.shape[0],1)


        run_info = fmin_bfgs(
            f = self.calculate_information,
            x0 = self.v,
            fprime = self.calculate_gradient,
            gtol = 0,
            maxiter = self.max_iterations,
            full_output = True,
            retall = True,
            disp = True,
            )
        self.v = None
        self.found_mids.append(run_info)

        #remove the found dimension from the stimulus
        found_v = run_info[0]
        self.s = self.s-dot(dot(found_v,found_v.T),self.s)

        return found_v

    def calculate_information(self,v):
        '''calculate the info between the filter, stimulus, and spikes'''
        # v = the filter
        v = v_norm(v)
        self.v = v
        # s = the stimulus (as frames)
        s = self.s
        # spikes = the spike rate vector
        spikes = self.spikes
        # x = the projections
        x = dot(v.T,s)
        print('v :')
        print(v)

        # determine the bins of the histogram, we'll just use the builtin
        step_size = (x.max() - x.min())/self.n_bins

        #compute Pv(x)
        P_x,edges = histogram(x,self.n_bins,density=True)
        print ('p_x :')
        print (P_x)

        #manually compute x_ind
        x_ind = zeros(s.shape[1])
        for i in range(len(P_x)):
            x_ind[ logical_and( x >= edges[i], x < edges[i+1]) ] = i
        #assign all values equal to the highest edge into the last bin
        #this is consistent with the python histogram function
        x_ind[x == edges[-1]] = len(edges)-2
        print('x_ind :')
        print(x_ind)

        #compute Pv(x|spikes)
        P_x_spike = zeros(P_x.shape)
        for i in range(len(P_x)):
            #should work fine with multiple events in a bin
            P_x_spike[i] = sum(spikes[x_ind == i])
        if sum(P_x_spike) != 0:
            P_x_spike /= sum(P_x_spike)
        print('P_x_spike :')
        print(P_x_spike)

        #compute the information, I
        ind = [P_x_spike != 0]
        I = -sum( P_x_spike[ind] * log2(P_x_spike[ind]) / P_x[ind])

        print('I :')
        print(I)

        self.P_x = P_x
        self.P_x_spike = P_x_spike
        self.x_ind = x_ind
        self.last_v = v

        return  self.I_tot - I

    def calculate_gradient(self,v):
        v = v_norm(v)
        if any(v != self.last_v):
            self.calculate_information(v)
        s = self.s
        spikes = self.spikes
        P_x = self.P_x
        P_x_spike = self.P_x_spike
        x_ind = self.x_ind

        #compute <s|x> and <s|x,spike> average stimulus in each bin, numerically
        s_x = zeros((s.shape[0],P_x.shape[-1]))
        s_x_spike = zeros(s_x.shape)
        for i in range(len(P_x)):
            temporary = s[: , x_ind == i]
            #control for untested conditions
            if(min(temporary.shape) == 0):
                temporary = zeros(s.shape)
            s_x[:,i] = mean(temporary,1)

            temporary_2 = spikes[ x_ind == i]
            if sum(temporary_2):
                s_x_spike[:,i] = dot(temporary,temporary_2) /sum(temporary_2)
        print('S_x :')
        print(s_x)
        print('S_x_spike :')
        print(s_x_spike)

        #compute d/dx [Pv(X|spike)/Pv(x)], numerically
        deriv = diff(P_x_spike / (P_x + spacing(1))) #spacing(1) prevents /0 err

        deriv = append( deriv, array( deriv[-1] ) ) # extend by one
        deriv[1:-1] = (deriv[0:-2] + deriv[1:-1]) / 2 # resmooth
        print('deriv :')
        print(deriv)

        #compute the gradient, dI
        dI = -sum( s_shape(P_x,s_x) * (s_x_spike - s_x) * s_shape(deriv, s_x),1)
        temporary = dot(dI,v)/dot(v,v)
        dI = dI - dot(temporary,v) #remove the projection of onto v from dI
        print('dI :')
        print(dI)

        #dI = random.random(dI.shape)-0.5
        return dI

if __name__ == '__main__':
    test1_stimulus = array([[ 0, 0, 0, 0, 1, 1, 1, 1,1,],
                            [ 1, 0, 1, 0, 1, 0, 1, 0,1,]])
    test1_response = array([  1, 0, 1, 0, 0, 1, 0, 1,1])
    mid = MID(test1_stimulus,test1_response)
    mid.use_STA = False
    v = mid.find_next_mid()
    print(v)
