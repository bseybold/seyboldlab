import numpy
import scipy.io

def spectrogram_to_frames(spectrogram,n_t_steps):
    end = spectrogram.shape[0]-n_t_steps
    frames = numpy.zeros((end, spectrogram.shape[1]*n_t_steps),dtype = 
                        numpy.float16)
    for i in range(n_t_steps):
        frames[:end, i*spectrogram.shape[1]:(i+1)*spectrogram.shape[1]] = (
                spectrogram[i:end+i,:])
    return frames

def trim_stimulus(s,fs,from_start = 0, from_end = 0):
    return s[numpy.floor(fs*from_start):
             s.shape[0]-numpy.ceil(fs*from_end), :]

def frame_to_filter(filter,n_t_steps):
    if len(filter)%n_t_steps != 0:
        raise ValueError('filter must be an whole number of frames')
    return filter.reshape((n_t_steps,filter.shape[0]/n_t_steps))

def bin_spiketimes(triggers,spikes,length,bin_size = 0.005):
    if not type(triggers) in numpy.ScalarType:
        triggers = triggers[0]
    bins = numpy.arange(0,length+0.0001,bin_size)+triggers
    return numpy.histogram(spikes,bins)[0]
    
def trim_spikes(spikes,fs,from_start = 0, from_end = 0):
    if spikes.ndim == 1:
        spikes = spikes.reshape((spikes.shape[0],1))
    #convert to 2-d but then only return one dimension
    return trim_stimulus(spikes,fs,from_start,from_end)[:,0]
