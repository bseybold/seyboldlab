# -*- coding: utf-8 -*-

'''code to analyze triggered recordings'''

import numpy
import scipy.io

known_stimuli = {
    'QuickFullToneStimuli': ('C:\Users\Bryan\test.mat',
                             [doRaster,doTuningCurve,doMTF,doPSTH,doTTL])
    }

run_values = {
    'raster_window':[-0.1, 1.1],
    'PSTH_window':[0, 0.1],
    'FTC_window':[0.005, 0.075],
    'Noise_window':[0.075 0.145],
    'TTLeffectWindow':[-0.1, 0.1],
    'TTLsoundWindow':[0.009, 0.017],
    'TTLttlWindow':[0, 0.005],
    'nPeriodHistogramBins':64,
    'resolution':0.001,
    }

class Analyzer():
    '''a class to hold all of the data analysis routines and results'''
    
    def __init__(self,spike_file = None, stimulus_name = None, save_name = None):
        if spike_file:
            self.load_spike_file(spike_file)
        if stimulus_name:
            self.load_stimulus(stimulus_name)
            
        self.save_name = save_name
        self.results = {}
        self.run_values = {}
        
        if spike_file and stimulus_name and save_name:
            self.run_analysis
            self.save_results
            
    def load_stimulus(self,stimulus_name):
        try:
            self.stimuli = scipy.io.loadmat(known_stimuli[stimulus_name][0])
            self.stimulus_name = stimulus_name
        except KeyError:
            raise ValueError('stimulus is not in known_stimuli')
            
    def load_spike_file(self,spike_file,require_full_trigger = True):
        self.spk = scipy.io.loadmat(spike_file)
        self.spike_file = spike_file
        #TODO add in the results in the .spk file
        #fill self.spiketimes
        #fill self.first_trigger times validate trigger times
        
        
    def run_analysis(self):
        if not self.stimulus_name or not self.spike_file:
            raise RuntimeError('Must load a spike_file and stimulus first')
 
        for analysis in known_stimuli[self.stimulus_name][1]:
            analysis(self)
            
    def doRaster(self):
        rasters = numpy.zeros(self.stimuli.shape, dtype = numpy.object)
        for index, stimulus in numpy.ndenumerate(self.stimuli):
            trigger_time = stimulus.trigger_time + self.first_trigger
            if trigger_time > self.spiketimes[-1]:
                rasters[index] = numpy.nan
            else:
                rasters[index] = self.spiketimes[ numpy.logical_and(
                        self.spiketimes > start + self.run_values['raster_window'][0],
                        self.spiketimes < start + self.run_values['raster_window'][1])] 
                rasters[index] -= trigger_time #time shift to rasters
        self.results['rasters'] = rasters
        self.run_values['raster_window'] = run_values['raster_window']
            
    def doPSTH(self):
        bins = range(run_values['PSTH_window'][0],run_values['PSTH_window'][1])
        psth = numpy.zeros(len(bins))
        count = 0
        for raster in numpy.nditr(self.results['rasters']):
            if rasters == numpy.nan:
                continue
            else:
                psth += numpy.histogram(raster,bins)
                count += 1
        psth /= count
        self.results['psth'] = psth
        self.run_values['PSTH_window'] = run_values['PSTH_window']

    def doTuningCurve(self):
        self.results['Tuning_Curve'] = self._slice_and_sum_rasters(run_values['FTC_window'])
        self.run_values['FTC_window'] = run_values['FTC_window']
    
    def _slice_and_sum_rasters(self,window):
        curve = numpy.zeros(self.results['rasters'].shape, dtype = numpy.int16)
        for index, raster in numpy.ndenumerate(self.results['rasters']):
            if raster == numpy.nan:
                curve[index] = numpy.nan
            else:
                curve[index] = sum(numpy.logical_and(
                        raster > start + window[0],
                        raster < start + window[1]))
        return curve
        
    def doMTF(self):
        num_f = self.stimuli.shape[0]
        frequencies = [x.frequency for x in stimuli[0]]
        periods = [1/f for f in frequencies]
        pbins = [range(0,p,p/run_values['nPeriodHistogramBins']) for p in periods]
        
        periodHistograms = numpy.zeros_like(numpy.array(pbins))
        counts = numpy.zeros(num_f)
        
        for index, raster in numpy.ndenumerate(self.results['rasters']):
            if raster == numpy.nan:
                continue
            else: #for length = 1, will be frequency-1 reps
                for x in range(1,frequencies[index[0]*self.stimuli[index].length):
                    #skip first presentation
                    spotBins = pbins[index[0]] + period[index[0]] * n 
                    periodHistograms[index[0],:] += numpy.histogram(raster,spotBins)
                counts[index[0]] += 1
                    
        #normalize bins by the number of sweeps and the bin width to get spike/sec
        periodHistograms /= numpy.tile(
            ((numpy.array(frequencies)-1)*counts)*(
                numpy.array(periods)/run_values['nPeriodHistogramBins']),
            1,pbin.shape[1])
        #the MTFs are the DC and fundamental of the Period Histograms
        rMTF = numpy.zeros(num_f)
        tMTF = numpy.zeros(num_f)
        for i, f in enumerate(frequencies):
            spectra = numpy.fft(periodHistograms[i,:])
            rMTF[i] = spectra[0]
            tMTF[i] = spectra[1]
        
        self.results['rMTF'] = rMTF
        self.results['tMTF'] = tMTF
        self.run_values['nPeriodHistogramBins'] = run_values['nPeriodHistogramBins']
        