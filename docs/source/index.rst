.. SeyboldLab documentation master file, created by
   sphinx-quickstart on Sat Aug 17 15:25:32 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SeyboldLab's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2
   Introduction 
   Project Design
   Installation
   Generating Stimuli
   Running Experiments
   Analyzing Results
   Using the Tools
   File Formats
   General Notes


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

