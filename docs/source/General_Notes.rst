=============
General Notes
=============

Access control for web apps
---------------------------

Access control for the servers is generally provided by presenting them on the localhost interface (127.0.0.1). This is simpler to implment than a reasonable authentication system. However, it does presnet a problem for remote access. The current method for working around this is to use SSH to set up a tunnel and set your webbrowser to use the tunnel as a proxy. Instructions for how to do this can be found online (search terms: SSH, tunnel, proxy, VPN)

Code development
----------------

This code was mostly written remotely using SSH and VIM. 

SSH is a wonderful protocol that lets you do almost any necessary task. There are protocols that are smoother to work with, but few are as across-the-board useful. Learn to use it on your system. 

VIM is a nice text editor to use remotely. Other people prefer EMACS, but I've become used to VIM. There is a distinct learning curve for different commands and modes, but the important ones become second nature quickly enough. Furthermore, people have spent a great deal of time extending VIM to work with differnt types of files and serve as a reasonable IDE. A copy of my .vimfiles directory (that houses the VIM scripts I use) is included in the tools directory.

Bottle.py
---------

Having looked at various web-microframeworks none impressed me as much as Bottle.py. It is entirely contained in one Pytho file and makes the simple kinds of servers I want to run simple. Almost every server is based on a very simple structure of "load a static page at /" and "handle requests with JSON".Setting up this level of code takes only a few moments with Bottle. Furthermore, Bottle.py supports both Python2 and Python3.
