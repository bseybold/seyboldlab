============
Installation
============

Installation will be described in three steps: setting up the working environment, acquiring the dependencies, and actual installation.

The working environment
-----------------------

This project is intended to run in either python 2.7 or python 3.3+. This will require substantial testing however, and is not a guarantee yet. Development has mostly occured in 2.7, but with the exception of metaclasses, the 2->3 process should be very smooth.

To get started, you should install one of these versions of python and set up virtual environments. Virtual environments were incorporated into Python3.3 (but still require some glue code), but all versions can use VirtualEnv and VirtualEnvWrapper. Virtual environments allow you to cleanly separate code and make changes. In particular, they allow you to build an isolated set of dependencies. This prevents breaking the project because you updated a dependency for some other reason.

By the time you read this, anything I write will be out of date, so go find out how to set up a virtual environment on the internet.

Dependencies
------------
The current known dependencies are as follows:

        Required Dependencies:
                Numpy
                Scipy
                Matplotlib
                Statsmodels
                PyTables

        Optional Dependencies:
                Brian    (the neural network simulator)

        Included Dependencies:
                Bottle

On a _nix system, these dependencies should be able to be built relatively easily or installed with pip. For this purpose a requirements.txt file is provided.

On a Windows system, precompiled binaries are easier to use. If you get errrors, check that you're using the versions in the requirements.txt file. Alternatively, setting up MingGW will allow you to use pip to install these from source. However, you will likely need to track down all the dependencies.

Installation
------------
At this point installation should be simple.

The preferred method of installation is to use pip. If you're going to be modifying the project, use a "development installation"::

        pip install -e setup.py

Otherwise, a regular installation will suffice::

        pip install setup.py
