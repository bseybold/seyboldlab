=====================
Design of the Project
=====================

This project was designed to build a cohesive toolchain for data, but maintain the ability to interact with novel data sources and formats at each step. Therefore, the design should contain conversion routines for each step and employ general modular structure.

Data and configurations should also be stored in portable file formats. To that end, the primary data types in the project are Python scripts (.py) files for configuration and HDF5 files for storage of large blocks of raw data. 

Furthermore, this project was designed to be usable anywhere. For that reason, user interfaces are generated as simple web apps with easy to use servers started from the command line.

Python
------

Out of all the languages out there, why use python? This has two parts, why use Python in general, and why not use Matlab like everyone else.

First, Python is a good language to write a system in. Python has a strong system for integrating different piece of code including other Python code, C libraries and others. Hardware developers provide interfaces to Python frequently enough, and when they do not, the ctype package in the standard library can generate one. Furthermore, Python is useful for interfacing to low level C libraries to interact with the system in a straight forward way. At the same time, Python has advances capabilities for interacting with the internet. From databases to web servers, Python is "batteries included." Lastly, Python has a good system for packages and virtual environments, this allows my code to be relatively cleanly isolated. This should increase stability over time.

There are some difficulties with Python however. For numeric operations, Python often seems statically typed. This requires additional casting of values and frustration. Most of these problems are simple to debug however. Additionally, debugging is not yet smooth for Matplotlib. It is often difficult to open plots while debugging. In the future, IDEs such as Spyder carry promise to releave these issues. Third, it is one more language for people to learn. This can scare people away, but I think the long term benefits in maintainability are worthwile. Additionally, Python has a well integrated system for unit tests. This increases the reliability of code and is relatively transparent for code that users acquire. Python is also open source, so it will remain accessible and free for decades. If I want to run this library on fifty computers at once, I could do so easily and at no additionl cost.

Why not Matlab? Matlab is the current standard in the field. However, it carries a number of drawbacks. First, it is a terrible system for building large libraries in. Sooner or later manually managing the path variable and trying to avoid namespace collisons will break down. Second, writing every function to a new file is a mess. (Admittedly, Matlab has an object oriented ability, but this is rarely used.) Third, Matlab isclosed-source and expensive, and worrying about license acquisition is annoying. 

There are some difficulties to not using Matlab. The main difficulty is that it can be hard to share code with others in the field. For that reason, much of the code is focused on low level routines that generate stimuli and acquire data. Tools for converting to Matlab parseable fromats can be implemented. For higher level analysis, this project should not prevent using Matlab if desired. Lastly, it does require people to learn a new language in some situations. However, once the library is established, most of it should be stable and rarely modified.

Metaclasses
-----------

One means of maintain modular structure is to track all declared instances with metaclasses. The primary metaclasses used track all defined functions that are subclasses of a particular base class. 

To use a user defined instance without modifing the main project repository, simply import the user file. The metaclass will note that a new type was created and add it to the list of possibilities. The package command line interface can import a list of files for this purpose.

Metaclasses were used rather than any other method for declaring plugins because it allows users to write code in a simple declarative style. To register a new class, users simply subclass the appropriate base class and import their file before running the desired service.

Data Formats
------------

Python scripts and HDF5 files were selected because these file should be portable across different systems and installations. Furthermore, the tools for handling these files should remain accessible for decades to come. To access these tools, users must simply download the required packages. 

Because the field currently uses Matlab extensively, interoperability between this tool chain and Matlab scripts is required. To ensure that the data types are retrievable from Matlab converters to .mat formats are encouraged and a library for reading this format of HDF5 files in Matlab is desirable. As a first pass, outputing code into simple text files that matlab can parse is acceptable, and all data streams should be capabale of this kind of output.

User Interfaces
---------------

To build interfaces that run anywhere (including over the internet), many components are integrate into simple web apps. These apps are hosted on simple, integrated web servers located in the tools directory.

Choosing to use web apps has several benefits. First, applications can be run remotely. Second, these applications should be able to be accessed on any system with a web browser without additional dependencies. Third, these applications can will have a consistent look and feel across platforms. Fourth, developing web apps is a useful skill beyond any single gui package. Fifth, these servers enforce a clean client-server architecture, which will hopefully ease development. In particular, it will be easy to adapt the server to a gui front-end.

Choosing to use web apps also has consequences. First, building web apps requires using three additional languages (HTML, CSS, and Javascript). Development in each of these is rather limited however, so hopefully the burden is not too great. Furthermore, the included examples can serve as templates for future work. Second, it is not always simple to pass images and videos from the server to web apps. This isn't always simple in gui packages either, so there is relatively little loss.Included tools for doing this will also lessen the burden.  

The Command Line
----------------

A command line interface was created to start up most project user interfaces. For simple use cases this is fine. For more complex tasks, the command line interface (in seyboldlab.__main__) documents how to start or use the servers.

To make it simple to add more functional classes, the command line interface can import .py files provided as arguments. If these files contain metaclass components, they will be found by the apporpriate metaclasses and included in the built in user interfaces.

